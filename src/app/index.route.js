(function(angular) {
  'use strict';

  angular
    .module('arquimetclient')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider, $urlRouterProvider) {
    // State
    $stateProvider.state('app', {
      abstract : true,
      views    :{
        'main@': {
          templateUrl: 'app/nucleo/layouts/content-body.html'
        },
        'header@app'   : {
          templateUrl: 'app/toolbar/toolbar.html',
          controller : 'ToolbarController as vm'
        },
        'quickpanel@app'   : {
          templateUrl: 'app/quickpanel/quickpanel.html',
          controller : 'QuickPanelController as vm'
        },
        'footer@app'   : {
          templateUrl: 'app/foot/foot.html',
          controller : 'FootController as vm'
        }
      }
    });


    $urlRouterProvider.otherwise('/propiedades');
  }

})(window.angular);
