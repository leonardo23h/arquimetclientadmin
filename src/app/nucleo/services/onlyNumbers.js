/**
 * Created by stackpointer on 24/06/17.
 */

(function (angular) {
  'use strict';

  angular
    .module('arquimetclient')
    .service('onlyNumbers', [onlyNumbersService]);

  function onlyNumbersService() {
    var vm = this;

    vm.validateNumber = validateNumber;
    //////////////////////////////////////////////////////////////////

    function validateNumber(event)
    {
      if (! /^-?\d+(\.\d+)?$/.test(event.key))
        event.preventDefault();
    }
  }
})(window.angular);
