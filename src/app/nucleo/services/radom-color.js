/**
 * Created by stackpointer on 9/06/17.
 */
(function (angular) {
  'use strict';

  angular
    .module('arquimetclient')
    .service('randomColor', [randomColor]);

  function randomColor() {

    var vm = this;

    /////////////////////// methods ///////////////////////
    vm.getRandomColor = getRandomColor;

    function aleatorio(inferior,superior){
      var
        numPosibilidades = superior - inferior,
        aleat = Math.random() * numPosibilidades;

      aleat = Math.floor(aleat);

      return parseInt(inferior) + aleat
    }

    function getRandomColor(){

      var hexadecimal = ["0","1","2","3","4","5","6","7","8","9","A","B","C","D","E","F"] ,
        color_aleatorio = "#";

      for (var i=0;i<6;i++){
        var posarray = aleatorio(0,hexadecimal.length);
        color_aleatorio += hexadecimal[posarray]
      }
      return color_aleatorio
    }
  }



})(window.angular);
