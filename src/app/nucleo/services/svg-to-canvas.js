(function (angular) {
  'use strict';

  angular
    .module('arquimetclient')
    .service('svgToCanvas', [svgToCanvas]);

  function svgToCanvas()
  {
    var vm = this;

    vm.getPngUrl = getPngUrl;

    function getPngUrl(element, width, height)
    {
      return new Promise(function (resolve, reject) {

        var svgHTML = angular.element(element)[0];
        var svgString = new XMLSerializer().serializeToString(svgHTML);

        var canvasHTML =
          '<canvas style="border:2px solid black;" width="' + width + '" height="' + height + '">\n' +
          '</canvas>';

        var canvas = angular.element(canvasHTML);

        var ctx = canvas[0].getContext("2d");
        var DOMURL = self.URL || self.webkitURL || self;
        var img = new Image();
        var svg = new Blob([svgString], {type: "image/svg+xml;charset=utf-8"});
        var url = DOMURL.createObjectURL(svg);
        img.onload = function () {
          ctx.drawImage(img, 0, 0);
          var pngUrl = canvas[0].toDataURL("image/png");
          DOMURL.revokeObjectURL(pngUrl);
          resolve(pngUrl);
        };
        if(url === '') reject('Error at url');
        img.src = url;
      });

    }
  }

})(window.angular);
