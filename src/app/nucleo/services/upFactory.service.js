(function (angular) {
  'use strict';

  angular
    .module('arquimetclient')
    .factory('upFactory', upFactory);

  /** @ngInject */
  function upFactory($window, $document) {

    /* getup.AdminLTE
     *
     * @type Object
     * @description It's used for implementing functions and options related
     *              to the template. Keeping everything wrapped in an object
     *              prevents conflict with other plugins and is a better
     *              way to organize our code.
     */
    var getup = {
      AdminLTE:{},
      up: _init
    };

    /* --------------------
     * - getup.AdminLTE Options -
     * --------------------
     * Modify these options to suit your implementation
     */
    getup.AdminLTE.options = {
      //Add slimscroll to navbar menus
      //This requires you to load the slimscroll plugin
      //in every page before app.js
      navbarMenuSlimscroll: true,
      navbarMenuSlimscrollWidth: "3px", //The width of the scroll bar
      navbarMenuHeight: "200px", //The height of the inner menu
      //General animation speed for JS animated elements such as box collapse/expand and
      //sidebar treeview slide up/down. This options accepts an integer as milliseconds,
      //'fast', 'normal', or 'slow'
      animationSpeed: 500,
      //Sidebar push menu toggle button selector
      sidebarToggleSelector: "[data-toggle='offcanvas']",
      //Activate sidebar push menu
      sidebarPushMenu: true,
      //Activate sidebar slimscroll if the fixed layout is set (requires SlimScroll Plugin)
      sidebarSlimScroll: true,
      //Enable sidebar expand on hover effect for sidebar mini
      //This option is forced to true if both the fixed layout and sidebar mini
      //are used together
      sidebarExpandOnHover: false,
      //BoxRefresh Plugin
      enableBoxRefresh: true,
      //Bootstrap.js tooltip
      enableBSToppltip: true,
      BSTooltipSelector: "[data-toggle='tooltip']",
      //Enable Fast Click. Fastclick.js creates a more
      //native touch experience with touch devices. If you
      //choose to enable the plugin, make sure you load the script
      //before getup.AdminLTE's app.js
      enableFastclick: false,
      //Control Sidebar Tree views
      enableControlTreeView: true,
      //Control Sidebar Options
      enableControlSidebar: true,
      controlSidebarOptions: {
        //Which button should trigger the open/close event
        toggleBtnSelector: "[data-toggle='control-sidebar']",
        //The sidebar selector
        selector: ".control-sidebar",
        //Enable slide over content
        slide: true
      },
      //Box Widget Plugin. Enable this plugin
      //to allow boxes to be collapsed and/or removed
      enableBoxWidget: true,
      //Box Widget plugin options
      boxWidgetOptions: {
        boxWidgetIcons: {
          //Collapse icon
          collapse: 'fa-minus',
          //Open icon
          open: 'fa-plus',
          //Remove icon
          remove: 'fa-times'
        },
        boxWidgetSelectors: {
          //Remove button selector
          remove: '[data-widget="remove"]',
          //Collapse button selector
          collapse: '[data-widget="collapse"]'
        }
      },
      //Direct Chat plugin options
      directChat: {
        //Enable direct chat by default
        enable: true,
        //The button to open and close the chat contacts pane
        contactToggleSelector: '[data-widget="chat-pane-toggle"]'
      },
      //Define the set of colors to use globally around the website
      colors: {
        lightBlue: "#3c8dbc",
        red: "#f56954",
        green: "#00a65a",
        aqua: "#00c0ef",
        yellow: "#f39c12",
        blue: "#0073b7",
        navy: "#001F3F",
        teal: "#39CCCC",
        olive: "#3D9970",
        lime: "#01FF70",
        orange: "#FF851B",
        fuchsia: "#F012BE",
        purple: "#8E24AA",
        maroon: "#D81B60",
        black: "#222222",
        gray: "#d2d6de"
      },
      //The standard screen sizes that bootstrap uses.
      //If you change these in the variables.less file, change
      //them here too.
      screenSizes: {
        xs: 480,
        sm: 768,
        md: 992,
        lg: 1200
      }
    };

    /* ----------------------------------
     * - Initialize the getup.AdminLTE Object -
     * ----------------------------------
     * All getup.AdminLTE functions are implemented below.
     */
    function _init() {
      'use strict';

      /* PushMenu()
       * ==========
       * Adds the push menu functionality to the sidebar.
       *
       * @type Function
       * @usage: getup.AdminLTE.pushMenu("[data-toggle='offcanvas']")
       */
      //var body = $document[0].body;

      getup.AdminLTE.pushMenu = {
        activate: function (toggleBtn) {
          //Get the screen sizes
          var screenSizes = getup.AdminLTE.options.screenSizes;



          //Enable sidebar toggle
          $document.on('click', toggleBtn, function (e) {
            e.preventDefault();

            //Enable sidebar push menu
            if ($window.innerWidth > (screenSizes.sm - 1)) {
              if (angular.element("body").hasClass('sidebar-collapse')) {
                angular.element("body").removeClass('sidebar-collapse');
              } else {
                angular.element("body").addClass('sidebar-collapse');
              }
            }
            //Handle sidebar push menu for small screens
            else {
              if (angular.element("body").hasClass('sidebar-open')) {
                angular.element("body").removeClass('sidebar-open').removeClass('sidebar-collapse');
              } else {
                angular.element("body").addClass('sidebar-open');
              }
            }
          });

          angular.element(".content-wrapper").click(function () {
            //Enable hide menu when clicking on the content-wrapper on small screens
            if ($window.innerWidth <= (screenSizes.sm - 1) && angular.element("body").hasClass("sidebar-open")) {
              angular.element("body").removeClass('sidebar-open');
            }
          });

          //Enable expand on hover for sidebar mini
          if (getup.AdminLTE.options.sidebarExpandOnHover
            || (angular.element("body").hasClass('fixed')
            && angular.element("body").hasClass('sidebar-mini'))) {
            this.expandOnHover();
          }
        },
        expandOnHover: function () {
          var _this = this;
          var screenWidth = getup.AdminLTE.options.screenSizes.sm - 1;
          //Expand sidebar on hover
          angular.element('.main-sidebar').hover(function () {
            if (angular.element("body").hasClass('sidebar-mini')
              && angular.element("body").hasClass('sidebar-collapse')
              && $window.innerWidth > screenWidth) {
              _this.expand();
            }
          }, function () {
            if (angular.element("body").hasClass('sidebar-mini')
              && angular.element("body").hasClass('sidebar-expanded-on-hover')
              && $window.innerWidth > screenWidth) {
              _this.collapse();
            }
          });
        },
        expand: function () {
          angular.element("body").removeClass('sidebar-collapse').addClass('sidebar-expanded-on-hover');
        },
        collapse: function () {
          if (angular.element("body").hasClass('sidebar-expanded-on-hover')) {
            angular.element("body").removeClass('sidebar-expanded-on-hover').addClass('sidebar-collapse');
          }
        }
      };
    }

    return getup;
  }

})(window.angular);
