/**
 * Created by stackpointer on 7/04/17.
 */

(function (angular) {
  angular
    .module('arquimetclient')
    .service('unidadesService', ['api', unidadesService]);

  function unidadesService(api) {
    var vm = this;

    //data
    vm.list = {};
    vm.unitSelected = {};

    //functions
    vm.reqUnidades = reqUnidades;
    vm.chngSelected = chngSelected;

    ///////////////////////////////////////////////////////////////////

    function reqUnidades() {
      return api.general.unidades.get()
        .then(reqUnidadesCompleted)
        .catch(reqUnidadesFailed);

      function reqUnidadesCompleted(response) {
        vm.list = response.data;
        vm.unitSelected = vm.list[7];

        return vm
      }

      function reqUnidadesFailed(err) {
        return err;
      }
    }

    function chngSelected(unidad) {
      vm.unitSelected = unidad;
    }

  }
})(window.angular);
