/**
 * Created by esleider on 25/05/17.
 */

(function (angular) {
  "use strict";

  angular
    .module('checklist',[])
    .service('checkList', [
      checklistService
    ]);

  function checklistService() {

    var vm = this;

    //functions
    vm.exists = exists;
    vm.content = content;
    vm.selectPerfiles = selectPerfiles;
    vm.deselectPerfiles = deselectPerfiles;
    vm.atLeasOne = atLeasOne;

    //////////////////////////////////////////////////////////////////////////////

    function exists(item, list) {
      return list.indexOf(item) > -1;
    }

    function content(list, items) {
      var length = items.length,
        count = 0,
        check = false;

      for(var i = 0; i < length; i++)
      {
        if ( list.indexOf(items[i]) > -1 )
        {
          count++;
        }
      }

      if(count === length)check = true;

      return check;
    }

    function atLeasOne(item, list) {
      var response = false;

      if(list.length === 1 && exists(item, list))
      {
        response = true;
      }

      return response;
    }

    function selectPerfiles(list, items, callbackAction) {
      var length = items.length;

      for(var i = 0; i < length; i++)
      {
        if (list.indexOf(items[i]) < 0 )
        {
          list.push(items[i]);
        }
      }
      if(callbackAction)callbackAction();
    }

    function deselectPerfiles(list, items, callbackAction) {
      var length = items.length;

      for(var i = 0; i < length; i++)
      {
        if ( list.indexOf(items[i]) > -1 )
        {
          list.splice(list.indexOf(items[i]), 1);
        }
      }

      callbackAction();
    }
  }
})(window.angular);
