/**
 * Created by stackpointer on 7/07/17.
 */

/**
 * Created by aesleider on 09/03/2017.
 */

(function (angular) {
  'use strict';

  angular
    .module('arquimetclient')
    .provider('urlBase', urlBase);

  function urlBase() {

    var vm = this;

    //data
    vm.base = 'http://localhost:50005/';

    //functions

    vm.$get = factoryProvider;

    //defaults attrib

    //////////////////////////////////////////////////
    function factoryProvider() {
      return vm;
    }

    return vm;
  }

})(window.angular);

