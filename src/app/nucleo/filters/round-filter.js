/**
 * Created by stackpointer on 15/06/17.
 */

(function (angular) {
  'use strict';

  angular
    .module('arquimetclient')
    .filter('round', [roundFilter]);

  function roundFilter() {
    return function (input, decimales) {
      if (decimales == undefined)decimales = 2;

      //NOTE: this filter need d3 library third part.
      return d3.format(',.'+ decimales + 'f')(input);
    }
  }
})(window.angular);
