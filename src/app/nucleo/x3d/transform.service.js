(function (angular) {
  'use strict';

  angular
    .module('arquimetclient')
    .service('transformService', [
      transformService
    ]);

  function transformService()
  {
    var vm = this;

    //injects

    //methods
    vm.newInstance = newInstance;

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function newInstance(nameSpace)
    {
      var transform = newTransform();

      if (typeof nameSpace !== 'undefined')
        transform.nameSpace = nameSpace;

      //data
      transform.render = true;
      transform.scale.x = '1';
      transform.scale.y = '1';
      transform.scale.z = '1';
      transform.translation.x = '0';
      transform.translation.y = '0';
      transform.translation.z = '0';
      transform.rotation.x = '0';
      transform.rotation.y = '0';
      transform.rotation.z = '1';
      transform.rotation.a = '0'; // angulo en radianes

      //methods
      transform.toScale = toScale;
      transform.toMeters = toMeters;


      /////////////////////////////////////////////////////////////////////////////////////////////
      function toScale(value)
      {
        return value / 2;
      }

      function toMeters(value)
      {
        return value * 2;
      }

      return transform
    }

    function newTransform()
    {
      var transform = {};

      transform.scale = {};
      transform.translation = {};
      transform.rotation = {};

      return transform;
    }


  }

})(window.angular);
