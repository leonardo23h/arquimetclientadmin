/**
 * Created by stackpointer on 24/07/17.
 */

/**
 * Created by aesleider on 13/03/2017.
 */
(function (angular) {
  'use strict';

  angular
    .module('arquimetclient')
    .service('x3dService', [
      'sceneService',
      'viewConnector',
      x3dService
    ]);

  function x3dService(sceneService, viewConnector)
  {
    //object (factory)
    var vm = this;

    //funtions
    vm.newInstance = newInstance;

    /////////////////////////////////////////////// Functions  ////////////////////////////////////////////////////

    function newInstance(idX3d)
    {

      var x3d = {};

      //data
      x3d.id = idX3d;
      x3d.axes = {show: true};
      x3d.orientationBx = {show: true};
      x3d.ebProgres = false;
      x3d.dbDoubleClick = true;
      x3d.dbRightDrag = true;
      x3d.scene = sceneService.newInstance();
      x3d.style = {};
      x3d.style.width = "100%";
      x3d.style.position = "relative";
      x3d.style.border = "0";
      x3d.style.margin = "2px";
      x3d.style.padding = "0";

      //methods
      x3d.resetView = rstView;
      x3d.zoom = deltaZoom;
      document.onload = viewConnect;



      return x3d;
      ///////////////////////////////////////////////////////////////////

      //function que restaura el objeto a su vista inicial configurada.
      function rstView()
      {
        var x3dElem = document.getElementById(x3d.id);
        x3dElem.runtime.resetView();
      }

      /**
       *
       * Funcion para establecer el zoom de la escena x3dom
       * en esta funcion se establece un rango y se calcula cuanto representa en un rango de 0 a 3.1416. donde 0 es un radio de vista nulo
       * y 3.1416 es un rango de vista amplio el objeto se ve lo mas lejos posible.
       *
       * @param delta -> en una escala de 1 - 100 en porcentaje cuanto varia el fieldOfView
       * @param rangoi -> limite inferior del rango en el que se mueve el zoom.
       * @param rangof -> limite superior del rango en el que se mueve el zoom.
       */

      function deltaZoom(delta, rangoi, rangof)
      {

        var rango = rangof - rangoi,
          porcentajeEscala = rangoi + (rango * delta) / 100,
          inverseFieldOfViewForZoom = porcentajeEscala / 100 * 3.1416,//valor invertido cercano a 3.1416 real para hacer zoom, @delta es un valor en porcentaje.
          fieldOfViewForZoom = 3.1416 - inverseFieldOfViewForZoom,//valor real cercano a 0 para hacer zoom, donde 0 es el 100%, no hay distancia visual.
          x3dElem = document.getElementById(x3d.id),
          vpt = x3dElem.getElementsByTagName("viewPoint")[0],
          currentfieldOfView = parseFloat(vpt.fieldOfView),
          deltafieldOfView = Math.abs(currentfieldOfView - fieldOfViewForZoom),
          zm = 0;

        /*
        *
        * negativo para cerrar el rango de vista, es decir ZoomIn.
        * positivo para abrir el rango de vista, es decir ZoomOut.
        * */
        currentfieldOfView > fieldOfViewForZoom ?
          zm = currentfieldOfView - deltafieldOfView :
          zm = currentfieldOfView + deltafieldOfView;

        if (zm > 0 && zm < 3.1416) {
          vpt.fieldOfView = zm;
        }
      }

      function viewConnect()
      {
        viewConnector.__init(x3d.id);
      }

    }

  }

})(window.angular);
