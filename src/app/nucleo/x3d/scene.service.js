(function (angular) {
  'use strict';

  angular
    .module('arquimetclient')
    .service('sceneService', [
      'transformService',
      sceneService
    ]);

  function sceneService(transformService)
  {

    //object (service)
    var vm = this;

    //funtions
    vm.newInstance = newInstance;

    /////////////////////////////////////////////// Functions  ////////////////////////////////////////////////////

    function newInstance()
    {
      var scene = {};

      scene.viewpoint = {};
      scene.background = {};
      scene.navigationInf = {};
      scene.directionalLight = {};
      scene.superTransform = transformService.newInstance();
      scene.superTransform.transforms = [];

      return scene;
    }

  }

})(window.angular);

