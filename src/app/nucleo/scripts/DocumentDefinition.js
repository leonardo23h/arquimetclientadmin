function DocumentDefinition()
{
	this.image_size_reader = new image_size_reader();
	this.docDefinition = {};
	this.stacks = [];
	this.contents = [];
	this.indent = 0;
	this.indentStep = 20;
	this.decimals = 2;

	/**
	 * Esta es una funcion que debe ser asignada externamente: dd.getImagenDinamica = buscarImagenDinamica;
	 * En donde "buscarImagenDinamica" es una funcion definida en otro lugar.
	 */
	this.getImagenDinamica = null;

	this.styles = {
		tituloPrincipal: {
			bold: true,
			fontSize: 14,
			color: 'black',
			alignment: 'center',
			margin: [0, 60, 0, 10]
		},
		tituloPrincipalGrande: {
			bold: true,
			fontSize: 18,
			color: 'black',
			alignment: 'center',
			margin: [0, 60, 0, 10]
		},
		tituloTabla: {
			bold: true,
			fontSize: 13,
			color: 'black',
			alignment: 'center',
			margin: [0, 20, 0, 5]
		},
		h1: {
			bold: true,
			fontSize: 13,
			color: 'black',
			alignment: 'center',
			margin: [0, 20, 0, 10]
		},
		h2: {
			bold: true,
			fontSize: 12,
			color: 'black',
			alignment: 'left',
			margin: [0, 10, 0, 10]
		},
		h3: {
			bold: false,
			fontSize: 11,
			color: 'black',
			alignment: 'left',
			margin: [0, 3, 0, 3]
		},
		tableHeader: {
			bold: true,
			fontSize: 10,
			color: 'black',
			alignment: 'center'
		},
		tableCell: {
			color: 'black',
			fontSize: 10,
			alignment: 'center'
		}
		,
		no_encontrado: {
			fillColor: '#FFFF00',
			color: '#FF0000'
		}
	};

	this.pageMargins = [80, 50, 80, 50];

	// Estilos globales de la pagina
	this.defaultStyle = {
		color: 'black',
		fontSize: 10,
		lineHeight: 1.1,
		font: 'Calibri'
	};

	// para las tablas
	this.layout_lineas_grises = {
		hLineColor: function (i, node)
		{
			return 'gray';
		},
		vLineColor: function (i, node)
		{
			return 'gray';
		}
	};

	// para las tablas
	this.layout_lineas_delgadas = {
		hLineWidth: function (i, node)
		{
			return 0.5;
		},
		vLineWidth: function (i, node)
		{
			return 0.5;
		}
	};

	// Para formatear los INDENT usando tablas
	this.layout_sin_lineas = {
		hLineWidth: function (i, node)
		{
			return 0;
		},
		vLineWidth: function (i, node)
		{
			return 0;
		}
	};

	// Para formatear los INDENT usando tablas
	this.layout_no_padding = {
		paddingLeft: function (i, node)
		{
			return 0;
		},
		paddingRight: function (i, node)
		{
			return 0;
		},
		paddingTop: function (i, node)
		{
			return 0;
		},
		paddingBottom: function (i, node)
		{
			return 0;
		}
	};

	// Constructor de un array previamente inicializado, para que PDFMAKE no falle
	this.get_array = function (width)
	{
		var row = [];
		for (j = 0; j < width; j++)
		{
			row[j] = '';
		}
		return row;
	};

	// insertar titulo en tamano H1
	this.titulo_h1 = function ()
	{
		this.add_content({
			text: this.format(arguments),
			style: 'h1'
		});
	};

	// insertar titulo en tamano H2
	this.titulo_h2 = function ()
	{
		this.add_content({
			text: this.format(arguments),
			style: 'h2'
		});
	};
	// insertar titulo en tamano H3
	this.titulo_h3 = function ()
	{
		this.add_content({
			text: this.format(arguments),
			style: 'h3'
		});
	};

	// insertar titulo en tamano H1
	this.titulo_h1_left = function ()
	{
		this.add_content({
			text: this.format(arguments),
			style: 'h1',
			alignment: 'left'
		});
	};

	// insertar titulo en tamano H2
	this.titulo_h2_left = function ()
	{
		this.add_content({
			text: this.format(arguments),
			style: 'h2',
			alignment: 'left'
		});
	};
	// insertar titulo en tamano H3
	this.titulo_h3_left = function ()
	{
		this.add_content({
			text: this.format(arguments),
			style: 'h3',
			alignment: 'left'
		});
	};

	// Insertar titulo principal del documento
	this.titulo_principal = function ()
	{
		this.add_content({
			text: this.format(arguments),
			style: 'tituloPrincipal'
		});
	};
	// Insertar titulo principal del documento
	this.titulo_principal_grande = function ()
	{
		this.add_content({
			text: this.format(arguments),
			style: 'tituloPrincipalGrande'
		});
	};

	// formatear el titulo de la tabla geometrica
	this.titulo_tabla = function ()
	{
		this.add_content({
			text: this.format(arguments),
			style: 'tituloTabla'
		});
	};

	// imprimir una variable de la solicitud
	this.add_variable = function (variable)
	{
		var linea = {
			table: {
				headerRows: 0,
				widths: ['*', 'auto'],
				body: [[{text: '??' + variable['search'] + '??', style: 'no_encontrado'}, {text: ' ', style: 'no_encontrado'}]]
			},
			layout: this.mergeAll(this.layout_no_padding, this.layout_sin_lineas)
		};

		if (variable['item'] != null)
		{
			var valor = variable['item']['value'];
			linea.table.body = [[variable['item']['descripcion'], {text: this.format("{0}{1}", valor, variable['item']['unidad']), alignment: 'right', margin: [5, 0, 0, 0]}]];
		}
		this.add_content(linea);
	};

	// Igual que add_variable pero recibiendo un array de variables
	this.add_variables = function (variables, decimals)
	{
		for (var i in variables)
		{
			this.add_variable(variables[i], decimals);
		}
	};


	// anexa una tabla NxM arbitraria
	// title = titulo de la tabla
	// columns: lista de columnas encontradas en los rows, ejemplo: { 'longitud':'Longitud', 'cargaMuerta':'Carga Muerta' }
	// rows: coleccion de filas encontradas en el json
	this.add_tabla_datos = function (title, columns, rows, formatos, mostrar_header_tabla, cell_parameters)
	{
		var width = columns.length;
		var heigth = rows.length;

		if (typeof cell_parameters == 'undefined')
		{
			cell_parameters = {};
		}
		if (typeof formatos == 'undefined')
		{
			formatos = {};
		}		
		if(typeof mostrar_header_tabla == 'undefined')
		{
			mostrar_header_tabla = true;
		}

		var contenido_tabla =
				{
					table:
							{
								headerRows: 0,
								widths: [],
								alignment: 'center',
								body: []
							},
					layout: this.mergeAll(this.layout_lineas_grises, this.layout_lineas_delgadas)
				};

		// titulos y primera fila de la tabla
		var titulos = this.get_array(width);
		var widths = this.get_array(width);
		var i = 0;
		for (var key in columns)
		{
			if (mostrar_header_tabla == true)
			{
				titulos[i] = {text: columns[key], style: 'tableCell'};
			}
			widths[i] = 'auto';
			i++;
		}

		this.contents[this.contents.length] = {text: '\n' + title, style: 'tableHeader'};
		if (mostrar_header_tabla == true)
		{
			contenido_tabla['table']['body'][contenido_tabla['table']['body'].length] = titulos;
		}
		contenido_tabla['table']['widths'] = widths;

		// valores
		for (var rowKey in rows)
		{
			var contenidoRow = this.get_array(width);
			var i = 0;
			for (var key in columns)
			{
				var contenido = rows[rowKey][key];
				var extra = {};

				if (typeof formatos[key] != 'undefined')
				{
					contenido = this.formatKeys(formatos[key], rows[rowKey]);
				}

				if (typeof cell_parameters[key] != 'undefined')
				{
					extra = cell_parameters[key];
				}

				contenidoRow[i] = this.mergeAll({text: contenido, style: 'tableCell'}, extra);
				i++;
			}
			contenido_tabla['table']['body'][contenido_tabla['table']['body'].length] = contenidoRow;
		}




		this.contents[this.contents.length] = {
			columns: [
				{width: '*', text: ''},
				this.mergeAll(contenido_tabla, {width: 'auto'}),
				{width: '*', text: ''}
			]
		};
	};

	// imprime una imagen de una variable, si esta variable no es del tipo "data:" entonces la imprime como texto
	// opcional: comparacion -> produce que la variable sea comparada utilizando este criterio, por ejemplo ">1"
	// opcional: decimals, imprime el valor con N decimales (opcional), default decimals=2
	this.add_imagen_variable = function (variable, comparacion, decimals, para, variable_cumplimiento, zoom)
	{
		var linea = {
			table: {
				headerRows: 0,
				widths: ['*', 'auto', 'auto'],
				body: [[{text: '??' + variable['search'] + '??', style: 'no_encontrado'}, '', '']],
				margin: [100, 0, 0, 0]
			},
			layout: this.mergeAll(this.layout_no_padding, this.layout_sin_lineas)
		};
		if (variable['item'] != null)
		{
			var margen = 5;
			var txt_comparacion = '';
			var primera_columna;
			var imagenCumplimiento = '';
			if (para == null)
			{
				para = 'Para ';
			}
			var imagen = {width: 0, height: 0};
			if (variable['item']['descripcion'].startsWith('data:image'))
			{
				imagen = this.getImage(variable['item']['descripcion']);
				if (zoom != null)
				{
					imagen['width'] *= zoom;
					imagen['height'] *= zoom;
				}

				primera_columna = this.mergeAll(imagen, {margin: [0, margen, 0, margen]})
			}
			else
			{
				primera_columna = {text: para + variable['item']['descripcion'], margin: [0, margen, 0, margen]};
			}


			if (comparacion != null)
			{
				var redondeado = 'NAN';
				try
				{
					redondeado = this.round(variable['item']['value'], decimals);
				}
				catch (e)
				{

				}
				var margen_superior = margen + (imagen['height'] > 4 ? (imagen['height'] / 2) - 2 : 0);
				txt_comparacion = {text: this.format("{0}{1}", redondeado, comparacion), alignment: 'right', margin: [5, margen_superior, 0, margen]};
			}

			if (variable_cumplimiento != null)
			{
				if (variable_cumplimiento['item'] != null)
				{
					imagenCumplimiento = this.getImage(variable_cumplimiento['item']['descripcion']);
					var margen_superior = margen + (imagen['height'] - imagenCumplimiento['height'] > 0 ? ((imagen['height'] - imagenCumplimiento['height']) / 2) : 0);
					imagenCumplimiento = this.mergeAll(imagenCumplimiento, {alignment: 'right', margin: [5, margen_superior, 0, 0]});
				}
			}

			linea['table']['body'] = [[primera_columna, txt_comparacion, imagenCumplimiento]];
		}
		this.add_content(linea);
	};

	// inserta una imagen base64 respetando el margen existente
	this.add_imagen_base64 = function (imagenBase64, zoom)
	{

		var margen = 5;

		var imagen = this.getImage(imagenBase64);
		
		if (zoom != null)
		{
			imagen['width'] *= zoom;
			imagen['height'] *= zoom;
		}
		
		var imagen_formateada = this.mergeAll(imagen, {margin: [0, margen, 0, margen]})

		this.add_content(imagen_formateada);
	};

	// inserta una imagen base64 respetando el margen existente
	this.add_imagen_dinamica = function (nombreImagen, zoom)
	{
		var no_encontrado = {text: '?? Imagen no encontrada: ' + nombreImagen + '??', style: 'no_encontrado'};
		var imagen = {width:0,height:0};

		/**
		 * Try and Catch, no sabemos si la funcion ya ha sido definida
		 */
		try
		{
			imagen = this.getImagenDinamica(nombreImagen);
		}
		catch (e)
		{

		}

		if (imagen == null)
		{
			this.add_content(no_encontrado);
			return;
		}

		this.add_imagen_base64(imagen, zoom);
	};

	/**
	 * Redondea el numero con N decimales
	 * Si la cantidad de decimales es -1 el numero se deja intacto
	 * @param {type} number
	 * @param {type} decimals
	 * @returns {unresolved}
	 */
	this.round = function (number, decimals)
	{
		if (decimals == -1)
		{
			return number;
		}
		else if ((decimals == null) || (decimals == ''))
		{
			decimals = this.decimals;
		}

		var tmp = Math.pow(10, decimals);
		return Math.round(eval(number) * tmp) / tmp;
	};

	// anadir imagen de cumplimiento de la solicitacion
	// comparacion: texto utilizado para comparar el valor numerico, por ejemplo ">1"
	this.add_cumplimiento = function (solicitacion, comparacion, decimals)
	{
		var redondeado = this.round(solicitacion['resultado']['factorCumplimiento'], decimals);

		var imagen_cumplimiento = this.getImage(solicitacion['resultado']['imagenCumplimiento']);
		var formula_cumplimiento = this.getImage(solicitacion['resultado']['imagenFormulaCumplimiento']);

		var linea = {
			table: {
				headerRows: 0,
				widths: ['*', 'auto', 'auto'],
				body: [[
						formula_cumplimiento,
						{text: this.format("{0}{1}", redondeado, comparacion), alignment: 'right', margin: [5, (imagen_cumplimiento['height'] / 2) - 4.5, 0, 0]},
						imagen_cumplimiento
					]]
			},
			layout: this.mergeAll(this.layout_no_padding, this.layout_sin_lineas)
		};
		this.add_content(linea);
	};

	// Buscar informacion sobre el contenido de una imagen en BASE64
	// retorna ancho y alto en PUNTOS, no en PIXELS
	this.getImage = function (contentBase64)
	{
		var output = this.image_size_reader.getDimensions(contentBase64);

		output['image' ] = contentBase64;
		output['width' ] *= 0.75;
		output['height'] *= 0.75;

		return output;
	};

	// Anadir contenido a este documento
	// si la variable this.indent es mayor a cero entonces el contenido es identado utilizando una tabla como ayuda
	this.add_content = function (param)
	{
		if (this.indent == 0)
		{
			this.contents[this.contents.length] = param;
		}
		else
		{
			this.contents[this.contents.length] = {
				table: {
					headerRows: 0,
					widths: [this.indentStep * this.indent, '*'],
					body: [['', param]]
				},
				layout: this.mergeAll(this.layout_no_padding, this.layout_sin_lineas)
			};
		}
	};

	// Anadir contenido a este documento
	// si la variable this.indent es mayor a cero entonces el contenido es identado utilizando una tabla como ayuda
	this.add_italics = function (textIn)
	{
		var obj_text = {text: textIn, italics: true};

		this.add_content(obj_text);
	};

	this.no_encontrado = function (txt)
	{
		var txtout =
				{
					table:
							{
								headerRows: 0,
								widths: ['*'],
								body: [[{text: '?? ' + txt + ' ??', style: 'no_encontrado'}]]
							},
				};
		this.contents[this.contents.length] = txtout;
	};

	// linea nueva
	this.new_line = function ()
	{
		this.contents[this.contents.length] = {text: '\n', margin: [0, 0, 0, 0]};
	};
	// pagina nueva
	this.new_page = function ()
	{
		if ((this.contents.length > 0) || (this.stacks.length > 0))
		{
			this.contents[this.contents.length] = {text: '', pageBreak: 'after'};
		}
	};

	// Anadir contenido con un margen superior e inferior para que se separe un poco del resto del texto (dar enfasis a un texto)
	this.add_content_margin = function (param)
	{
		this.add_content(
				{
					text: param,
					margin: [0, 5, 0, 5]
				}
		);
	};

	/**
	 * Buscar un item con un KEY especifico dentro de un ARRAY
	 * @param {type} listado_buscar
	 * @param {type} keyName
	 * @param {type} keyValue
	 * @returns {DocumentDefinition.buscar_key.o}
	 */
	this.buscar_key = function (listado_buscar, keyName, keyValue)
	{
		var o = {search: keyValue, item: null};
		if (typeof listado_buscar != 'undefined')
		{
			for (var i = 0; i < listado_buscar.length; i++)
			{
				if (listado_buscar[i][keyName] == keyValue)
				{
					o['item'] = listado_buscar[i];
					break;
				}
			}
		}
		else
		{
			o['search'] += ' (en lista vacia)';
		}

		return o;
	};

	// identico a buscar key pero recibe un array de entradas y devuelve un array de resultados
	this.buscar_keys = function (listado_buscar, keyName, keyValuesArray)
	{
		var output = [];

		for (i in keyValuesArray)
		{
			output[output.length] = this.buscar_key(listado_buscar, keyName, keyValuesArray[i]);
		}

		return output;
	};

	// Recibe un string y una coleccion[] de variables para insertarlas en el string original siguiendo el orden {0} {1} {2} ... {n}
	// format("hola {0} {1}","Pedro","Perez") o format(["hola {0} {1}","Pedro","Perez"]) => "hola Pedro Perez"
	this.format = function ()
	{
		var args = arguments;
		if (typeof args[0] == 'object')
		{
			args = args[0];
		}

		return args[0].replace(/{(\d+)}/g, function (match, number)
		{
			var N = eval(number + "+1");
			return typeof args[N] != 'undefined' ? args[N] : match;
		});
	};

	// Recibe un string de formato y un objeto para insertarlo en el string original guiandose por sus KEYS "{key1} {key4} {key3}"
	this.formatKeys = function ()
	{
		var args = arguments;
		if (typeof args[0] == 'object')
		{
			args = args[0];
		}

		return args[0].replace(/\{(.+?)\}/g, function (match, found)
		{
			return typeof args[1][found] != 'undefined' ? args[1][found] : match;
		});
	};



	// Une N objetos en uno solo
	this.mergeAll = function ()
	{
		var output = {};
		var args = arguments;

		for (var i in args)
		{
			var src = args[i];
			Object.getOwnPropertyNames(src).forEach(function (key)
			{
				output[ key ] = src[ key ];
			});
		}

		return output;
	};

	/**
	 * Envia todo lo acumulado a un STACK individual
	 * @returns {undefined}
	 */
	this.pushStack = function ()
	{
		this.stacks[this.stacks.length] = {stack: this.contents};
		this.contents = [];
		this.indent = 0;
	}

	/**
	 * Retorna la definicion de documento utilizada por PDFMake
	 * Debe ejecutar pushStack() antes de esta parte para que lo acumulado sea incluido en la salida
	 * @returns {DocumentDefinition.docDefinition}
	 */
	this.output = function ()
	{
		this.docDefinition['styles' ] = this.styles;
		this.docDefinition['defaultStyle' ] = this.defaultStyle;
		this.docDefinition['content'] = [{stack: this.stacks}];
		this.docDefinition['pageMargins'] = this.pageMargins;
		return this.docDefinition;
	};

	/**
	 * Para generacion de documentos mas dinamicos.
	 * Se anaden abjetos directamente para los tipos soportados.
	 * @param {type} object
	 * @returns {undefined}
	 */
	this.addObject = function(obj)
	{
		if(typeof obj != 'object')
		{
			this.no_encontrado('no es un objecto');
			return;
		}
		
		if(typeof obj['tipo'] != 'string')
		{
			this.no_encontrado('objeto sin tipo (string) definido');
			return;
		}

		if(obj['tipo'] == '')
		{
			return;
		}

		var funcion_buscada = 'addObject_' + obj['tipo'];
		if(typeof this[funcion_buscada] == 'undefined')
		{
			this.no_encontrado('tipo de objeto no definido ('+ obj['tipo'] +')');
			return;
		}

		var ind = this.indent;
		if(typeof obj.tab != 'undefined')
		{
			this.indent = obj.tab;
		}
		this[funcion_buscada]( obj );
		this.indent = ind;
	};


	this.addObject_texto = function(obj)
	{
		if(typeof obj.tab != 'undefined')
		{
			this.indent = obj.tab;
		}
		this.add_content(obj['descripcion']);
	};
	
	this.addObject_imagen = function(obj)
	{
		var zoom = 1;
		if(typeof obj.zoom != 'undefined')
		{
			zoom = obj.zoom;
		}
		
		this.add_imagen_base64(obj['descripcion'], zoom);
		if((typeof obj.valor != 'undefined') && (obj.valor != ''))
		{
			this.add_content({text: obj.valor, alignment: 'center', bold: true, fontSize:13});
		}
		this.new_line();
	};
	
	this.addObject_tabla = function(obj)
	{
		var width = obj.columnas.length;

		var contenido_tabla =
				{
					table:
							{
								headerRows: 0,
								widths: [],
								alignment: 'center',
								body: []
							},
					layout: this.mergeAll(this.layout_lineas_grises, this.layout_lineas_delgadas)
				};

		this.contents[this.contents.length] = {text: '\n' + obj.nombre, style: 'tableHeader'};

		// defino los anchos de la tabla
		var widths = this.get_array(width);
		for (var i=0; i < width; i++)
		{
			widths[i] = 'auto';
		}
		contenido_tabla['table']['widths'] = widths;

		// titulos y primera fila de la tabla
		if(obj.titulos.length > 0)
		{
			var titulos = this.get_array(width);
			var i = 0;
			for (var key in obj.titulos)
			{
				titulos[i] = {text: obj.titulos[key], style: 'tableCell'};
				i++;
			}
			contenido_tabla['table']['body'][contenido_tabla['table']['body'].length] = titulos;
		}

		// valores
		for (var rowKey in obj.contenido)
		{
			var contenidoRow = this.get_array(width);
			var i = 0;
			for (var colKey in obj.columnas)
			{
				var contenido = this.formatKeys(obj.columnas[colKey], obj.contenido[rowKey]);

				var alineacion = 'center';
				
				if (typeof obj.alineaciones != 'undefined')
				{
					if (typeof obj.alineaciones[colKey] != 'undefined')
					{
						alineacion = obj.alineaciones[colKey];
					}
				}

				contenidoRow[i] = {text:contenido, style: 'tableCell', alignment:alineacion};
				i++;
			}
			contenido_tabla['table']['body'][contenido_tabla['table']['body'].length] = contenidoRow;
		}

		this.contents[this.contents.length] = {
			columns: [
				{width: '*', text: ''},
				this.mergeAll(contenido_tabla, {width: 'auto'}),
				{width: '*', text: ''}
			]
		};
		this.new_line();
	};
	
	this.addObject_tablaDinamica = function(obj)
	{
		var contenido_tabla =
				{
					table:
							{
								headerRows: 0,
								widths: [],
								alignment: 'center',
								body: []
							},
					layout: this.mergeAll(this.layout_lineas_grises, this.layout_lineas_delgadas)
				};

		this.contents[this.contents.length] = {text: '\n' + obj.nombre, style: 'tableHeader'};

		// valores
		for (var rowKey in obj.contenido)
		{
			var width = obj.contenido[rowKey].length;
			var contenidoRow = this.get_array(width);
			var widths = this.get_array(width);
			var i = 0;
			for (var colKey in obj.contenido[rowKey])
			{
				var contenido = obj.contenido[rowKey][colKey];
				var alineacion = 'center';


				contenidoRow[i] = {text:contenido, style: 'tableCell', alignment:alineacion};
				widths[i] = 'auto';
				i++;
			}
			contenido_tabla['table']['body'][contenido_tabla['table']['body'].length] = contenidoRow;
			// se construye el ancho en cada cilo pero no importa...
			contenido_tabla['table']['widths'] = widths;
		}

		this.contents[this.contents.length] = {
			columns: [
				{width: '*', text: ''},
				this.mergeAll(contenido_tabla, {width: 'auto'}),
				{width: '*', text: ''}
			]
		};
		this.new_line();
	};
	
	this.addObject_h1C = function(obj)
	{
		this.titulo_h1(obj['descripcion']);
	};
	
	this.addObject_h2C = function(obj)
	{
		this.titulo_h2(obj['descripcion']);
	};
	
	this.addObject_h3C = function(obj)
	{
		this.titulo_h3(obj['descripcion']);
	};
	
	this.addObject_h1L = function(obj)
	{
		this.titulo_h1_left(obj['descripcion']);
	};
	
	this.addObject_h2L = function(obj)
	{
		this.titulo_h2_left(obj['descripcion']);
	};
	
	this.addObject_h3L = function(obj)
	{
		this.titulo_h3_left(obj['descripcion']);
	};
	
	this.addObject_tituloDocumento = function(obj)
	{
		this.titulo_principal_grande(obj['descripcion']);
	};
}






/***************************************/
/* Clase para leer tamanos de imagenes */
/***************************************/
function image_size_reader()
{
	this.read_4_bytes = function (bytes, index)
	{
		return (bytes[index] << 24) | (bytes[index + 1] << 16) | (bytes[index + 2] << 8) | bytes[index + 3];
	};

	this.read_2_bytes = function (bytes, index)
	{
		return (bytes[index] << 8) | bytes[index + 1];
	};

	this.getPngDimensions = function (b)
	{
		return {width: this.read_4_bytes(b, 16), height: this.read_4_bytes(b, 20)};
	};
	this.getJpegDimensions = function (b)
	{
		var nIndex;
		var height = 0;
		var width = 0;
		var size = 0;
		var nSize = b.length;

		// marker FF D8  starts a valid JPEG
		if (this.read_2_bytes(b, 0) === 0xFFD8)
		{
			for (nIndex = 2; nIndex < nSize - 1; nIndex += 4)
			{
				if (b[nIndex] === 0xFF/*FF*/ && b[nIndex + 1] === 0xC0 /*C0*/)
				{
					var w = this.read_2_bytes(b, nIndex + 7);
					var h = this.read_2_bytes(b, nIndex + 5);
					if (w * h > size)
					{
						size = w * h;
						width = w;
						height = h;
					}
				}
			}
		}
		return {width: width, height: height};
	};

	this.getDimensions = function (dataURI)
	{
		var def = {width: 0, height: 0};

		if ((dataURI === '') || (dataURI === null))
		{
			return def;
		}


		try
		{
			// convert base64 to raw binary data held in a string
			var byteString = atob(dataURI.split(',')[1]);
			// separate out the mime component
			var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

			// write the bytes of the string to an ArrayBuffer
			var byteArray = new Array(byteString.length);
			for (var i = 0; i < byteString.length; i++)
			{
				byteArray[i] = byteString.charCodeAt(i);
			}

			if (mimeString === 'image/png')
			{
				// soportar PNG
				return this.getPngDimensions(byteArray);
			}
			else if (mimeString === 'image/jpeg')
			{
				// soportar JPEG
				return this.getJpegDimensions(byteArray);
			}
		}
		catch (e)
		{
		}

		// No Soportado
		return def;
	};
}


