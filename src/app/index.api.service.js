/**
 * Created by stackpointer on 7/04/17.
 */
(function(angular) {
    'use strict';

    angular
        .module('arquimetclient')
        .factory('api', ['$http', 'urlBase', api]);

    function api($http, urlBase) {

        var vm = {},
            urlBase = urlBase.base;
        //urlBase = 'api/';
        //urlBase = 'http://arquimet.stackpointer.co:50005/api/';


        //////////////////////////////// @general ////////////////////////////////
        vm.general = {};
        vm.general.unidades = {};
        vm.general.unidades.get = getUnidades;

        //////////////////////////////// @metaldeck ////////////////////////////////
        vm.metaldeck = {};
        vm.metaldeck.calcular = metaldeckCalcularTecnico;
        vm.metaldeck.getJsonStoraged = getMetaldeckJsonStoraged;

        vm.metaldeck.acabados = {};
        vm.metaldeck.acabados.get = getMetaldeckAcabados;

        //////////////////////////////// @propiedades ////////////////////////////////
        vm.propiedades = {};
        vm.propiedades.calcularTecnico = calcularTecnico;
        vm.propiedades.calcularAcademico = calcularAcademico;

        vm.propiedades.solicitaciones = {};
        vm.propiedades.solicitaciones.get = getSolicitaciones;

        vm.propiedades.acabados = {};
        vm.propiedades.acabados.get = getPropiedadesAcabados;
        vm.propiedades.acabados.getPropiedadesGeometricas = getPropiedadesGeometricas;
        vm.propiedades.acabados.getPropiedadesPerfilSelected = getPropiedadesPerfilSelected;
        //TODO eliminar api getPropiedadesEfectivas
        vm.propiedades.acabados.getPropiedadesEfectivas = getPropiedadesEfectivas;

        vm.propiedades.orientaciones = {};
        vm.propiedades.orientaciones.get = getOrientaciones;

        //////////////////////////////// @vientos ////////////////////////////////
        vm.vientos = {};
        vm.vientos.calcular = vientosCalcular;

        vm.correas = {};
        vm.correas.orientaciones = {};
        vm.correas.orientaciones.get = correasGetOrientaciones;
        vm.correas.getCorreasJsonStoraged = getCorreasJsonStoraged;


        return vm;
        /////////////////////////////////////////////////////////

        //////////////////////////////// request GENERAL's subUrl ////////////////////////////////
        function getUnidades() {
            var subUrl = 'general/unidades';
            return $http.get(urlBase + subUrl);
        }

        //////////////////////////////// request METALDECK's subUrl ////////////////////////////////
        function metaldeckCalcularTecnico(body) {
            var subUrl = 'metaldeck';
            return $http.post(urlBase + subUrl, body);
        }

        function getMetaldeckJsonStoraged() {
            return $http.get('app/main/metaldeck/dataCalcular.json');

        }

        function getCorreasJsonStoraged() {
            return $http.get('app/main/correas/dataCalcular.json');
        }

        function getMetaldeckAcabados(idUnidad) {
            var subUrl = 'metaldeck/';
            return $http.get(urlBase + subUrl + idUnidad);
        }

        //////////////////////////////// request PROPIEDADES's subUrl ////////////////////////////////
        function getPropiedadesAcabados(idTipoPerfil) {
            var subUrl = 'propiedades/ObtenerPerfiles/';
            return $http.get(urlBase + subUrl + idTipoPerfil);
        }

        function getPropiedadesGeometricas(idTipoPerfil, idPerfil, orientacion, idUnidad) {
            var subUrl = 'propiedades/ObtenerPropiedadesGeometricas/';
            return $http.get(urlBase + subUrl + idTipoPerfil + '/' + idPerfil + '/' + orientacion + '/' + idUnidad);
        }

        function getPropiedadesPerfilSelected(idTipoPerfil, idPerfil, orientacion) {
            var subUrl = 'propiedades/ObtenerPropiedadesPerfil/';
            return $http.get(urlBase + subUrl + idTipoPerfil + '/' + idPerfil + '/' + orientacion);
        }

        function getPropiedadesEfectivas(body) {
            var subUrl = 'propiedades/CalculoAreaSeccionEfectiva';
            return $http.post(urlBase + subUrl, body);
        }

        function getSolicitaciones(tipoPerfilId) {
            var subUrl = 'propiedades/Solicitaciones/';
            return $http.get(urlBase + subUrl + tipoPerfilId);
        }

        function calcularTecnico(body) {
            var subUrl = 'propiedades/calcularTecnico';
            return $http.post(urlBase + subUrl, body);
        }

        function calcularAcademico(body) {
            var subUrl = 'propiedades/calcularAcademico';
            return $http.post(urlBase + subUrl, body);
        }

        function getOrientaciones(tipoPerfilId) {
            var subUrl = 'propiedades/Orientaciones/';
            return $http.get(urlBase + subUrl + tipoPerfilId);
        }

        //////////////////////////////// request VIENTOS's subUrl ////////////////////////////////
        function vientosCalcular(body) {
            var subUrl = 'vientos/calcular';
            return $http.post(urlBase + subUrl, body);
        }

        //////////////////////////////// request CORREAS's subUrl ////////////////////////////////
        function correasGetOrientaciones(tipoPerfilId) {
            var subUrl = 'correas/Orientaciones/';
            return $http.get(urlBase + subUrl + tipoPerfilId);
        }

    }
})(window.angular);
