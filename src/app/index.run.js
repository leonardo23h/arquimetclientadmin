(function(angular) {
  'use strict';

  angular
    .module('arquimetclient')
    .run(runBlock);

  /** @ngInject */
  function runBlock($rootScope, $http, urlBase) {

    $rootScope.$on('$locationChangeSuccess', function (event, next, current) {
      $http.defaults.headers.post['Content-Type'] = 'application/json';
      $http.defaults.headers.put['Content-Type'] = 'application/json';
      $http.defaults.headers.get = $http.defaults.headers.post;
      $http.defaults.headers.get['Content-Type'] = 'application/json';
      $http.defaults.headers.delete = $http.defaults.headers.post;
      $http.defaults.headers.delete['Content-Type'] = 'application/json';
    });

    $http.get('app/conection-file.json')
      .then(function (response) {
        var data = response.data;
        urlBase.base = data.base + data.api;
      })
      .catch(function (err) {
        console.log(err);
      })
  }

})(window.angular);
