/* global malarkey:false, moment:false */
(function(angular) {
  'use strict';

  angular
    .module('arquimetclient')
    .constant('malarkey', malarkey)
    .constant('moment', moment);

})(window.angular);
