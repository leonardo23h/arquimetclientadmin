(function(angular) {
  'use strict';

  angular
    .module('arquimetclient')
    .config(config);

  /** @ngInject */
  function config($logProvider, toastrConfig, urlBaseProvider) {
    // Enable log
    $logProvider.debugEnabled(true);

    // Set options third-party lib
    toastrConfig.allowHtml = true;
    toastrConfig.timeOut = 3000;
    toastrConfig.positionClass = 'toast-top-right';
    toastrConfig.preventDuplicates = true;
    toastrConfig.progressBar = true;

    urlBaseProvider.base = urlBaseProvider.base + 'api/';

  }

})(window.angular);
