(function(angular) {
    'use strict';

    angular
        .module('arquimetclient', [
            //thrd parts

            'checklist',
            'rzModule',
            'nvd3',
            /////////////////
            'ngAnimate',
            'ngCookies',
            'ngTouch',
            'ngSanitize',
            'ngMessages',
            'ngAria',
            'ui.router',
            'ui.bootstrap',
            'arquimetclient.carousel',
            'toastr',
            'angular.filter',

            //contents
            'app.metaldeck',
            'app.propiedades',
            'app.vientos',
            'app.correas'
        ]);

})(window.angular);
