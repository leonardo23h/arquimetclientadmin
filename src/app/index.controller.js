/**
 * Created by stackpointer on 9/05/17.
 */

(function (angular) {
  'use strict';
  angular
    .module('arquimetclient')
    .controller('IndexController',[
      'activeItem',
      metaldeckController
    ]);

  function metaldeckController(activeItem) {

    var vm = this;

    vm.activeItem = activeItem;

    vm.activeItem.metaldeck = false;
    vm.activeItem.propiedades_mecanicas = true;
    vm.activeItem.title = "Arquimet - Propiedades Mecánicas";

    /*
     * functions
     * */

    /*******************************************************************/

  }

})(window.angular);
