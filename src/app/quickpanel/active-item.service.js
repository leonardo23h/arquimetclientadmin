/**
 * Created by stackpointer on 25/04/17.
 */
(function(angular) {
    angular
        .module('arquimetclient')
        .factory('activeItem', activeItem);

    function activeItem() {
        var vm = {},
            ARQUIMET_TITLE = "Arquimet";

        vm.propiedades_mecanicas = true;
        vm.metaldeck = false;
        vm.vientos = false;
        vm.correas = false;
        vm.viguetas = false;


        vm.setActive = setActive;

        return vm;


        function setActive(nameModule) {
            switch (nameModule) {
                // case "Propiedades":
                //     activePropiedades();
                //     break;

                // case "Metaldeck":
                //     activeMetaldeck();
                //     break;

                // case "Vientos":
                //     activeVientos();
                //     break;

                case "Correas":
                    activeCorreas();
                    break;

                // case "Viguetas":
                //     activeViguetas();
                //     break;

                default:
                    activePropiedades();
                    break;
            }
        }

        // function activePropiedades() {
        //     vm.propiedades_mecanicas = true;

        //     vm.metaldeck = false;
        //     vm.vientos = false;
        //     vm.correas = false;
        //     vm.viguetas = false;

        //     vm.title = ARQUIMET_TITLE + "Propiedades Mecánicas";
        // }

        // function activeMetaldeck() {
        //     vm.metaldeck = true;

        //     vm.propiedades_mecanicas = false;
        //     vm.vientos = false;
        //     vm.correas = false;
        //     vm.viguetas = false;

        //     vm.title = ARQUIMET_TITLE + "Diseño Metaldeck"
        // }

        // function activeVientos() {
        //     vm.vientos = true;

        //     vm.metaldeck = false;
        //     vm.propiedades_mecanicas = false;
        //     vm.correas = false;
        //     vm.viguetas = false;

        //     vm.title = ARQUIMET_TITLE + "Análisis de Vientos"
        // }

        function activeCorreas() {
            vm.correas = true;

            vm.metaldeck = false;
            vm.propiedades_mecanicas = false;
            vm.vientos = false;
            vm.viguetas = false;

            vm.title = ARQUIMET_TITLE + "Diseño de Correas"
        }

        // function activeViguetas() {
        //     vm.viguetas = true;

        //     vm.metaldeck = false;
        //     vm.propiedades_mecanicas = false;
        //     vm.vientos = false;
        //     vm.correas = false;

        //     vm.title = ARQUIMET_TITLE + "Diseño de Viguetas"
        // }

    }
})(window.angular);
