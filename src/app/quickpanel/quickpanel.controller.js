/**
 * Created by Usuario on 02/03/2017.
 */
(function() {
  'use strict';

  angular
    .module('arquimetclient')
    .controller('QuickPanelController', QuickPanelController);

  /** @ngInject */
  function QuickPanelController(activeItem, upFactory) {
    var vm = this;

    vm.AdminLTE = upFactory.AdminLTE;
    vm.activeItem = activeItem;
    /* ------------------
     * - Implementation -
     * ------------------
     * The next block of code implements AdminLTE's
     * functions and plugins as specified by the
     * options above.
     */

    //Easy access to options
    var o = vm.AdminLTE.options;

    //Set up the object
    upFactory.up();

    //Activate sidebar push menu
    vm.AdminLTE.pushMenu.activate(o.sidebarToggleSelector);


    //functions


    ////////////////////////////////////////////////////////////////
  }
})();
