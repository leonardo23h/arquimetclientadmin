/**
 * Created by stackpointer on 25/07/17.
 */

(function (angular) {
  'use strict';

  angular
    .module('app.correas')
    .service('correasPerfileriaService', [
      'api',
      correasPerfileriaService
    ]);

  function correasPerfileriaService(api) {
    var vm = this;

    //methods
    vm.getAcabados = getAcabados;
    vm.getOrientaciones = getOrientaciones;

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    function getAcabados(idTipoPerfil)
    {
      return  api.propiedades.acabados.get(idTipoPerfil)
        .then(getAcabadosComplete)
        .catch(getAcabadosFailed);

      function getAcabadosComplete(response) {
        //La logica puede ser mas compleja antes de resolver el objeto real a entregar para manipular
        return response.data;
      }

      function getAcabadosFailed(err) {
        //La logica puede ser mas compleja antes de resolver error ocurrido en la peticion.
        return err;
      }

    }

    function getOrientaciones(idTipoPerfil) {
      return api.correas.orientaciones.get(idTipoPerfil)
        .then(getOrientacionesComplete)
        .catch(getOrientacionesFailed);

      function getOrientacionesComplete(response) {
        //La logica puede ser mas compleja antes de resolver el objeto real a entregar para manipular
        return response.data;
      }

      function getOrientacionesFailed(err) {
        //La logica puede ser mas compleja antes de resolver error ocurrido en la peticion.
        return err
      }
    }
  }
})(window.angular);
