(function (angular) {
  'use strict';

  angular
    .module('app.correas')
    .service('perfileriaService', [
      'transformService',
      perfileriaService
    ]);

  function perfileriaService(transformService) {
    var vm = this;

    //injects

    //methods
    vm.newPerfileria = newPerfileria;
    vm.setProfilesDistribution = setProfilesDistribution;

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    function newPerfileria(n, gap, large)
    {

      var perfileria = buildNPerfiles(n, large);

      setProfilesDistribution(perfileria, gap);

      return perfileria;
    }

    function buildNPerfiles(n, large) {
      var perfiles = [];

      for(var i = 0; i < n; i++)
      {
        perfiles.push(newShape(large))
      }

      return perfiles;
    }

    function newShape(large)
    {
      var shape = transformService.newInstance();

      //@Override
      shape.scale.x = toScale(large);
      shape.scale.y = '0.6';
      shape.scale.z = '0.5';
      shape.translation.y = '0.3210';
      shape.tipo = "perfil_c.x3d";

      return shape;
    }

    function setProfilesDistribution(perfiles, gap) {
      var count = perfiles.length,
        cubiertaLarge = (count - 1) * gap;

      /**/
      for(var i = 0; i < count; i++)
      {
        if(i === 0)
        {
          perfiles[i].translation.z = cubiertaLarge/2;
        }
        else {
          perfiles[i].translation.z = perfiles[i-1].translation.z - gap;
        }
      }
    }

    function toScale(value) {
      return value / 10;
    }

  }

})(window.angular);
