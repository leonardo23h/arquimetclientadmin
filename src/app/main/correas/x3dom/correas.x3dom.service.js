/**
 * Created by stackpointer on 24/07/17.
 */

/**
 * Created by aesleider on 13/03/2017.
 */
(function (angular) {
  'use strict';

  angular
    .module('app.correas')
    .factory('correasX3dom', [
      'x3dService',
      'viewConnector',
      'lightCorrea',
      correasX3dom
    ]);

  function correasX3dom(x3dService, viewConnector, lightCorrea)
  {
    //object (factory)
    var vm = {};

    //objects (attribs)
    vm.x3d = x3dService.newInstance("x3domCorreasCentralSceneView");


    vm.x3d.scene.superTransform.transforms.push(lightCorrea.newLight(5, 1.7, 6, 10));

    //functions

    //////////////////////////////////////////////// Data ////////////////////////////////////////////////////////

    /////////////////////////////////////////////// Functions  ////////////////////////////////////////////////////

    return vm;

    /*
     * **************************************************************************************************************
     * */

  }

})(window.angular);
