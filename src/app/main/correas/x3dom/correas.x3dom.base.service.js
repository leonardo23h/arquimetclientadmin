(function (angular) {
  'use strict';

  angular
    .module('app.correas')
    .service('baseService', [
      'transformService',
      baseService
    ]);

  function baseService(transformService) {
    var vm = this;

    //injects

    //methods
    vm.newBase = newBase;
    vm.setBasesDistribution = setBasesDistribution;

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    function newBase(large, gap)
    {
      var bases = [ newShape(large), newShape(large) ];

      setBasesDistribution(gap, bases);

      return bases;
    }

    function newShape(large)
    {
      var shape = transformService.newInstance(),
      largeScaled = largeToScale(large);

      //@Override
      shape.scale.y = '0.5';
      shape.scale.x = '0.5';
      shape.scale.z = largeScaled - (largeScaled / 6);
      shape.tipo = "separacion_metaldeck.x3d";

      return shape;
    }

    function setBasesDistribution(gap, bases)
    {
      var count = bases.length;

      for(var i = 0; i < count; i++)
      {
        if(i === 0)
        {
          bases[i].translation.x = gap / 2;
        }
        else {
          bases[i].translation.x = bases[i-1].translation.x - gap;
        }
      }
    }

    function largeToScale(value)
    {
      return value / 4;
    }

  }

})(window.angular);
