(function (angular) {
  'use strict';

  angular
    .module('app.correas')
    .service('cubiertaService', [
      'transformService',
      'baseService',
      'perfileriaService',
      cubiertaService
    ]);

  function cubiertaService(transformService, baseService, perfileriaService) {
    var vm = this;

    //injects

    //methods
    vm.newCubierta = newCubierta;
    vm.setPendiente = setPendiente;

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    function newCubierta(nPerfiles, gapPerfiles, longitudVano, pendiente)
    {
      var cubierta = {},
        cubiertaLarge = getLargoLamina(nPerfiles, gapPerfiles);

      cubierta.transform = newTransformInstance();
      cubierta.lamina = newShape(cubiertaLarge, longitudVano);
      cubierta.bases = __newBases(cubiertaLarge, longitudVano);
      cubierta.perfileria = __newPerfileria(nPerfiles, gapPerfiles, longitudVano);

      setPendiente(cubierta, pendiente);

      return cubierta;
    }

    function newTransformInstance()
    {
      var transform = transformService.newInstance();

      //@Override
      transform.rotation.x = '-1';
      transform.rotation.z = '0';
      transform.rotation.a = '0.174533';

      return transform;
    }

    function newShape(large, width)
    {
      var shape = transformService.newInstance();

      shape.scale.x = widthToScale(width);
      shape.scale.y = widthToScale(width);
      shape.scale.z = largeToScale(large) + 0.1;
      shape.translation.x = '0';
      shape.translation.y = '0.7200';
      shape.translation.z = '0';
      shape.tipo = "cubierta.x3d";

      return shape;
    }

    function __newBases(large, gap)
    {
      return baseService.newBase(large, gap);
    }

    function __newPerfileria(n, gap, large)
    {
      return perfileriaService.newPerfileria(n, gap, large);
    }

    function widthToScale(value)
    {
      return value / 10;
    }

    function largeToScale(value)
    {
      return value / 4;
    }

    function getLargoLamina(nPerfiles, gapPerfiles)
    {
      return (nPerfiles - 1) * gapPerfiles
    }

    function setPendiente(cubierta, grados)
    {
      cubierta.transform.rotation.a = grados * Math.PI / 180;
    }


  }

})(window.angular);
