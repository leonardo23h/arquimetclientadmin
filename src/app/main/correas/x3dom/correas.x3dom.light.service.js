(function (angular) {
  'use strict';

  angular
    .module('app.correas')
    .service('lightCorrea', [
      'transformService',
      'cubiertaService',
      lightCorrea
    ]);

  function lightCorrea(transformService, cubiertaService) {
    var vm = this;

    //injects

    //methods
    vm.newLight = newLight;

    //////////////////////////////////////////////////////////////////////////////////////////////////////
    function newLight(nPerfiles, gapPefiles, longitudVano, pendiente) {
      var light = {},
        cubiertaLarge = (nPerfiles - 1) * gapPefiles / 4;

      light.transform = __newDefaultTransform();
      light.cubierta = __newCubierta(nPerfiles, gapPefiles, longitudVano, pendiente);

      return light;
    }

    function __newCubierta(large, width, nPerfiles, gapPerfiles, pendiente) {
      return cubiertaService.newCubierta(large, width, nPerfiles, gapPerfiles, pendiente);
    }

    function __newDefaultTransform()
    {

      return transformService.newInstance();

    }

  }
})(window.angular);
