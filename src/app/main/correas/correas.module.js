/**
 * Created by stackpointer on 27/06/17.
 */

(function (angular) {
  'use strict';

  angular
    .module('app.correas',[])
    .config(config);

  function config($stateProvider)
  {
    $stateProvider.state('app.correas', {
      url: '/correas',
      views: {
        'content@app': {
          templateUrl: 'app/main/correas/correas.html',
          controller: 'CorreasController as vm'
        },
        'quickationstoolbar@app': {
          templateUrl: 'app/main/correas/quickationstoolbar/quickationstoolbar.html',
          controller: 'CorreasQuickationstoolbarController as vm'
        },
        'design@app.correas': {
          templateUrl: 'app/main/correas/tabDesign/tabDesign.html',
          controller: 'TabCorreasDesignController as vm'
        },
        'results@app.correas': {
          templateUrl: 'app/main/correas/tabResults/tabResults.html',
          controller: 'TabCorreasResultsController as vm'
        },
        'scene@app.correas': {
          templateUrl: 'app/main/correas/scene/scene.html',
          controller: 'SceneCorreasController as vm'
        }
      }
    });
  }

})(window.angular);
