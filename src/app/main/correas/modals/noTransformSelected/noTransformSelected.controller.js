/**
 * Created by aesleider on 30/03/2017.
 */
(function (angular) {
  'use strict';

  angular
    .module('app.metaldeck')
    .controller('noTransformSelected', noTransformSelected);

  function noTransformSelected($uibModalInstance) {
    var vm = this;

    vm.ok = function () {
      $uibModalInstance.close();
    };
  }
})(window.angular);
