/**
 * Created by stackpointer on 25/07/17.
 */

(function(angular) {
  'use strict';

  angular
    .module('app.correas')
    .controller('TraslapoModalController', [
      '$uibModalInstance',
      'correasPerfileria',
      TraslapoModalController
    ]);

  function TraslapoModalController($uibModalInstance, correasPerfileria) {
    var vm = this;

    //injects
    vm.perfileria = correasPerfileria.data;
    vm.derecha = 0.0;

    //data

    //methods
    vm.ok = ok;
    vm.cancelar = cancelar;
    getDataTraslapo()

    ////////////////////////////////////////////////////////////////////////////////////////////////
    //function para obtener los valores por default
    function getDataTraslapo() {
      if (vm.perfileria.traslapoOptimizacionSoftware == true) {
        vm.checkEspUser = false;
        vm.checkRecSoft = true;
      } else if (vm.perfileria.traslapoOptimizacionSoftware == false) {
        vm.checkEspUser = true;
        vm.checkRecSoft = false;
      }
    }

    function ok() {
      vm.perfileria.traslapoOptimizacionSoftware = vm.checkRecSoft;
      $uibModalInstance.close(vm.perfileria);
    }

    function cancelar() {
      $uibModalInstance.dismiss();
    }


  }

})(window.angular);
