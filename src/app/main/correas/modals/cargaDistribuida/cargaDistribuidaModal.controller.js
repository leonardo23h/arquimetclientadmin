/**
 * Created by aesleider on 31/03/2017.
 */
(function (angular) {
  'use strict';

  angular
    .module('app.correas')
    .controller('CargaDistModalInstanceController', CargaDistModalInstanceController);

  function CargaDistModalInstanceController($uibModalInstance, unidadesService) {
    var vm = this;
    vm.carga = {};
    vm.unidades = unidadesService;
    vm.carga.unidades = angular.copy(vm.unidades.unitSelected);
    vm.ok = function () {
      $uibModalInstance.close(vm.carga);
    };

    vm.cancel = function () {
      $uibModalInstance.dismiss('cancel');
    };
  }
})(window.angular);
