/**
 * Created by stackpointer on 27/07/17.
 */
(function(angular) {
  'use strict';

  angular
    .module('app.correas')
    .controller('CorreasGeometriaModalInstanceController', [
      '$uibModalInstance',
      '$filter',
      'correasPerfileria',
      'geometriaData',
      CorreasGeometriaModalInstanceController
    ]);

  function CorreasGeometriaModalInstanceController($uibModalInstance, $filter, correasPerfileria, geometriaData) {
    var vm = this;

    //injects
    vm.geometria = geometriaData.data.geometria;

    //methods
    vm.chngParaCubiertas = chngParaCubiertas;
    vm.chngParaCerramiento = chngParaCerramiento;
    vm.chngUnAgua = chngUnAgua;
    vm.chngDosAguas = chngDosAguas;
    vm.chngPendienteGrados = chngPendienteGrados;
    vm.chngPendientePorcentaje = chngPendientePorcentaje;
    vm.ok = ok;
    vm.cancel = cancel;
    getUsoTipologia();
    getUsoPanel();


    //////////////////////////////////////////////////⁄/////////////////////////⁄////////////////////////

    function getUsoPanel() {
      if (vm.geometria.usoDelPanel === true) {
        vm.paraCubiertas = true;
        vm.paraCerramiento = false;
      } else if (vm.geometria.usoDelPanel === false) {
        vm.paraCubiertas = false;
        vm.paraCerramiento = true;
      }
    }

    function getUsoTipologia() {
      if (vm.geometria.tipologia === true) {
        vm.unAgua = true;
        vm.dosAguas = false;

      } else if (vm.geometria.tipologia === false) {
        vm.unAgua = false;
        vm.dosAguas = true;
      }
    }

    function ok() {
      vm.geometria.usoDelPanel = vm.paraCubiertas;
      vm.geometria.tipologia = vm.unAgua;
      console.log("geometria", vm.geometria);
      $uibModalInstance.close(vm.geometria);
    }

    function cancel() {
      $uibModalInstance.dismiss();
    }

    function chngPendienteGrados() {
      if (vm.paraCerramiento === false) {
        if (vm.geometria.pendienteEnGrados <= 0) {
          vm.geometria.pendienteEnGrados = 0;
        }

        if (vm.geometria.pendienteEnGrados > 89.99) {
          vm.geometria.pendienteEnGrados = 89.99;
        } else {
          vm.geometria.pendienteEnPorcentaje = parseFloat($filter("round")(((Math.tan(vm.geometria.pendienteEnGrados * (Math.PI / 180))) * 100), 2));
        }
      }
    }

    function chngPendientePorcentaje() {
      if (vm.paraCerramiento === false) {
        if (vm.geometria.pendienteEnPorcentaje <= 0) {
          vm.geometria.pendienteEnPorcentaje = 0;
        }

        if (vm.geometria.pendienteEnPorcentaje > 57295779) {
          vm.geometria.pendienteEnPorcentaje = 57295779;
        } else {
          vm.geometria.pendienteEnGrados = parseFloat($filter("round")((180.0 / Math.PI) * Math.atan(vm.geometria.pendienteEnPorcentaje / 100), 2));
        }
      }
    }

    function chngParaCubiertas() {
      switch (vm.paraCubiertas) {
        case true:
          vm.geometria.pendienteEnGrados = 10.00;
          vm.geometria.pendienteEnPorcentaje = 17.63;
          vm.paraCerramiento = false;
          break;

        case false:
          vm.geometria.pendienteEnGrados = 90;
          vm.geometria.pendienteEnPorcentaje = NaN;
          vm.paraCerramiento = true;
          break;
      }
    }

    function chngParaCerramiento() {
      switch (vm.paraCerramiento) {
        case true:
          vm.unAgua = true;
          vm.dosAguas = false;
          vm.paraCubiertas = false;
          vm.geometria.pendienteEnGrados = 90;
          vm.geometria.pendienteEnPorcentaje = NaN;
          break;

        case false:
          vm.geometria.pendienteEnGrados = 10.00;
          vm.geometria.pendienteEnPorcentaje = 17.63;
          vm.paraCubiertas = true;
          break;
      }

    }

    function chngUnAgua() {
      switch (vm.unAgua) {
        case true:
          vm.dosAguas = false;
          break;

        case false:
          vm.dosAguas = true;
          break;
      }

      forceWhenIsParaCerramiento();

    }

    function chngDosAguas() {
      switch (vm.dosAguas) {
        case true:
          vm.unAgua = false;
          break;

        case false:
          vm.unAgua = true;
          break;
      }

      forceWhenIsParaCerramiento();
    }

    function forceWhenIsParaCerramiento() {
      if (vm.paraCerramiento === true) {
        vm.unAgua = true;
        vm.dosAguas = false;
      }
    }

  }

})(window.angular);
