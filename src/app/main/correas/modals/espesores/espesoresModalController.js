/**
 * Created by stackpointer on 5/04/17.
 */
(function(angular) {
  'use strict';
  angular
    .module('app.correas')
    .controller('espesoresModalController', [
      'correasPerfileria',
      'checkList',
      '$filter',
      'espesorList',
      '$uibModalInstance',
      espesoresModalController
    ]);

  function espesoresModalController(correasPerfileria, checkList, $filter, espesorList, $uibModalInstance) {
    var vm = this;

    vm.listaVanos = espesorList.seleccionado;
    vm.grupoListado = espesorList.grupo;

    vm.ok = ok;
    vm.cancel = cancel;

    vm.vanoSelected = vm.grupoListado[0];


    function ok() {
      $uibModalInstance.close(vm.vanoSelected);
    }

    function cancel() {
      $uibModalInstance.dismiss();
    }


  }

})(window.angular);
