/**
 * Created by stackpointer on 11/04/17.
 */
(function (angular) {
  'use strict';

  angular
    .module('app.metaldeck')
    .controller('resultCalculosModalInstanceController', resultCalculosModalInstanceController);

  function resultCalculosModalInstanceController($uibModalInstance, report) {
    var vm = this;
    vm.report = report;



    console(vm.report)

    vm.metaldeckStyle = {
      "width": "80%",
      "height": "30px"
    };

    vm.topLineStyle = {
      "width": "98.5%",
      "height": "10px",
      "background-color": "gray"
    };

    vm.rellenoStyle = {
      "width": "5px",
      "height": "10px",
      "background-color": "red"
    };

    vm.downloadPDF = downloadPDF;
    vm.console = console;

    vm.ok = function () {
      $uibModalInstance.close(report);
    };

    vm.cancel = function () {
      $uibModalInstance.dismiss('cancel');
    };

    function downloadPDF() {
      if(vm.report.report.length == 0){
        _showAlert('Error', 'No hay datos para imprimir');
      }
      else{
        kendo.drawing.drawDOM(".modal-calculos-body", {
          paperSize: "A4",
          margin: "0.5cm",
          scale: 0.6175
        }).then(function(group){
          kendo.drawing.pdf.saveAs(group, "resultado_calculos_metaldeck.pdf");
        });
      }
    }

    function console(expresion) {
      window.console.log(expresion)
    }
  }
})(window.angular);
