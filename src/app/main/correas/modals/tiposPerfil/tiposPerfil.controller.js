/**
 * Created by stackpointer on 25/07/17.
 */

(function(angular) {
  'use strict';

  angular
    .module('app.correas')
    .controller('TiposPerfilModalInstanceController', [
      '$uibModalInstance',
      'correasPerfileria',
      TiposPerfilModalInstanceController
    ]);

  function TiposPerfilModalInstanceController($uibModalInstance, correasPerfileria) {
    var vm = this;

    //injects
    vm.perfileria = correasPerfileria;
   
    //data

    //methods
    vm.cerrar = cerrar;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    function cerrar() {
      $uibModalInstance.close();
    }
  }

})(window.angular);
