/**
 * Created by aesleider on 30/03/2017.
 */
(function (angular) {
  'use strict';
  
  angular
    .module('app.metaldeck')
    .controller('LongVanoModalInstanceController', LongVanoModalInstanceController);
  
  function LongVanoModalInstanceController($uibModalInstance, metalZ) {
    var vm = this;
    vm.metalZ = metalZ;
    vm.ok = function () {
      $uibModalInstance.close(vm.metalZ);
    };
    
    vm.cancel = function () {
      $uibModalInstance.dismiss('cancel');
    };
  }
})(window.angular);
