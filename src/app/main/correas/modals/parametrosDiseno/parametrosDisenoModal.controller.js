/**
 * Created by stackpointer on 4/04/17.
 */
(function(angular) {
  'use strict';

  angular
    .module('app.metaldeck')
    .controller('CorreasParamsDisenoModalInstanceController', CorreasParamsDisenoModalInstanceController);

  function CorreasParamsDisenoModalInstanceController($uibModalInstance, paramDisData) {
    var vm = this;

    vm.tipoparametro = paramDisData.data.parametrosDiseno;
    console.log(paramDisData);
    vm.checkPorUsuario = false;
    //data
    //vm.checkCargaVivaCubierta = true;
    //vm.cargaVivaCubierta = 240;
    //vm.checkCargaMuertaVivaCubierta = false;
    //vm.cargaMuertaViva = 180;
    //vm.checkCargaViento = false;
    //vm.cargaViento = 280;
    //vm.checkCargaGranizo = false;
    //vm.cargaGranizo = 280;
    //vm.checkCoeficienteCBCalculado = true;
    //vm.checkPorUsuario = false;
    //vm.checkPorUsuario = false;
    //vm.coeficienteCB = 1;
    //vm.checkArrugamientoAlma = false;
    //vm.checkSujetoApoyo = false;
    //vm.checkFactorR = false;
    //vm.cargaVientoSuccion = 0.7;
    //vm.cargaSentidoGravedad = 0.9;
    //vm.checkCubiertaPanelRigido = false;
    //vm.checkCalcularEje = false;


    vm.ok = function() {

        console.log("tipoparametro", paramDisData.data.parametrosDiseno)
      $uibModalInstance.close(vm.tipoparametro);
    };

    vm.cancel = function() {
      $uibModalInstance.dismiss('cancel');
    };
  }
})(window.angular);
