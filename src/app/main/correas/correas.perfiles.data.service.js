/**
 * Created by stackpointer on 25/07/17.
 */

(function(angular) {
  'use  strict';

  angular
    .module('app.correas')
    .factory('correasPerfileria', [
      'correasPerfileriaService',
      'api',
      'unidadesService',
      correasPerfileria
    ]);

  function correasPerfileria(correasPerfileriaService, api, unidadesService) {
    var vm = {};

    //injects

    //data
    vm.list = [];
    vm.currentPerfilType = 1;
    vm.orientaciones = {};
    vm.orientaciones.list = [];
    vm.orientaciones.selected = 1;
    vm.selecteds = [];
    vm.perfilDiseno = 1;
    //vm.continuidad = 1;
    //vm.riostas = 1;
    //vm.sujecionCubierta = 1;
    vm.data = {}


    //methods
    vm.chngTipoPerfil = chngTipoPerfil;
    vm.restartPerfilDefault = restartPerfilDefault;
    vm.chngDesignType = chngDesignType;
    vm.chngSujecion = chngSujecion;
    vm.getJsonStoraged = getJsonStoraged;
    vm.chgTipoContinuidad = chgTipoContinuidad;
    vm.chngTipoRiostra= chngTipoRiostra;
    chngTipoPerfil(1)

    return vm;
    //////////////////////////////////////////////////////////////////////////////////////////////////
    function chngTipoPerfil(tipo) {
      return getPerfilesByTipo(tipo)
        .then(function() {
          return getOrientacionesByTipo(tipo)
        })
        .then(function() {
          vm.currentPerfilType = tipo;
          // console.log("inspeccionar tipo de perfil", vm.currentPerfilType);

          return vm.currentPerfilType;
        })
    }

    function chngDesignType(chngTypeDesign, perfilType) {
      return getPerfilesByTipo(perfilType)
        .then(function() {
          return getOrientacionesByTipo(perfilType)
        })
        .then(function() {
          vm.currentPerfilType = perfilType;
          return vm.currentPerfilType;
        })
        .then(function() {
          vm.data.diseno = chngTypeDesign;
          vm.perfilDiseno = chngTypeDesign;
          return vm.data.diseno;
        })
        .then(function() {
          vm.selecteds = [];
          return vm.selecteds;
        })
    }

    function chngSujecion(sujecion) {
      return vm.data.tipoSujecionCubierta = sujecion;
    }

    function chgTipoContinuidad(continuidad) {
      return vm.data.tipoDeContinuidad = continuidad;
    }

    function chngTipoRiostra(riostra){
        return vm.data.tipoRiostra = riostra;
    }

    function getPerfilesByTipo(tipoPerfil) {
      return correasPerfileriaService.getAcabados(tipoPerfil)
        .then(function(response) {
          vm.list = [];
          vm.list = response;
          restartPerfilDefault();
          return vm.list;
        })
    }

    function getOrientacionesByTipo(tipoPerfil) {
      return correasPerfileriaService.getOrientaciones(tipoPerfil)
        .then(function(response) {
          vm.orientaciones = {};
          vm.orientaciones.list = response.orientaciones;
          vm.orientaciones.selected = response.orientaciones[0]

          return vm.orientaciones;
        })
    }


    function restartPerfilDefault() {
      if (vm.selecteds.length == 0) {
        vm.selecteds.push(vm.list[0].perfiles[0]);
      }
    }

    //llamar json por defecto
    function getJsonStoraged() {
      return api.correas.getCorreasJsonStoraged()
        .then(function(response) {
          console.log("inspeccionar los datos del json", response);
          return vm.data = response.data;
        })
    }


  }
})(window.angular);
