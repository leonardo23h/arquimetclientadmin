/**
 * Created by stackpointer on 15/06/17.
 */

(function (angular) {
  'use strict';

  angular
    .module('app.correas')
    .filter('selectedsByTipoP', [
      selectedsByTipoP
    ]);

  function selectedsByTipoP() {
    return function (input, id) {
      var filteredsByTypeP = [];

      input.forEach(function (selected) {
        switch (selected.tipoPerfil.tipoPerfilId === id){
          case true:
            filteredsByTypeP.push(selected);
            break;
        }
      });
      return filteredsByTypeP;
    }
  }
})(window.angular);

/**
 * Created by stackpointer on 15/06/17.
 */
