
(function (angular) {
  'use strict';

  angular
    .module('app.correas')
    .filter('selectedsByAcabadoP', [
      selectedsByTipoP
    ]);

  function selectedsByTipoP() {
    return function (input, id) {
      var filteredsByAcabadoP = [];

      input.forEach(function (selected) {
        switch (selected.acabadoPerfilId === id){
          case true:
            filteredsByAcabadoP.push(selected);
            break;
        }
      });
      return filteredsByAcabadoP;
    }
  }
})(window.angular);