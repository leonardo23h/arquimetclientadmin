/**
 * Created by stackpointer on 16/05/17.
 */

(function() {
  'use strict';

  angular
    .module('app.correas')
    .controller('CorreasQuickationstoolbarController', [
      'unidadesService',
      'correasPerfileria',
      CorreasQuickationstoolbarController
    ]);

  /** @ngInject */
  function CorreasQuickationstoolbarController(unidadesService, correasPerfileria) {
    var vm = this;

    //injects
    vm.unidades = unidadesService;
    vm.correasData = correasPerfileria;

    vm.unidades.reqUnidades();

    //methods
    vm.calcular = calcular;


    (function() {
      vm.correasData.getJsonStoraged();

    })();


    function calcular() {
      vm.correasData.data.unidad = vm.unidades.unitSelected.codigo;
      //console.log("inspeccionar calcular", vm.correasData.data);
      
    }


  }
})();
