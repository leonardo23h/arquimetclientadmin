/**
 * Created by stackpointer on 5/04/17.
 */
(function(angular) {
  'use strict';
  angular
    .module('app.correas')
    .controller('TabCorreasResultsController', [
      'correasPerfileria',
      'checkList',
      '$filter',
      '$uibModal',
      TabCorreasResultsController
    ]);

  function TabCorreasResultsController(correasPerfileria, checkList, $filter, $uibModal) {
    var vm = this;

    //injects
    vm.perfileria = correasPerfileria;

    //data
    vm.isCollapsed = isCollapsed;
    vm.exists = checkList.exists;
    vm.atLeasOne = checkList.atLeasOne;
    vm.content = checkList.content;
    vm.isCollapsedSelecteds = isCollapsedSelecteds;
    vm.toggleSelectPerfil = toggleSelectPerfil;
    // vm.deleteSelecteds = deleteSelecteds;
    vm.getDesignByProfile = vm.perfileria.getDesignByProfile;
    vm.eliminarSeleccionados = eliminarSeleccionados;
    vm.updateSelection = updateSelection;
    vm.openModalChangeEspesor = openModalChangeEspesor;
    vm.isCheckAll = isCheckAll;
    vm.checkDeletePerfil = checkDeletePerfil;
    vm.chngSujecion= vm.perfileria.chngSujecion;

    vm.perfilSeleccionado = [];



    //////////////////////////////////////////////////////////////////////
    function isCollapsed(index) {

      vm.perfileria.list[index].isCollapsed = !vm.perfileria.list[index].isCollapsed;
      for (var i = 0; i < vm.perfileria.list.length; i++) {
        if (i !== index) {
          vm.perfileria.list[i].isCollapsed = true;
        }
      }
    }

    function isCollapsedSelecteds(index) {
      vm.perfileria.selecteds[index].isCollapsed = !vm.perfileria.selecteds[index].isCollapsed;

      for (var i = 0; i < vm.perfileria.selecteds.length; i++) {
        if (i !== index) {
          vm.perfileria.selecteds[i].isCollapsed = true;
        }
      }
    }

    function toggleSelectPerfil(perfil, event) {
      var index = vm.perfileria.selecteds.indexOf(perfil);

      if (event.target.checked === true && !index > -1) //index > -1 means exist
      {
        perfil.checked = false;
        vm.perfileria.selecteds.push(perfil);

      } else if (event.target.checked === false && index > -1) {
        vm.perfileria.selecteds.splice(vm.perfileria.selecteds.indexOf(perfil), 1);
      }

      if (vm.perfileria.selecteds.length == 0) {
        vm.perfileria.restartPerfilDefault();
      }
    }

    //funtion para eliminar los elementos seleccionados
    function eliminarSeleccionados(data) {
      angular.forEach(data, function(value, key) {
        //identificar el indice en el array
        var index = vm.perfileria.selecteds.indexOf(value);
        //si esta en estado verdadero y el index no este indefinido
        if (value.checked === true) {
          //elimina de la posicion el elemento en el array
          vm.perfileria.selecteds.splice(index, 1);
        }
      })
    }

    function updateSelection(grupos, event, index, perfiles) {
      var arrayTemp = [];

      if (event.target.checked === true) {
        angular.forEach(perfiles, function(value, key) {
          if (index !== key) {
            value.checked = false;
            grupos[0].checked = true;
          }
        })
        arrayTemp.push(grupos[0]);
        vm.perfileria.selecteds = arrayTemp;
        vm.perfilSeleccionado = grupos;

      } else if (event.target.checked === false) {
        vm.perfileria.selecteds.splice(vm.perfileria.selecteds.indexOf(grupos[0]), 1);
      }

      if (vm.perfileria.selecteds.length == 0) {
        vm.perfileria.restartPerfilDefault();
      }
    }

    //function para abrir la ventana de cambio de espesor
    function openModalChangeEspesor() {
      var copySelecteds = angular.copy(vm.perfileria.selecteds)
      var parentElem = angular.element(document.body);
      var modalInstance = $uibModal.open({
        animation: true,
        templateUrl: 'app/main/correas/modals/espesores/espesoresModal.html',
        controller: 'espesoresModalController',
        controllerAs: 'vm',
        size: 'md',
        appendTo: parentElem,
        resolve: {
          espesorList: function() {
            var obj = { grupo: vm.perfilSeleccionado, seleccionado: vm.perfileria.selecteds };
            return obj
          }
        }
      });

      //obtener el resultado del modal
      modalInstance.result.then(function(response) {
        var arrayTemp = [];
        angular.forEach(vm.perfilSeleccionado, function(value, key) {
          if (value.espesorFinal === response.espesorFinal) {
            arrayTemp.push(value);
          }
        })
        vm.perfileria.selecteds = arrayTemp;
      })
    }
   
    //function para seleccionar todos los elementos en los acabados
    function isCheckAll(typeAcabado) {
      if (typeAcabado.isCheckAll) {
        checkList.selectPerfiles(vm.perfileria.selecteds, typeAcabado.perfiles);
      } else {
        checkList.deselectPerfiles(vm.perfileria.selecteds, typeAcabado.perfiles, vm.perfileria.restartPerfilDefault);
      }
    }
    
    //function para seleccionar todos los elementos de los acabados en los seleccionados
    function checkDeletePerfil(perfil, acabado, seleccionados, event) {
      if (event.target.checked === true) {
        angular.forEach(seleccionados, function(value, key) {
          if (value.acabadoPerfilId == acabado && value.tipoPerfilId == perfil) {
            value.checked = true;
          }
        })
      }else if(event.target.checked === false){
        angular.forEach(seleccionados, function(value, key) {
          if (value.acabadoPerfilId == acabado && value.tipoPerfilId == perfil) {
            value.checked = false;
          }
        })
      }
    }

  }

})(window.angular);
