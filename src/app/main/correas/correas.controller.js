/**
 * Created by stackpointer on 27/06/17.
 */

(function (angular) {
  'use strict';
  angular
    .module('app.correas')
    .controller('CorreasController',[
      'activeItem',
      CorreasController
    ]);

  function CorreasController(activeItem) {

    var vm = this;

    vm.activeItem = activeItem;

    vm.activeItem.setActive("Correas");
    /*
     * functions
     * */

    /*******************************************************************/

  }

})(window.angular);
