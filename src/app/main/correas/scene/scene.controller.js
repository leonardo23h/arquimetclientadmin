/**
 * Created by stackpointer on 5/04/17.
 */
(function (angular) {
  'use strict';
  angular
    .module('app.correas')
    .controller('SceneCorreasController', [
      'correasInstance',
      SceneCorreasController
    ]);

  function SceneCorreasController(correasInstance) {

    var vm = this;

    //inject properties
    vm.x3d = correasInstance.x3d;
    vm.axes = vm.x3d.axes;
    vm.orientationBx = vm.x3d.orientationBx;
    vm.scene = vm.x3d.scene;
    vm.superTransform = vm.scene.superTransform;


    //data
    vm.sliderZooming = {
      minValue:1,
      maxValue:100,
      value: 95,
      options: {
        showSelectionBar: true,
        floor:1,
        ceil:100,
        step: 1,
        vertical: true,
        onChange: function () {
          vm.x3d.zoom(vm.sliderZooming.value, 80, 95);
        }
      }

    };


    /*
     * functions
     * */

    /*******************************************************************/

  }

})(window.angular);
