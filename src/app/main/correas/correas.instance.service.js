/**
 * Created by stackpointer on 24/07/17.
 */

(function (angular) {
  'use strict';

  angular
    .module('app.correas')
    .factory('correasInstance', [
      'correasX3dom',
      correasInstance
    ]);

  function correasInstance(correasX3dom) {
    var vm = {};

    //injects
    vm.x3d = correasX3dom.x3d;
    vm.axes = vm.x3d.axes;
    vm.orientationBx = vm.x3d.orientationBx;
    vm.scene = vm.x3d.scene;
    vm.superTransform = vm.scene.superTransform;

    //data


    //methods


    return vm;
    /////////////////////////////////////////////////////////////////////////////////////////////////////////
  }
})(window.angular);
