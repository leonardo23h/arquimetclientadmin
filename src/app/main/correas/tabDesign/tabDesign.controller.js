/**
 * Created by stackpointer on 5/04/17.
 */
/**
 * Created by aesleider on 30/03/2017.
 */
(function(angular) {
  'use strict';
  angular
    .module('app.correas')
    .controller('TabCorreasDesignController', [
      '$uibModal',
      '$log',
      'correasPerfileria',
      'unidadesService',
      TabCorreasDesignController
    ]);

  function TabCorreasDesignController($uibModal, $log, correasPerfileria, unidadesService) {

    var vm = this;

    //inject properties
    vm.perfilDesign = correasPerfileria;
    vm.getSelectPerfilUniforme = vm.perfilDesign.getSelectPerfilUniforme;
    vm.chngDesignType = vm.perfilDesign.chngDesignType
    vm.chngSujecion = vm.perfilDesign.chngSujecion;
    vm.chgTipoContinuidad = vm.perfilDesign.chgTipoContinuidad;
    vm.chngTipoRiostra = vm.perfilDesign.chngTipoRiostra;
    vm.unidades = unidadesService;

    //variable toolbar 
    ;
    console.log("inspeccionar perfileria data", vm.perfilDesign)

    //data
    vm.hrefDownload = "data:application/octet-stream," + encodeURIComponent();

    //methods
    vm.downloadData = downloadData;
    vm.calcular = calcular;
    vm.openTiposPerfil = openTiposPerfil;
    vm.openParamsDiseno = openParamsDiseno;
    vm.openGeometria = openGeometria;
    vm.openLongVano = openLongVano;
    vm.openCargaDistribuida = openCargaDistribuida;
    vm.openGenerarReporte = openGenerarReporte;
    vm.openTraslapo = openTraslapo;

    /*******************************************************************/

    (function() {
      vm.perfilDesign.getJsonStoraged();

    })();

    function downloadData() {
      var contenidoDeArchivo = vm.dataMetaldeck.data;
      var elem = angular.element('<a/>');

      elem.attr({
        href: 'data:attachment/octet-stream;charset=utf-8,' + encodeURI(JSON.stringify(contenidoDeArchivo)),
        download: "data_metaldeck_calculo.json"
      })[0].click();
    }

    /*function anyTransformSelected() {
      var validar = false;

      if (vm.superTransform.transformSelected.length > 0)
        validar = true;

      return validar;
    }

    function openNoTransformSelected() {
      var parentElem = angular.element(document.body);
      $uibModal.open({
        animation: true,
        templateUrl: 'app/main/metaldeck/modals/noTransformSelected/noTransformSelected.html',
        controller: 'noTransformSelected',
        controllerAs: 'vm',
        size: 'sm',
        appendTo: parentElem
      });
    }
    */

    function openTraslapo() {
      console.log("ingresando al controlador traslapo")
      vm.chgTipoContinuidad(3);
      var parentElem = angular.element(document.body);
      $uibModal.open({
        animation: true,
        templateUrl: 'app/main/correas/modals/traslapo/traslapo.html',
        controller: 'TraslapoModalController',
        controllerAs: 'vm',
        size: 'sm',
        appendTo: parentElem,
        resolve: {
          paramTranslapoData: vm.perfilDesign
        }
      });
    }

    function openTiposPerfil() {

      var parentElem = angular.element(document.body);
      var modalInstance = $uibModal.open({
        animation: true,
        templateUrl: 'app/main/correas/modals/tiposPerfil/tiposPerfil.html',
        controller: 'TiposPerfilModalInstanceController',
        controllerAs: 'vm',
        size: 'md',
        appendTo: parentElem,
        resolve: {
          // correasPerfileria:function(){

          // }
        }
      });

    }

    function openParamsDiseno() {

      var parentElem = angular.element(document.body);
      var modalInstance = $uibModal.open({
        animation: true,
        templateUrl: 'app/main/correas/modals/parametrosDiseno/parametrosDisenoModal.html',
        controller: 'CorreasParamsDisenoModalInstanceController',
        controllerAs: 'vm',
        size: 'md',
        appendTo: parentElem,
        resolve: {
          paramDisData: vm.perfilDesign
        }
      });

      modalInstance.result.then(function(parametros) {}, function() {
        $log.info('Modal dismissed at: ' + new Date());
      });


    }

    function openGeometria() {
      var parentElem = angular.element(document.body);
      var modalInstance = $uibModal.open({
        animation: true,
        templateUrl: 'app/main/correas/modals/geometria/geometria.html',
        controller: 'CorreasGeometriaModalInstanceController',
        controllerAs: 'vm',
        size: 'md',
        appendTo: parentElem,
        resolve: {
          geometriaData: vm.perfilDesign
        }
      });

      modalInstance.result.then(function(geometria) {

      }, function() {
        $log.info('Modal dismissed at: ' + new Date());
      });

    }

    function openLongVano() {
      if (anyTransformSelected()) {
        var parentElem = angular.element(document.body);
        var modalInstance = $uibModal.open({
          animation: true,
          templateUrl: 'app/main/metaldeck/modals/longitudVano/longitudVanoModal.html',
          controller: 'LongVanoModalInstanceController',
          controllerAs: 'vm',
          size: 'sm',
          appendTo: parentElem,
          resolve: {
            metalZ: function() {
              vm.metalZ = parseFloat(vm.superTransform.transformSelected[0].metaldeck.scale.z);
              return vm.metalZ;
            }
          }
        });

        modalInstance.result.then(function(metalZ) {
          vm.metalZ = metalZ;
          vm.dataMetaldeck.chngLightSizeBySelecteds(metalZ);
        }, function() {
          $log.info('Modal dismissed at: ' + new Date());
        });
      } else {
        openNoTransformSelected();
      }

    }

    function openCargaDistribuida() {

      //if (anyTransformSelected()) {
        var parentElem = angular.element(document.body);
        var modalInstance = $uibModal.open({
          animation: true,
          templateUrl: 'app/main/correas/modals/cargaDistribuida/cargaDistribuidaModal.html',
          controller: 'CargaDistModalInstanceController',
          controllerAs: 'vm',
          size: 'md',
          appendTo: parentElem,
          resolve: {
          cargaDistribuidaData: vm.perfilDesign
        }
        });

        modalInstance.result.then(function(carga) {
          vm.dataMetaldeck.aggCargaDistribuida(carga)
        }, function() {
          $log.info('Modal dismissed at: ' + new Date());
        });
     // } else {
       // openNoTransformSelected();
      //}

    }

    function openGenerarReporte() {

      var parentElem = angular.element(document.body);
      var modalInstance = $uibModal.open({
        animation: true,
        templateUrl: 'app/main/metaldeck/modals/resultCalculosModal/resultCalculosModal.html',
        controller: 'resultCalculosModalInstanceController',
        controllerAs: 'vm',
        size: 'lg',
        appendTo: parentElem,
        resolve: {
          report: vm.dataMetaldeck
        }
      });

      modalInstance.result.then(function(report) {

      }, function() {
        $log.info('Modal dismissed at: ' + new Date());
      });
    }

    function calcular() {
      vm.perfilDesign.data.unidad = vm.unidades.unitSelected.codigo;
      console.log("inspeccionar calculo en tab design", vm.perfilDesign.data);

    }


  }

})(window.angular);
