/**
 * Created by stackpointer on 15/05/17.
 */
/**
 * Created by stackpointer on 26/04/17.
 */
(function (angular) {
  'use strict';

  angular
    .module('app.propiedades')
    .factory('chartService',
      [
        chartService
      ]);

  function chartService()
  {
    var vm = {};

    //data
    vm.scope = {};

    //functions
    vm.convertToXY = convertToXY;
    vm.getDataNVD3 = getDataNVD3;
    vm.getOptionsNVD3 = getOptionsNVD3;


    return  vm;
    ///////////////////////////////////////////////////////////////////////////////

    function getOptionsNVD3(xLabel, yLabel) {

      return {
        chart: {
          type: 'multiChart',
          height:350,
          margin : {
            top: 20,
            right: 45,
            bottom: 60,
            left: 120
          },
          //useInteractiveGuideline: true,
          tooltip:{
            contentGenerator: function(d){

              var html =
                "<table>" +
                "<thead>";

              d.series.forEach(function(elem){
                html +=
                  "<tr>"+
                  "<td class='legend-color-guide'><div style='background-color:" + elem.color + "'></div></td>" +
                  "<td class='key'>" + elem.key + "</td>"+
                  "<td class='value'>" + yLabel + " : " + d3.format('.02f')(elem.value) + "</td>"+
                  '</tr>'
              });

              html += "</thead>"+
                "<tbody>" +
                "<tr>"+
                "<th colspan='3'>" + "<strong class='x-value'>" + xLabel + " : </strong>" + d3.format('.02f')(d.value) +
                "</th>"+
                "</tr>"+
                '</tbody>'+
                "</table>";

              return html;
            }
          },
          duration: 500,
          xAxis: {
            tickFormat: function(d){
              return d3.format(',.2f')(d);
            }
          },
          yAxis1: {
            tickFormat: function(d){
              return d3.format(',.2f')(d);
            },
            axisLabelDistance: 45
          },
          yAxis2: {
            tickFormat: function(d){
              return d3.format(',.2f')(d);
            },
            axisLabelDistance: 45
          }
        }
      };

    }

    function getDataNVD3(data, key, removable, isSolicitacion)
    {
      var chartData = {};
      chartData.values = convertToXY(assertChartData(data));      //values - represents the array of {x,y} data points
      chartData.key = key; //key  - the name of the series.
      chartData.type = getTypeChart(data);
      chartData.removable = removable;
      chartData.isSolicitacion = isSolicitacion;
      chartData.strokeWidth = getStrokeWidth(data, isSolicitacion);
      chartData.yAxis = 1;

      return chartData;
    }

    function getStrokeWidth(data, isSolicitacion) {
      var strokeWidth = 2;
      if(data[0].length > 2 && isSolicitacion === true)
      {
        strokeWidth = 1;
      }

      return strokeWidth;
    }

    //se obtiene el tipo de grafica a dibujar
    function getTypeChart(data) {
      var typeChart = "line";
      if(data[0].length == 2)
      {
        typeChart = "scatter";
      }

      return typeChart;
    }

    //asegura que el array x, y tenga más de 1 elemento tanto en arrayX y arrayY.
    function assertChartData(charData) {

      if(charData[0].length == 1)
      {
        charData[0].splice(0, 0, 0);
        charData[1].splice(0, 0, 0);
      }

      return charData;
    }

    //se necesita que el valor de cada punto en la grafica sea un array con objetos (x,y) para dibujar.
    function convertToXY(data)
    {
      var arrayData = [];

      if(Array.isArray(data)){
        var length = data[0].length;

        for(var i = 1; i < length; i++)
        {
          arrayData.push({x:data[0][i], y:data[1][i]})
        }

      }else if(typeof  data == 'number'){
        arrayData.push({x:0.5 , y:data})
      }

      return arrayData;
    }

  }

})(window.angular);
