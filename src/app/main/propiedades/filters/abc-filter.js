(function (angular) {
  'use strict';

  angular
    .module('app.propiedades')
    .filter('abcFilter', [
      abcFilter
    ]);

  function abcFilter()
  {
    return function (input, acabado) {
      var abc = input.split(","), name = "";
      switch (acabado)
      {
        case "Galvanizado":
          name = "PAG";
          break;
        case "Negro":
          name = "PHR";
          break;
      }

      return name + ' ' + abc[0] + 'x' + abc[1] + 'x' + abc[2];
    }
  }
})(window.angular);
