(function (angular) {
  'use strict';

  angular
    .module('app.propiedades')
    .filter('metalTubRectFilter', [
      '$parse',
      'filterWatcher',
      metalTubRectFilter
    ]);

  function metalTubRectFilter($parse, filterWatcher)
  {

    return function (collection, property) {

      if(!angular.isObject(collection) || angular.isUndefined(property)) {
        return collection;
      }

      return filterWatcher.isMemoized('metalTubRectFilter', arguments) ||
        filterWatcher.memoize('metalTubRectFilter', arguments, this,
          _groupByAB(collection, $parse(property)));

      /**
       * groupByAB function
       * @param collection
       * @param getter
       * @returns {{}}
       */
      function _groupByAB(collection, getter) {
        var result = {};
        var prop;

        angular.forEach( collection, function( elm ) {
          prop = getter(elm);

          if (prop[0] === prop[1]) {
            if(!result.Cuadrado) {
              result.Cuadrado = [];
            }
            result.Cuadrado.push(elm)
          }else {
            if(!result.Rectangular) {
              result.Rectangular = [];
            }
            result.Rectangular.push(elm);
          }
        });
        return result;
      }
    }
  }
})(window.angular);
