(function (angular) {
  'use strict';

  angular
    .module('app.propiedades')
    .filter('metalTubFami', [
      metalTubFami
    ]);

  function metalTubFami()
  {
    return function (input) {
      var abc = input.split(",");

      return abc[0] + 'x' + abc[1];
    }
  }
})(window.angular);
