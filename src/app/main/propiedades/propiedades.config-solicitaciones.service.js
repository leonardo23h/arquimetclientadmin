/**
 * Created by stackpointer on 26/05/17.
 */

(function (angular) {
  "use strict";

  angular
    .module('arquimetclient')
    .service('configSolicitaciones', [
      configSolicitaciones
    ]);

  function configSolicitaciones() {

    var vm = this;

    //functions
    vm.getConfiguracion = getConfiguracionSolicitacion;

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    function getConfiguracionSolicitacion(solicitacion) {
      var configuration = {};

      switch (solicitacion){
        case 1:
          configuration.components = [];
          configuration.components.push({key:"puC", descripcion: "Pu", valor: 0, type:"text"});
          configuration.components.push({key:"klC", descripcion: "Kl", valor: 0, type:"text"});
          configuration.xAxisLabel = "Longitud Efectiva Kl";
          configuration.yAxisLabel = "ØPn";
          break;
        case 2:
          configuration.components = [];
          configuration.components.push({key:"muF", descripcion: "Mu", valor: 0, type:"text"});
          configuration.components.push({key:"lbF", descripcion: "Lb", valor: 0, type:"text"});
          configuration.xAxisLabel = "Longitud No Arriostrada";
          configuration.yAxisLabel = "ØMn";
          break;
        case 3:
          configuration.components = [];
          configuration.components.push({key:"vuC", descripcion: "Vu", valor: 0, type:"text"});
          configuration.xAxisLabel = "";
          configuration.yAxisLabel = "ØVn";
          break;
        case 4:
          configuration.components = [];
          configuration.components.push({key:"tuT", descripcion: "Tu", valor: 0.001, type:"text"});
          configuration.xAxisLabel = "";
          configuration.yAxisLabel = "ØTn";
          break;
        case 5:
          configuration.components = [];
          configuration.components.push({key:"pwA", descripcion: "Pw", valor: 0, type:"text"});
          configuration.components.push({key:"apoyoA", descripcion: "Apoyo", valor: 0, type:"text"});
          configuration.components.push({key:"cargasOpuestasEspaciadasA", descripcion: "Cargas opuestas espaciadas > 1.5 h", valor: true, type:"checkbox"});
          configuration.components.push({key:"sujetoAlApoyoA", descripcion: "Sujeto al apoyo", valor: true, type:"checkbox"});
          configuration.components.push({key:"localizacionApoyoExteriorA", descripcion: "Localización del apoyo", valor: true, type:"radio", radios: [ {descripcion:"Exterior"},{descripcion:"Interior"} ]});
          configuration.xAxisLabel = "Longitud de Apoyo";
          configuration.yAxisLabel = "ØPn";
          break;
        case 6:
          configuration.components = [];
          configuration.components.push({key:"vuFyC",descripcion: "Vu", valor: 0, type:"text"});
          configuration.components.push({key:"muFyC",descripcion: "Mu", valor: 0, type:"text"});
          configuration.components.push({key:"lbFyC",descripcion: "Lb", valor: 0, type:"text"});
          configuration.xAxisLabel = "Mu/ØMn";
          configuration.yAxisLabel = "Vu/ØVn";
          break;
        case 7:
          configuration.components = [];
          configuration.components.push({key:"muAyF", descripcion: "Mu", valor: 0, type:"text"});
          configuration.components.push({key:"pwAyF", descripcion: "Pw", valor: 0, type:"text"});
          configuration.components.push({key:"apoyoAyF", descripcion: "Apoyo", valor: 0, type:"text"});
          configuration.components.push({key:"cargasOpuestasEspaciadasAyF", descripcion: "Cargas opuestas espaciadas > 1.5 h", valor: false, type:"checkbox"});
          configuration.components.push({key:"sujetoAlApoyoAyF", descripcion: "Sujeto al apoyo", valor: true, type:"checkbox"});
          configuration.components.push({key:"localizacionApoyoExteriorAyF", descripcion: "Localización del apoyo", valor: true, type:"radio", radios: [ {descripcion:"Exterior"},{descripcion:"Interior"} ]});
          configuration.xAxisLabel = "Mu/ØMnxu";
          configuration.yAxisLabel = "Pu/ØPn";
          break;
        case 8:
          configuration.components = [];
          configuration.components.push({key:"trT",descripcion: "Tr", valor: 0, type:"text"});
          configuration.components.push({key:"lT",descripcion: "L", valor: 0, type:"text"});
          configuration.xAxisLabel = "Longitud L";
          configuration.yAxisLabel = "ØTr";
          break;
        case 9:
          configuration.components = [];
          configuration.components.push({key:"puTyCyFyC",descripcion: "Pu", valor: 0, type:"text"});
          configuration.components.push({key:"muTyCyFyC",descripcion: "Mu", valor: 0, type:"text"});
          configuration.components.push({key:"vuTyCyFyC",descripcion: "Vu", valor: 0, type:"text"});
          configuration.components.push({key:"trTyCyFyC",descripcion: "Tr", valor: 0, type:"text"});
          configuration.components.push({key:"lTyCyFyC",descripcion: "L", valor: 0, type:"text"});
          configuration.xAxisLabel = "Relación Cortante - Torsión";
          configuration.yAxisLabel = "Relacion Flexión - Compresión";
          break;
      }

      return configuration;
    }


    function getConfiguracion(solicitacion)
    {

      return new Promise(function (resolve, reject)
      {
        buildCompresion(solicitacion)
          .then(function (id) { return validate(id, buildFlexion) })
          .then(function (id) { return validate(id, buildCortante) })
          .then(function (id) { return validate(id, buildTension) })
          .then(function (id) { return validate(id, buildArrugamiento) })
          .then(function (id) { return validate(id, buildFlexionCortante) })
          .then(function (id) { return validate(id, buildArrugamientoFlexion) })
          .then(function (id) { return validate(id, buildMxMyCompresion) })
          .then(function (id) { return validate(id, buildMxMyTension) })
          .then(function (id) { return validate(id, buildTorsion) })
          .then(function (id) { return validate(id, buildTorsionCortanteFlexionCompresion) })
          .catch(function (configurationComponents) {
            resolve(configurationComponents);
          });
      })

    }

    function validate(solicitacion, buildConfiguration)
    {
      var buildResult = buildConfiguration(solicitacion);
      return new Promise(function (resolve, reject) {
        if(buildResult.length == 0)
        {
          resolve(solicitacion)
        }

        reject(buildResult);
      });
    }


    function buildCompresion(solicitacion)
    {
      var configurationsComponents = [];

      if(solicitacion == 1)
      {
        configurationsComponents = [];
        configurationsComponents.push({key:"pu", descripcion: "Pu", type:"text"});
        configurationsComponents.push({key:"kl", descripcion: "Kl", type:"text"});
      }
      return configurationsComponents;
    }

    function buildFlexion(solicitacion)
    {
      var configurationsComponents = [];
      if(solicitacion == 2)
      {
        configurationsComponents = [];
        configurationsComponents.push({key:"mu", descripcion: "Mu", type:"text"});
        configurationsComponents.push({key:"lb", descripcion: "Lb", type:"text"});
      }
      return configurationsComponents;
    }

    function buildCortante(solicitacion)
    {
      var configurationsComponents = [];
      if(solicitacion == 3)
      {
        configurationsComponents = [];
        configurationsComponents.push({key:"vu", descripcion: "Vu", type:"text"});
      }
      return configurationsComponents;
    }

    function buildTension(solicitacion)
    {
      var configurationsComponents = [];
      if(solicitacion == 4)
      {
        configurationsComponents = [];
        configurationsComponents.push({key:"tu", descripcion: "Tu", type:"text"});
        reject(configurationsComponents);
      }
      return configurationsComponents;
    }

    function buildArrugamiento(solicitacion)
    {
      var configurationsComponents = [];
      if(solicitacion == 5)
      {
        configurationsComponents = [];
        configurationsComponents.push({key:"pw", descripcion: "Pw", type:"text"});
        configurationsComponents.push({key:"apoyo", descripcion: "Apoyo", type:"text"});
        configurationsComponents.push({key:"cargasOpuestasEspaciadas", descripcion: "Cargas opuestas espaciadas > 1.5 h", type:"checkbox"});
        configurationsComponents.push({key:"sujetoAlApoyo", descripcion: "Sujeto al apoyo", type:"checkbox"});
        configurationsComponents.push({key:"localizacion", descripcion: "Localización del apoyo", type:"radio", radios: [ {descripcion:"exterior"},{descripcion:"interior"} ]});
      }
      return configurationsComponents;
    }

    function buildFlexionCortante(solicitacion)
    {
      var configurationsComponents = [];
      if(solicitacion == 6)
      {
        configurationsComponents = [];
        configurationsComponents.push({key:"vu",descripcion: "Vu", type:"text"});
        configurationsComponents.push({key:"mu",descripcion: "Mu", type:"text"});
        configurationsComponents.push({key:"lb",descripcion: "Lb", type:"text"});
      }
      return configurationsComponents;
    }

    function buildArrugamientoFlexion(solicitacion)
    {
      var configurationsComponents = [];
      if(solicitacion == 7)
      {
        configurationsComponents = [];
        configurationsComponents.push({key:"mu", descripcion: "Mu", type:"text"});
        configurationsComponents.push({key:"pw", descripcion: "Pw", type:"text"});
        configurationsComponents.push({key:"apoyo", descripcion: "Apoyo", type:"text"});
        configurationsComponents.push({key:"cargasOpuestasEspaciadas", descripcion: "Cargas opuestas espaciadas > 1.5 h", type:"checkbox"});
        configurationsComponents.push({key:"sujetoAlApoyo", descripcion: "Sujeto al apoyo", type:"checkbox"});
        configurationsComponents.push({key:"localizacion", descripcion: "Localización del apoyo", type:"radio", radios: [ {descripcion:"exterior"},{descripcion:"interior"} ]});
      }
      return configurationsComponents;
    }

    function buildMxMyCompresion(solicitacion)
    {
      var configurationsComponents = [];
      if(solicitacion == 8)
      {
        configurationsComponents = [];
        configurationsComponents.push({key:"pu",descripcion: "Pu", type:"text"});
        configurationsComponents.push({key:"kl",descripcion: "KL", type:"text"});
        configurationsComponents.push({key:"Mux",descripcion: "Mux", type:"text"});
        configurationsComponents.push({key:"Muy",descripcion: "Muy", type:"text"});
      }
      return configurationsComponents;
    }

    function buildMxMyTension(solicitacion)
    {
      var configurationsComponents = [];
      if(solicitacion == 9)
      {
        configurationsComponents = [];
        configurationsComponents.push({key:"tu",descripcion: "Tu", type:"text"});
        configurationsComponents.push({key:"kl",descripcion: "KL", type:"text"});
        configurationsComponents.push({key:"Mux",descripcion: "Mux", type:"text"});
        configurationsComponents.push({key:"Muy",descripcion: "Muy", type:"text"});
      }
      return configurationsComponents;
    }

    function buildTorsion(solicitacion)
    {
      var configurationsComponents = [];
      if(solicitacion == 10)
      {

      }
    }

    function buildTorsionCortanteFlexionCompresion(solicitacion)
    {
      var configurationsComponents = [];
      if(solicitacion == 11)
      {

      }
    }

  }
})(window.angular);
