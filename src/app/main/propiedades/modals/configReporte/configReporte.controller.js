(function (angular) {
  'use strict';

  angular
    .module('app.propiedades')
    .controller('configReporteInstanceController', [
      'solicitaciones',
      '$uibModalInstance',
      'checkList',
      configReporteInstanceController
    ]);

  function configReporteInstanceController(solicitaciones, $uibModalInstance, checkList)
  {
    var vm = this;
    vm.solicitaciones = solicitaciones;

    //data
    vm.isTecnico = true;
    vm.idSolicitacionesList = [];

    vm.idSolicitacionesList.push(vm.solicitaciones.selected.codigo);

    //methods
    vm.ok = ok;
    vm.cancel = cancel;
    vm.toggleSelectSolicitacion = toggleSelectSolicitacion;
    vm.exists = checkList.exists;
    vm.atLeasOne = checkList.atLeasOne;

    ////////////////////////////////////////////////////////////////////////////
    function ok(isTecnico)
    {
      var configReporte = {};
      configReporte.isTecnico = isTecnico;
      configReporte.solicitaciones = vm.idSolicitacionesList;
      $uibModalInstance.close(configReporte);
    }

    function cancel()
    {
      $uibModalInstance.dismiss();
    }

    function toggleSelectSolicitacion(idSolicitacion, event)
    {
      var index = vm.idSolicitacionesList.indexOf(idSolicitacion);

      if(event.target.checked === true && ! index > -1) //index > -1 means exist
      {
        vm.idSolicitacionesList.push(idSolicitacion);
      }
      else if (event.target.checked === false && index > -1 )
      {
        vm.idSolicitacionesList.splice(index, 1);
      }

      if (vm.idSolicitacionesList.length === 0){
        vm.idSolicitacionesList.push(vm.solicitaciones.selected.codigo);
      }
    }

  }

})(window.angular);
