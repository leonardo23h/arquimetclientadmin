(function (angular) {
  "use strict";

  angular
    .module('app.propiedades')
    .controller('PropiedadesEfectivasModalInstanceController', [
      "$uibModalInstance",
      "propiedadesEfectivas",
      PropiedadesEfectivasModalInstanceController
    ]);

  function PropiedadesEfectivasModalInstanceController($uibModalInstance, propiedadesEfectivas) {
    var vm = this;

    //injects
    vm.propiedadesEfectivas = propiedadesEfectivas;

    //data

    //methods
    vm.ok = ok;

    ////////////////////////////////////////////////////////////////////////////////////
    function ok() {
      $uibModalInstance.close();
    }
  }
})(window.angular);
