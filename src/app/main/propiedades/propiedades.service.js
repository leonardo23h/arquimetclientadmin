/**
 * Created by stackpointer on 27/05/17.
 */

(function (angular) {
  "use strict";

  angular
    .module('app.propiedades')
    .service('propiedades', [
      'api',
      'chartService',
      propiedades
    ]);

  function propiedades(api, chartService) {
    var vm = this;

    //methods
    vm.calcular = calcularTecnico;
    vm.calcularAcademico = calcularAcademico;
    vm.exportData = generarExcel;
    vm.getOrientaciones = getOrientaciones;

    ////////////////////////////////////////////////////////////////////////////////////////

    function calcularTecnico(perfiles, unidadId, solicitacion, orientacion)
    {
      var json = {};
      json.perfiles = perfiles;
      json.unidadId = unidadId;
      json.solicitacionesId = [solicitacion.codigo];
      json.configuracion = solicitacion.configuration.components;
      json.orientacion = orientacion;

      return api.propiedades.calcularTecnico(json)
        .then(calcularTecnicoComplete)
        .catch(calcularTecnicoFailed);

      function calcularTecnicoComplete(response)
      {
        solicitacion.reporteTecnico = response.data;

        setChartOfResults(solicitacion);

        return solicitacion;
      }

      function calcularTecnicoFailed(err)
      {
        return err;
      }
    }

    function calcularAcademico(perfiles, unidadId, solicitacion, orientacion)
    {
      var json = {};
      json.perfiles = perfiles;
      json.unidadId = unidadId;
      json.solicitacionesId = [solicitacion.codigo];
      json.configuracion = solicitacion.configuration.components;
      json.orientacion = orientacion;

      return api.propiedades.calcularAcademico(json)
        .then(calcularAcademicoComplete)
        .catch(calcularAcademicoFailed);

      function calcularAcademicoComplete(response)
      {
        solicitacion.reporteAcademico = response.data;

        return solicitacion;
      }

      function calcularAcademicoFailed(err)
      {
        return err;
      }
    }

    function getOrientaciones(tipoPerfilId) {
      return api.propiedades.orientaciones.get(tipoPerfilId)
        .then(getOrientacionesComplete)
        .catch(getOrientacionesFailed);

      function getOrientacionesComplete(response) {

      }

      function getOrientacionesFailed(err) {

      }
    }

    ///////////////////////// OTHER FUNCTIONS /////////////////////////

    /*
    *
    * Se encarga de montar todos los resultados a la graficadora
    * @param solicitacion --> recibe la solicitacion con la cual se estan obteniendo los calculos Tecnicos.
    *
    * 1. remueve los objetos de la grafica anterior, si los hay, para la solicitacion dada.
    * 2. envia cada perfil a dibujar.
    * 3. envia el objeto obtenido del calculo tecnico.
    * 4. establece los colores para cada objeto dibujado en la grafica.
    *
    * */
    function setChartOfResults(solicitacion)
    {
      //paso 1
      deleteRemovableCharts(solicitacion.chart.data.length, solicitacion.chart.data);

      //paso 2
      for(var i = 0; i < solicitacion.reporteTecnico.resultado.length; i++)
      {
        setChartByPerfilResult(solicitacion.reporteTecnico.resultado[i].perfiles, solicitacion);
      }

      //paso3
      setChartBySolicitacionResult(solicitacion.reporteTecnico.resultado[0], solicitacion);

      //paso 4
      setColorDataObjects(solicitacion);
    }

    /*
    *
    * Retira los objetos dibujados en una grafica anterior para la solicitacion dada.
    *
    * @param length --> largo del array de objetos dibujados en la grafica anterior.
    * @param chartSolicitacion --> array de objetos que sera retirado.
    *
    * */
    function deleteRemovableCharts(length, chartSolicitacion) {
      var index = length - 1;

      if(index  >= 0 && chartSolicitacion[index].removable)
      {
        chartSolicitacion.splice(index, 1);
        deleteRemovableCharts(chartSolicitacion.length, chartSolicitacion);
      }

    }

    /*
    *
    * Establece los objetos a dibujar para un grupo de acabados.
    *
    * @param acabados --> acabados a dibujar en la grafica
    * @param solicitacion --> solicitacion a la cual se agregara los objetos a dibujar.
    *
    * */
    function setChartByPerfilResult(perfiles, solicitacion) {
      for(var i = 0; i < perfiles.length; i++)
      {
        var itemPerfil = perfiles[i],
          key = itemPerfil.perfil.nombre,
          dataArray = itemPerfil.resultado,
          chartData = chartService.getDataNVD3(dataArray, key, true, false);

        itemPerfil.resultadoXY = chartService.convertToXY(itemPerfil.resultado);
        solicitacion.chart.data.push(chartData);
      }
    }

    /*
    *
    * Establece el objeto a dibujar del resultado solicitacion consultada.
    *
    * @param solicitacionResult --> objeto respuesta de la solicitacion consultada.
    * @param solicitacion --> solicitacion consultada, en la que se agregan los objetos a dibujar.
    *
    * */
    function setChartBySolicitacionResult(solicitacionResult, solicitacion) {

      var key = solicitacion.descripcion,
          dataArray = solicitacionResult.resultado,
          chartData = chartService.getDataNVD3(dataArray, key, true, true);

      solicitacion.chart.data.push(chartData);

    }


    /*
    *
    * Establece los colores sobre los objetos a dibujar en la grafica.
    *
    * @param solicitacion --> solicitacion a la que se establecen los colores de los objetos dibujados en la grafica.
    *
    * */
    function setColorDataObjects(solicitacion) {
      var COLORS = [
          "#ED3214","#019BE7","#902C28","#40AF91","#FB8EE9","#638F6D","#A69EEA","#1F3533","#4F5ED8","#AB5B2C","#844BDA","#4230BF",
        "#2F9607","#C2A2B3","#FF25AF","#87B647","#D2E990","#EF14A9","#F00116","#A9BA4F","#2EBA98","#33BED3","#933AAA",
        "#FA0FD5","#30659F","#4F05CF","#66BD1B","#96DE65","#63CB60","#2D0418","#EA2F2A","#B51C7B","#9AAABC","#62BB36",
        "#C6111C","#C540AE","#8C05C8","#429F9B","#7EC7F1","#7C71BA","#F1AFE6","#2CB023","#657F88","#CD22F0","#49321B",
        "#E4EEB4","#D18BBF","#077544","#1AA354","#68E03B","#65C000","#2A7433","#77125C","#5C06C9","#E7E286","#816220",
        "#01A61B","#0DBAE1","#E25A57","#1BC974","#B98FB7","#F20862","#694DF6","#50923E","#723A7C","#B21B83","#4EACB3",
        "#A92B12","#2D577B","#ACF0EC","#AECFE3","#923666","#6B9439","#302863","#068F02","#D391B4","#4971DA","#9D7F23",
        "#886EFE","#520FE8","#1B2A50","#C5F9D4","#D5A853","#5DCFA3","#278C1D","#32FC06","#4B602A","#326417","#3A6535",
        "#F76239","#4DBB9E","#4BEC74","#827440","#F5979C","#AB1BBE","#0DB544","#C96D36","#013F10","#77F541","#0BCEF1",
        "#4E1D52","#4C178F","#D4A18B","#2ADD94","#A4014E","#AA5EBE","#FEE409","#4E11D5","#71DFC4","#7E3159","#0BEB9F",
        "#EB8C6F","#2898E6","#C80D9B","#E00F48","#BA0E05","#EE6ACB","#F7FCFB","#64F727","#5A4B45","#268530","#BB2B70",
        "#5D6DB0","#911A90","#044853","#F04701","#A42B8B","#B0CC09","#8D316D","#F1718D","#CFDC19","#7D66CC","#E8BBC6",
        "#9C87C5","#38B36B","#0099FB","#DFA91B","#E58AEF","#3F2F2B","#BAAEB5","#A7898F","#70877D","#CF50DD","#F6CA1F",
        "#1EE77C","#2823D2","#32CD93","#E10550","#63B1AD","#1D51E8","#E2D0BD","#172C45","#C9E66E","#0FA5AF","#162EB0",
        "#06393E","#66E81B","#0FEB94","#AC9ADB","#C8B87C","#264A0B","#606C11","#12C57C","#54CA36","#5147FE","#9A0532",
        "#14F4A8","#5CD594","#7AE373","#1B5FD0","#52568E","#D9D7C5","#FD35FD","#4313D4","#5A6240","#4BE65F","#6AA8EB",
        "#151D51","#4079D1","#E18A23","#A95D61","#5A9AC1","#42A0A8","#394A03","#498DC5","#9CCB8A","#B597D1","#84AAFE",
        "#269C2C","#7D7223","#AC9B30","#A629F4","#58FDB2","#EB3632","#BDB5D1","#0DCACB","#227360","#04316A","#1D0D06",

      ],
        chartData = solicitacion.chart.data;

      //recorre todos los objetos a dibujar en la grafica
      for(var i = 0; i < chartData.length; i++)
      {
        /*
        * @prop isSolicitacion --> hay objetos que se dibujan perteneciendo al resultado de la solicitacion o al resultado contenido en un perfil.
        * */

        //los tipos isSolicitacion varian los colores
        if(!chartData[i].isSolicitacion)
        {
          chartData[i].color = COLORS[i];
        }else {
          chartData[i].color = '#31C0BE'; //el resto son color fijo
        }
      }

    }

    function generarExcel() {

      var blob = new Blob([document.getElementById('exportable').innerHTML], {
        type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
      });
      saveAs(blob, "Reporte_propiedades.xls");

    }

  }

})(window.angular);
