/**
 * Created by stackpointer on 21/06/17.
 */
(function (angular) {
  'use strict';

  angular
    .module('app.propiedades')
    .directive('pOrientacion', [
      pOrientacion
    ]);

  function pOrientacion() {
    return {
      restrict: 'E',
      scope: {
        tipoPerfil: '=',
        orientacion: '=',
        propiedades: '='
      },
      controller: ['$scope', pOrientacionController],
      templateUrl: 'app/main/propiedades/directives/orientations/p-orientacion.html'
    };

    function pOrientacionController($scope) {

      $scope.getOrientacionTemplate = getOrientacionTemplate;

      function getOrientacionTemplate() {
        var urlTemplate = 'app/main/propiedades/directives/orientations/templates/profile.' +
          $scope.tipoPerfil + '.'+ $scope.orientacion + '.html';
        return urlTemplate;
      }
    }
  }

})(window.angular);
