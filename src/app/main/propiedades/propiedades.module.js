/**
 * Created by stackpointer on 25/04/17.
 */
/**
 * Created by aesleider on 30/03/2017.
 */

(function (angular)
{
  'use strict';

  angular
    .module('app.propiedades', ['toggle-switch', 'a8m.filter-watcher'])
    .config(config);

  /** @ngInject */
  function config($stateProvider)
  {

    $stateProvider.state('app.propiedades', {
      url    : '/propiedades',
      views  : {
        'content@app': {
          templateUrl: 'app/main/propiedades/propiedades.html',
          controller : 'PropiedadesController as vm'
        },
        'quickationstoolbar@app': {
          templateUrl: 'app/main/propiedades/quickationstoolbar/quickationstoolbar.html',
          controller : 'PropiedadesQuickationstoolbarController as vm'
        },
        'design@app.propiedades':{
          templateUrl: 'app/main/propiedades/tabDesign/tabDesign.html',
          controller : 'TabPropDesignController as vm'
        },
        'results@app.propiedades':{
          templateUrl: 'app/main/propiedades/tabResults/tabResults.html',
          controller : 'TabPropResultsController as vm'
        }
      }
    });

  }

})(window.angular);
