/**
 * Created by stackpointer on 25/04/17.
 */
/**
 * Created by stackpointer on 5/04/17.
 */
(function (angular) {
  'use strict';
  angular
    .module('app.propiedades')
    .controller('TabPropResultsController',[
      'unidadesService',
      'perfileria',
      'solicitacionesService',
      'checkList',
      'configSolicitaciones',
      'propiedades',
      'printableChart',
      'chartService',
      '$uibModal',
      TabPropResultsController
    ]);

  function TabPropResultsController(unidadesService, perfileria, solicitacionesService, checkList, configSolicitaciones, propiedades, printableChart, chartService, $uibModal) {
    var vm = this;

    //injects
    vm.perfileria = perfileria;
    vm.solicitaciones = solicitacionesService;
    vm.propiedades = propiedades;
    vm.configSolicitaciones = configSolicitaciones;
    vm.unidades = unidadesService;
    vm.printableChart = printableChart;

    //data
    vm.ngClassOrientationCarousel = { active: true };
    vm.acabadosCollapsed = [];
    vm.dataPropiedades = [];
    vm.isCollapsed = {};

    // functions
    vm.isCollapsedGroup = isCollapsedGroup;
    vm.isCheckAllByAcabado = isCheckAllByAcabado;
    vm.isCheckallByGrupo = isCheckallByGrupo;
    vm.atLeasOne = checkList.atLeasOne;
    vm.exists = checkList.exists;
    vm.content = checkList.content;
    vm.toggleSelectPerfil = toggleSelectPerfil;
    vm.propiedadGeometricasChanged = propiedadGeometricasChanged;
    vm.nextCarousel = nextCarousel;
    vm.prevCarousel = prevCarousel;
    vm.getClassesOrientationCarousel = getClassesOrientationCarousel;
    vm.reqPropiedadesEfectivas = reqPropiedadesEfectivas;
    vm.calcular = calcular;
    vm.onReadyChart = onReadyChart;


    //////////////////////////////////////////////////////////////////////

    function onReadyChart(scope)
    {
      chartService.scope = scope;
    }

    function isCollapsedGroup(key)
    {

      vm.isCollapsed[key] = ! vm.isCollapsed[key];

      Object.keys(vm.isCollapsed).forEach(function (t) {
        if(t !== key){
          vm.isCollapsed[t] = true;
        }
      });
    }

    function isCheckAllByAcabado(ev, perfiles)
    {
      if (ev.target.checked === true) {
        checkList.selectPerfiles(vm.perfileria.selectedPerfiles, perfiles);
      } else {
        checkList.deselectPerfiles(vm.perfileria.selectedPerfiles, perfiles, vm.perfileria.restartPerfilDefault);
      }
    }

    function isCheckallByGrupo(ev, grupo)
    {
      if (ev.target.checked === true) {
        checkList.selectPerfiles(vm.perfileria.selectedPerfiles, grupo);
      } else {
        checkList.deselectPerfiles(vm.perfileria.selectedPerfiles, grupo, vm.perfileria.restartPerfilDefault);
      }
    }

    function toggleSelectPerfil(perfil, event)
    {
      var index = vm.perfileria.selectedPerfiles.indexOf(perfil);

      if(event.target.checked === true && ! index > -1) //index > -1 means exist
      {
        vm.perfileria.selectedPerfiles.push(perfil);
        vm.perfileria.setPerfilDefault(perfil);
      }
      else if (event.target.checked === false && index > -1 )
      {
        //Si el es el ultimo de la lista a eliminar se selecciona el anterior.
        if(index === vm.perfileria.selectedPerfiles.length - 1){
          var perfilBefore = vm.perfileria.selectedPerfiles[index - 1];

          vm.perfileria.setPerfilDefault(perfilBefore);
        }
        vm.perfileria.selectedPerfiles.splice(vm.perfileria.selectedPerfiles.indexOf(perfil), 1);
      }

      if (vm.perfileria.selectedPerfiles.length == 0){
        vm.perfileria.restartPerfilDefault();
      }
    }

    function propiedadGeometricasChanged()
    {
      getPropiedadesGeometricas();
    }

    function nextCarousel(index)
    {
      /**/
      vm.perfileria.orientaciones.selected = index + 1;
      getPropiedadesGeometricas();

    }

    function prevCarousel(index)
    {

      /**/
      vm.perfileria.orientaciones.selected = index + 1;
      getPropiedadesGeometricas();

    }

    function getPropiedadesGeometricas()
    {
      var tipoPerfil = vm.perfileria.perfilCurrentType,
        idPerfil = vm.perfileria.selectedPerfil.perfilId,
        orientacion = vm.perfileria.orientaciones.selected,
        idUnidad = vm.unidades.unitSelected.codigo;

      return vm.perfileria.getPropiedades(tipoPerfil, idPerfil, orientacion, idUnidad);
    }

    function getClassesOrientationCarousel(active)
    {
      console.log(active);
      return {
        active:active === 0
      }
    }

    function calcular()
    {
      var perfiles = perfileria.selectedPerfiles,
        unidadId = vm.unidades.unitSelected.codigo,
        solicitacion= solicitacionesService.selected,
        orientacion = vm.perfileria.orientaciones.selected;

      return propiedades.calcular(perfiles, unidadId, solicitacion, orientacion)
        .then(function (solicitacion) {
          solicitacion.chart.scope.api.refresh();
          setTimeout(function () {
            vm.printableChart.setSVG();
          },  3000);
        });
    }

    function openPropiedadesEfectivas()
    {

      var parentElem = angular.element(document.body);
      return $uibModal.open({
        animation: true,
        templateUrl: 'app/main/propiedades/modals/propiedadesEfectivas/propiedadesEfectivas.html',
        controller: 'PropiedadesEfectivasModalInstanceController',
        controllerAs: 'vm',
        size: 'sm',
        appendTo: parentElem,
        resolve: {
          propiedadesEfectivas: vm.propiedadesEfectivas
        }
      });

    }

    function reqPropiedadesEfectivas()
    {
      var tipoPerfil = vm.perfileria.selectedPerfil,
        orientacion = vm.perfileria.orientaciones.selected;

      vm.perfileria.getPropiedadesEfectivas(tipoPerfil, orientacion)
        .then(function (propiedades) {
          vm.propiedadesEfectivas = propiedades;

          return openPropiedadesEfectivas()
        })
    }
  }

})(window.angular);
