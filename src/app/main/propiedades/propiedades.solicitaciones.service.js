/**
 * Created by stackpointer on 8/05/17.
 */

(function (angular) {
  'use strict';
  angular
    .module('arquimetclient')
    .factory('solicitacionesService', [
      'api',
      'configSolicitaciones',
      'chartService',
      solicitacionesService
    ]);

  function solicitacionesService(api, configSolicitaciones, chartService) {
    var vm = this;

    //data
    vm.list = {};
    vm.selected = {};
    vm.selected.configuration = {};
    vm.selected.configuration.components = [];
    vm.selected.chart = {};
    vm.selected.chart.options = chartService.getOptionsNVD3("", "");
    vm.selected.chart.data = [];

    //functions
    vm.reqSolicitaciones = reqSolicitaciones;
    vm.setConfiguration = setConfiguration;

    return vm;

    ///////////////////////////////////////////////////////////////////

    function reqSolicitaciones(tipoPerfilId)
    {
      var resPromise = api.propiedades.solicitaciones.get(tipoPerfilId);

      return new Promise(function (resolve, reject) {

        resPromise.then(function (response) {

          vm.list = setConfiguration(response.data);

          vm.selected = vm.list[0];

          resolve(vm);

        }).catch(function (err) {
          reject(err);
        })

      });
    }

    function setConfiguration(solicitaciones) {
      var array = [];

      for(var i = 0; i < solicitaciones.length; i++)
      {
        var solicitacion = solicitaciones[i],
          configuration = configSolicitaciones.getConfiguracion(solicitacion.codigo),
          chartData = {};

        solicitacion.configuration = {};
        solicitacion.configuration.components = configuration.components;
        solicitacion.chart = {};
        solicitacion.chart.options = chartService.getOptionsNVD3(configuration.xAxisLabel, configuration.yAxisLabel);
        solicitacion.chart.options.chart.xAxis.axisLabel = configuration.xAxisLabel;
        solicitacion.chart.options.chart.yAxis1.axisLabel = configuration.yAxisLabel;
        solicitacion.chart.printableOptions = angular.copy(solicitacion.chart.options);
        solicitacion.chart.printableOptions.chart.width = 1024;
        solicitacion.chart.data = [];
        solicitacion.chart.scope = chartService.scope;

        array.push(solicitaciones[i]);
      }

      return array;
    }

  }
})(window.angular);
