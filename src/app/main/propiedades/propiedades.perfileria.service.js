/**
 * Created by stackpointer on 26/04/17.
 */
(function (angular) {
  'use strict';

  angular
    .module('app.propiedades')
    .factory('perfileria',
      [
        'api' ,
        'unidadesService',
        perfileriaService
      ]);

  function perfileriaService(api, unidadesService) {
    var vm = {};

    //data
    vm.acabados = [];
    vm.selectedPerfiles = [];
    vm.selectedPerfil = {};
    vm.propertiesSelectedPerfil = {};
    vm.propertiesPerfilSelected = [];

    vm.orientaciones = {};
    vm.orientaciones.list = [];
    vm.orientaciones.selected = {};


    vm.perfilCurrentType = 1;

    //functions
    vm.getAcabados = getAcabados;
    vm.getOrientaciones = getOrientaciones;
    vm.getPropiedades = getPropiedades;
    vm.getPropiedadesEfectivas = getPropiedadesEfectivas;
    vm.setPerfilDefault = setPerfilDefault;
    vm.restartPerfilDefault = restartPerfilDefault;

    return vm;

    ///////////////////////////////////////////////////////////////////////////////
    function getAcabados(idTipoPerfil)
    {
      return  api.propiedades.acabados.get(idTipoPerfil)
        .then(getAcabadosComplete)
        .catch(getAcabadosFailed);

      function getAcabadosComplete(response) {
        vm.acabados = response.data;
        vm.selectedPerfiles = [];
        vm.selectedPerfiles.push(vm.acabados[0].perfiles[0]);
        vm.selectedPerfil = vm.selectedPerfiles[0];
        vm.perfilCurrentType = idTipoPerfil;
        getOrientaciones(vm.perfilCurrentType, unidadesService.unitSelected.codigo);
        return vm;
      }

      function getAcabadosFailed(err) {
        return console.log(err)
      }

    }

    function getOrientaciones(idTipoPerfil, idUnidad) {
      return api.propiedades.orientaciones.get(idTipoPerfil)
        .then(getOrientacionesComplete)
        .catch(getOrientacionesFailed);

      function getOrientacionesComplete(response) {
        vm.orientaciones.list  = response.data.orientaciones;
        vm.orientaciones.selected = response.data.orientaciones[0];
        getPropiedades(vm.perfilCurrentType, vm.selectedPerfil.perfilId, vm.orientaciones.selected, idUnidad);
        return vm.orientaciones;
      }

      function getOrientacionesFailed(err) {
        return err
      }
    }

    function getPropiedades(idTipoPerfil, idPerfil, orientacion, idUnidad)
    {
      return api.propiedades.acabados.getPropiedadesGeometricas(idTipoPerfil, idPerfil, orientacion, idUnidad)
        .then(function (response) {
          api.propiedades.acabados.getPropiedadesPerfilSelected(idTipoPerfil, idPerfil, orientacion)
            .then(function (resp) {
              vm.propertiesSelectedPerfil = resp.data;
            });
          vm.propertiesPerfilSelected = response.data;
          return vm.propertiesPerfilSelected;
        })
    }

    function getPropiedadesEfectivas(perfil, orientacion) {
      var json = {};
      json.perfil = perfil;
      json.orientacion = orientacion;

      return api.propiedades.acabados.getPropiedadesEfectivas(json)
        .then(getPropiedadesEfectivasCompleted)
        .catch(getPropiedadesEfectivasFailed);

      function getPropiedadesEfectivasCompleted(response)
      {
         return response.data;
      }

      function getPropiedadesEfectivasFailed(err)
      {
        return err;
      }
    }

    function setPerfilDefault(item)
    {
      vm.selectedPerfil = item;
      getPropiedades(vm.perfilCurrentType, item.perfilId, vm.orientaciones.selected, unidadesService.unitSelected.codigo);
    }

    function restartPerfilDefault()
    {
      if(vm.selectedPerfiles.length == 0){
        vm.selectedPerfiles.push(vm.acabados[0].perfiles[0]);
        vm.selectedPerfil = vm.selectedPerfiles[0];
      }else {
        vm.selectedPerfil = vm.selectedPerfiles[0];
      }
    }

  }
})(window.angular);
