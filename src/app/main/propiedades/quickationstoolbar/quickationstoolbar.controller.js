/**
 * Created by stackpointer on 16/05/17.
 */

(function() {
  'use strict';

  angular
    .module('app.propiedades')
    .controller('PropiedadesQuickationstoolbarController', [
      'unidadesService',
      'perfileria',
      'solicitacionesService',
      'propiedades',
      'reporteService',
      'printableChart',
      '$uibModal',
      PropiedadesQuickationstoolbarController
    ]);

  /** @ngInject */
  function PropiedadesQuickationstoolbarController(unidadesService, perfileria, solicitacionesService, propiedades, reporteService, printableChart, $uibModal) {
    var vm = this;

    //injects
    vm.unidades = unidadesService;
    vm.printableChart = printableChart;

    //data

    vm.unidades.reqUnidades();

    //functions
    vm.unitsChange = unitsChange;
    vm.calcular = calcular;
    vm.exportData = propiedades.exportData;
    vm.openConfigReporte = openConfigReporte;

    ////////////////////////////////////////////////////////////////////////
    function calcular()
    {
      var perfiles = perfileria.selectedPerfiles,
        unidadId = vm.unidades.unitSelected.codigo,
        solicitacion= solicitacionesService.selected,
        orientacion = perfileria.orientaciones.selected;

      return propiedades.calcular(perfiles, unidadId, solicitacion, orientacion)
        .then(function (solicitacion) {
          solicitacion.chart.scope.api.refresh();
          setTimeout(function () {
            vm.printableChart.setSVG();
          },  3000);
        });
    }

    function unitsChange()
    {
      getPropiedadesGeometricas();
      calcular();
    }

    function getPropiedadesGeometricas()
    {
      var tipoPerfil = perfileria.perfilCurrentType,
        idPerfil = perfileria.selectedPerfil.perfilId,
        orientacion = perfileria.orientaciones.selected,
        idUnidad = vm.unidades.unitSelected.codigo;

      return perfileria.getPropiedades(tipoPerfil, idPerfil, orientacion, idUnidad);
    }

    function openConfigReporte()
    {
      var parentElem = angular.element(document.body);
      var modalInstance = $uibModal.open({
        animation: true,
        templateUrl: 'app/main/propiedades/modals/configReporte/configReporte.html',
        controller: 'configReporteInstanceController',
        controllerAs: 'vm',
        size: 'md',
        appendTo: parentElem,
        resolve: {
          solicitaciones: solicitacionesService
        }
      });

      modalInstance.result.then(function (configReport) {
        //luego de obtener configuraciones para reporte.
        var perfiles = perfileria.selectedPerfiles,
          unidadId = vm.unidades.unitSelected.codigo,
          solicitacion= solicitacionesService.selected,
          orientacion = perfileria.orientaciones.selected;

        reporteService.getReporte(configReport.isTecnico, perfiles, unidadId, solicitacion, orientacion);

      }, function () {
        //luego de cancelar generar reporte.
      });

    }

  }
})();

