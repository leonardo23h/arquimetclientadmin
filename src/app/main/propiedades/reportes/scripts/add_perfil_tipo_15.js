function add_perfil_tipo_15(report, dd)
{
	/****************************************/
	/*               Titulo                 */
	/****************************************/
	dd.new_page();
	dd.indent = 0;
	dd.titulo_principal("REPORTE ACADÉMICO PARA {0} \nCOMPROBACIÓN DE PERFIL {1}\nACABADO {2}, Fy = {3} MPa \nREGLAMENTO NSR-10 / AISI S100-12", report['perfil']['tipoPerfil']['descripcion'], report['perfil']['nombre'], report['perfil']['acabadoPerfil']['descripcion'], report['perfil']['material']['fy']);

	/****************************************/
	/*         Tabla de propiedades         */
	/****************************************/
	// insertar titulo tabla propiedades
	dd.titulo_tabla("TABLA DE PROPIEDADES GEOMÉTRICAS");
	var contenido_tabla_propiedades =
			{
				table: {
					// headers are automatically repeated if the table spans over multiple pages
					// you can declare how many rows should be treated as headers
					headerRows: 0,
					widths: ['28%', '18%', '18%', '18%', '18%'],
					body: []
				},
				layout: dd.mergeAll(dd.layout_lineas_grises, dd.layout_no_padding, dd.layout_lineas_delgadas)
			};

	// Ancho de columnas de las propiedades de la tabla
	var ancho = 4; // ancho tabla = 5, menos la columna de imagen es 4

	// Titulo del cuadro
	var titulo_encabezado_tabla = dd.get_array(ancho + 1);
	titulo_encabezado_tabla[1] = {text: report['perfil']['nombre'], style: 'tableHeader', colSpan: ancho};
	contenido_tabla_propiedades['table']['body'][contenido_tabla_propiedades['table']['body'].length] = titulo_encabezado_tabla;

	var keys_propiedades_tabla = ["D", "e", "-", "-", "ro", "J", "Peso Propio", "Area", "I2", "S", "Z", "r"];
	var N = keys_propiedades_tabla.length;

	// iterar de 4 en 4 propiedades
	for (var i = 0; i < N; i += ancho)
	{
		var encabezado = dd.get_array(ancho + 1);
		var valores = dd.get_array(ancho + 1);

		// llenar los valores
		for (var j = 0; (j < ancho) && (j + i < N); j++)
		{
			var find = keys_propiedades_tabla[j + i];
			var item = {key: '?' + find + '?', magnitud: '', valor: {text:'*???*', style:'no_encontrado'}};

			if(find == '-')
			{
				item = {key: '-', magnitud: '', valor: '-'};
			}
			else
			{
				var found = dd.buscar_key(report['propiedadesGeometricas'], 'key', find);
				if (found['item'] != null)
				{
					item = found['item'];
				}
			}
			var formato_encabezado = (item['magnitud'] != '') ? "{0} ({1})" : "{0}";
			encabezado[j + 1] = {text: dd.format(formato_encabezado, item['key'], item['magnitud']), style: 'tableHeader'};
			valores[j + 1] = {text: item['valor'], style: 'tableCell'};
		}
		contenido_tabla_propiedades['table']['body'][contenido_tabla_propiedades['table']['body'].length] = encabezado;
		contenido_tabla_propiedades['table']['body'][contenido_tabla_propiedades['table']['body'].length] = valores;
	}

	// inserto imagen con RowSpan dinamico
	var imagen_tabla = dd.getImage(report['imagenOrientacion']);
	contenido_tabla_propiedades['table']['body'][0][0] = dd.mergeAll(imagen_tabla, {alignment: 'center', rowSpan: contenido_tabla_propiedades['table']['body'].length});
	// inserto el contenido de la tabla
	dd.add_content(contenido_tabla_propiedades);

	dd.titulo_h2('ESPESOR DE DISEÑO DE PARED DE PERFILES TUBULARES');
	dd.add_content_margin('De acuerdo con la sección F.2.2.4.2 de la NSR-10 para los cálculos que involucren el espesor de pared de un PTE, debe usarse un espesor de diseño, t, igual a 0.93 el espesor nominal si el PTE fue fabricado con soldadura por resistencia eléctrica (ERW).');
  dd.add_imagen_base64("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQ8AAAAwCAYAAAAPdP/yAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAbgSURBVHhe7Z0BcusqDEW7riwo68lqspksJt+SwJaEhDFJfp/Te2aYqW0QIKQL7ozbnycAAEwA8QAATAHxAABMAfEAAEwB8QAATAHxAABMAfEAb+F+/Xlebo9yBc7OyHpCPByP2+X58/NTyvV5L/db7s/rWo/K5el9bW3Fdc6P+OGaOwqcFI7fy+2ZhWwoHtzoL0bD/WqclTvv8bxdXMK4tlEdUvPG/qn9HPjhcXteiljqnYvnTvfL/NfrtRRhVe1NCdfhRWpfke1mHL2NpJCNPW0r/rN1sw3mSN2KCHt+gti3STGatQ/Fgxf2D4pHc1QjQYgWiIPE3Y/uOVgsVCCd3s805yjxFr9dLnauy80lkMs1+8om1P2qfNcI8btPNyVplj7uyQZhY0Hq50moaMZOt3rioZ/1+jlSVyCfXrp1RmyqdXM04tHuCNnEv41ApctO0gRtKCp7AW7tf4Of05MTicftzoG4PVdBmPm18nHx2IhPl21/cb2AQDzGkeQdm+dOXV6DB8dZLh6eyGbezxedPGSSNiF96Z0MogDNglbuDwnNOq5WHM7p5400MEvgik+qz8lnmw+qeGbtTQLS9YfENRQFM25hTjxo7Q+M28+7R7fu4uvy7JB4JDYzG3htWTkiHgsc0JIAPXF63K58j4NvqaMX4dx+znck8k2dJ8+RAzJIpCK4jd+8b7s+kjUy9V3pJc9HxMP0PyoeVlz79OvqV8Bx8chtvlU8JBFGJ3oWJAiNk9LTRAAHTd8n3m/nFo88qMgX2/3qV3qNif3TxBP5sibqkTWYIBYFGbPu85B4rPVGTx7UX7vxxOzUbcSrlp79vs23iod1CnU84qAWCZoyuZGF6SI7oXWYLz0HSvtWPMbmNiQEvLDbGP6GeCyUeV/SRHLJahJQ+nk9PmJGxWN4rdzY96G468WlZr/u4+Fmkq3Ryp5NyYto6rl4dB1ABucEQ+Mn+tv4QOLr6rXeDhgGTKvm3v4nk+J/IUuU5b4NWAnA9XRBvjSOdL5q7Eoy95Ngjlg8aAhaLPIEauj4JNq8qH8zL+0b1+ZI3UojHkdsMm0cV0LxYAO8U1OpDWsA1CKBwM7Xu3PT1rarA63trrRIyl7Tz9CKvQ8Omtq3DoIyLz2ctW44Rgn41RYXJ7ihn88ErZUbt5qTcQvfV+KxzlvKGsD6mTIg8WLvvQQn0dZ/a9uun0mwDD12LyBRcgd+MH3pNkfqFlafLWWd2hGbBNVPfB6LR4QxQkGjk10lhe9MX9NgV6eKSMgjFYRNP87h4N/CrCn4LkhA3YanGBYPe7zpiAdBAkAqtogAt1sEgK8pyJaTyCYe2oaIhO2HTA0eF8HvUXYws2OBUyOn6lw4iPefPFaKINzcSWQlFg+cPAA4B+PiwYm8vRfpk0X9mWup9yx573PtyhHXtKsnlaA+djMA/k0OiAcAAGxAPAAAU0A8AABTQDwAAFNAPAAAU0A8AABTQDwAAFNAPAAAU0A8AABTQDzAW2g+/QanZ29NIR7gReTT9fjzpeSDOf0peGm4/nmDtehvmoJPI/zn7cxYva0v90yPi0v/w7BDNLZr2ft2a3TuhdpP86XzQTsF/oyksSVAPMALSECGwrFAf0vzupQw+Oh7pnqfA94mqv47nBzArhPzrxpWaDzajoxvE6+98bZ133qa0nNmpA8/N8venCrF1mI//ncSo3ZayP9RPYgHmIeSPhIGZjmR0DMWhiDRG/HIkrpzstnFtu3tolE//foTJOJxTKD2/TE2bul7zK/UZ3sKg3iAaaITwcqSKJIUSZC6RKqvEk0iZeKzi08yNQ7q2/cV9PNp8Thuf184iCG7jZD1iNcQ4gGm6f1Cbe+1IwzecgIx7+IT4mF+f7L2K4m33ZOEWJ8fEg9lKymhX4pomeL9khDPKWZfPOKTRI9orSEeYJLOsXcVAV1csHZ2Pg7+Wr/YGswxhyQ5B30kQpzMdVztrj60gx+hmbP4MBPgGDWnhP64qf0xMSYgHuCtZCcPCl57v03MnnjY+vvJ0oN3bDYUJA0LSi4eW9s3Ecx5po+9Nrl4kFgdFw4iWmuIB5gmCqg4QN0rAqETiZLYJINN9DYZsiSgdvqEo3d2/bPg7dqklPoH87rP3smDnjengt6cFoI2mXg0oq79HvZdiX0B8QDzNMkgu7d/TeFgrvcpAsurSHhdignyBU5sUycTD1fPRLx/rpOSsM/9GF4imCMXPb5UPAbb8M9J3aT/vnBVaAztM4gHeAHakbKAA18DCYsRYQHiAV6DdrTgiAy+BTp1+BOaAPEAr1OOxG895oNfR14VY+EgIB4AgCkgHgCAKSAeAIApIB4AgCkgHgCACZ7P/wBWzq1iRGy37gAAAABJRU5ErkJggg==");



  /****************************************/
	/*          MIEMBROS A TENSION          */
	/****************************************/
	var solicitacion = dd.buscar_key(report['solicitaciones'], 'idSolicitacion', 4);
	if(solicitacion['item'] != null)
	{
		// pagina 1
		var variables = solicitacion['item']['resultado']['listaVariables'];

		dd.indent = 0;
		dd.titulo_h1("DISEÑO DE MIEMBROS A TENSIÓN");

		dd.add_content_margin("El diseño debe cumplir:");
		dd.add_cumplimiento(solicitacion['item'], '≤1');
		dd.add_content_margin("donde:");
		dd.add_variable(dd.buscar_key(variables, 'key', 'Tu'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'ɸt'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Tn'));
		dd.indent = 1;
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_Pn'));
		dd.add_content_margin("donde:");
		dd.add_variable(dd.buscar_key(variables, 'key', 'As'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Fy'));

		dd.new_line();
		dd.indent = 0;
		dd.titulo_h2("DISEÑO PARA LOS MECANISMOS DE TRANSFERENCIA DE FUERZAS");
		dd.add_content_margin('El diseño debe cumplir');
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_MecanismoTransferenciaFuerzas'), '≤1');
		dd.add_content_margin("donde:");
		dd.add_variables(dd.buscar_keys(variables, 'key', ['Vr']));
		dd.indent = 1;
		dd.add_content_margin('Fuerza externa aplicada a la sección acero');
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_Vr'));
		dd.add_content_margin("donde:");
		dd.add_variables(dd.buscar_keys(variables, 'key', ['Pr','As','Fy','Pno']));
		dd.indent = 2;
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_Pno'));
		dd.add_content_margin("donde:");
		dd.add_variables(dd.buscar_keys(variables, 'key', ['Pp']));
		dd.indent = 3;
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_Pp'));
		dd.add_content_margin("donde:");
		dd.add_variables(dd.buscar_keys(variables, 'key', ['As','Fy','C2','fc','Ac']));

		dd.new_line();
		dd.indent = 0;
		dd.add_content_margin('Resistencia suministrada por adherencia entre el acero y el concreto');
		dd.add_variables(dd.buscar_keys(variables, 'key', ['O','Rn']));
		dd.indent = 1;
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_Rn'));
		dd.add_content_margin("donde:");
		dd.add_variables(dd.buscar_keys(variables, 'key', ['D','Cin','Fin']));



	}

	/****************************************/
	/*         MIEMBROS A COMPRESION        */
	/****************************************/
	var solicitacion = dd.buscar_key(report['solicitaciones'], 'idSolicitacion', 1);
	if(solicitacion['item'] != null)
	{
		var variables = solicitacion['item']['resultado']['listaVariables'];

		// pagina 2
		dd.indent = 0;
		dd.titulo_h1("DISEÑO DE MIEMBROS A COMPRESIÓN");

		dd.add_content_margin("Verificación de las limitantes del NSR-10 F.2.9.2.2.1 y AISC 360-10 I2-2a.");
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_Verificacion_1'),'≥1');
		dd.add_content_margin("donde:");
		dd.add_variables(dd.buscar_keys(variables, 'key', ['Ag','As']));
		dd.add_content_margin('La sección clasifica como compacta según la tabla F.2.9.1–1a del NSR-10 o la tabla I1.1A del AISC 360-10, debido a que:');
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_D_T_1'),'≤1');
		dd.add_content_margin("donde:");
		dd.add_variables(dd.buscar_keys(variables, 'key', ['D','t','E','Fy']));


		dd.add_content_margin("El diseño debe cumplir:");
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_CumplimientoAdicional'),'≤1');
		//dd.add_cumplimiento(solicitacion['item'], '≤1');
		dd.add_content_margin("donde:");
		dd.add_variables(dd.buscar_keys(variables, 'key', ['Pu', 'OcPnsc', 'Oc', 'Pn']));
		dd.indent = 1;
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_Para_0_44_Pno'), '≤1');
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_Pn'));
		dd.add_content_margin("donde:");
		dd.add_variables(dd.buscar_keys(variables, 'key', ['Pno']));
		dd.indent = 2;
		dd.add_content_margin('Para secciones compactas');
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_Pno'));
		dd.add_content_margin("donde:");
		dd.add_variables(dd.buscar_keys(variables, 'key', ['Pp']));
		dd.indent = 3;
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_Pp'));
		dd.add_content_margin("donde:");
		dd.add_variables(dd.buscar_keys(variables, 'key', ['As','Fy','C2','fc','Ac']));
		dd.new_line();
		dd.indent = 1;
		dd.add_variable(dd.buscar_key(variables, 'key', 'Pe'));
		dd.indent = 2;
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_Pe'));
		dd.add_content_margin("donde:");
		dd.add_variables(dd.buscar_keys(variables, 'key', ['KL','EIeff']));
		dd.indent = 3;
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_El'));
		dd.add_content_margin("donde:");
		dd.add_variables(dd.buscar_keys(variables, 'key', ['ES','Is','C3']));
		dd.indent = 4;
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_C3'));
		dd.add_content_margin("donde:");
		dd.add_variables(dd.buscar_keys(variables, 'key', ['As','Ac']));
		dd.indent = 3;
		dd.add_variables(dd.buscar_keys(variables, 'key', ['Ec','Ic']));


		dd.indent = 0;
		dd.titulo_h2("DISEÑO PARA LOS MECANISMOS DE TRANSFERENCIA DE FUERZAS");
		dd.add_content_margin('El diseño debe cumplir');
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_MecanismoTransferenciaFuerzas'),'≤1');
		dd.add_content_margin("donde:");
		dd.add_variables(dd.buscar_keys(variables, 'key', ['Vr']));

		dd.indent = 1;
		dd.add_content_margin('Fuerza externa aplicada a la sección de acero');
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_Vr'));
		dd.add_content_margin("donde:");
		dd.add_variables(dd.buscar_keys(variables, 'key', ['Pr','As','Fy','Pno']));

		dd.indent = 2;
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_Pno'));
		dd.add_content_margin("donde:");
		dd.add_variable(dd.buscar_key(variables, 'key', 'Pp'));
		dd.indent = 3;
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_Pp'));
		dd.add_content_margin("donde:");
		dd.add_variables(dd.buscar_keys(variables, 'key', ['As','Fy','C2','fc','Ac']));
		dd.new_line();

		dd.indent = 0;
		dd.add_content_margin("Resistencia suministrada por adherencia entre el acero y el concreto");
		dd.add_variables(dd.buscar_keys(variables, 'key', ['O', 'Rn']));

		dd.indent = 1;
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_Rn'));
		dd.add_content_margin("donde:");
		dd.add_variables(dd.buscar_keys(variables, 'key', ['D','Cin','Fin']));

	}

	/****************************************/
	/*          MIEMBROS A FLEXION          */
	/****************************************/
	var solicitacion = dd.buscar_key(report['solicitaciones'], 'idSolicitacion', 2);
	if(solicitacion['item'] != null)
	{
		// pagina 1
		var variables = solicitacion['item']['resultado']['listaVariables'];

		dd.indent = 0;
		dd.titulo_h1("DISEÑO DE MIEMBROS A FELEXIÓN");
		dd.add_content_margin('La sección clasifica como compacta según la tabla F.2.9.1–1b del NSR-10 o la tabla I1.1B del AISC 360-10, debido a que:');
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_D_T_1'),'≤1');
		dd.add_content_margin("donde:");
		dd.add_variables(dd.buscar_keys(variables, 'key', ['D','t','E','Fy']));

		dd.add_content_margin("El diseño debe cumplir:");
		dd.add_cumplimiento(solicitacion['item'], '≤1');
		dd.add_content_margin("donde:");
		dd.add_variable(dd.buscar_key(variables, 'key', 'Mu'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Ob'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Mn'));

		dd.indent = 1;
		dd.add_content_margin('Para secciones compactas');
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_Mn'));
		dd.add_content_margin("donde:");
		dd.add_variables(dd.buscar_keys(variables, 'key', ['Mp']));

	}

	/****************************************/
	/*     DISENO DE MIEMBROS A CORTANTE    */
	/****************************************/
	var solicitacion = dd.buscar_key(report['solicitaciones'], 'idSolicitacion', 3);
	if(solicitacion['item'] != null)
	{
		// pagina 1
		var variables = solicitacion['item']['resultado']['listaVariables'];

		dd.indent = 0;
		dd.titulo_h1("DISEÑO DE MIEMBROS A CORTANTE");
		dd.add_content_margin('Según F.2.9.4.1 del NSR-10 o I4.1 del AISC 360-10 la resistencia de diseño a cortante de la sección compuesta puede ser tomada como la resistencia de diseño a cortante de la sección de acero tomada aisladamente.');
		dd.add_content_margin("El diseño debe cumplir:");
		dd.add_cumplimiento(solicitacion['item'], '≤1');
		dd.add_content_margin("donde:");
		dd.add_variables(dd.buscar_keys(variables, 'key', ['Vu','Ov','Vn']));
		dd.indent = 1;
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_Vn'));
		dd.add_content_margin("donde:");
		dd.add_variables(dd.buscar_keys(variables, 'key', ['Ag','Fcr']));

		dd.indent = 2;
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_Fcr'));
		dd.add_content_margin("donde:");
		dd.add_variables(dd.buscar_keys(variables, 'key', ['E','Fy','D','t']));
	}

	/****************************************/
	/*               Finalizo               */
	/****************************************/
	dd.pushStack();
}
