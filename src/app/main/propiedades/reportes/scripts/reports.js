function reports()
{
	this.dd = new DocumentDefinition();

	this.add_json = function(jsonDecoded)
	{
		// anadir todos los acabados recibidos
		for(var i in jsonDecoded)
		{
			this.add_perfil(jsonDecoded[i]);
		}
	};

	this.add_perfil = function(perfil)
	{
		// verificar si el perfil es soportado
		var functionName = 'add_perfil_tipo_'+perfil['perfil']['tipoPerfilId'];
		if( typeof window[functionName] != 'undefined' )
		{
			window[functionName](perfil, this.dd);
		}
		else
		{

		}
	};

	this.output = function()
	{
		return this.dd.output();
	};
}

