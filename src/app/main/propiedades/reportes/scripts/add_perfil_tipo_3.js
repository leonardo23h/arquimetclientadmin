function add_perfil_tipo_3(report, dd)
{
	/****************************************/
	/*               Titulo                 */
	/****************************************/
	dd.new_page();
	dd.indent = 0;
	dd.titulo_principal("REPORTE ACADÉMICO PARA {0} \nCOMPROBACIÓN DE PERFIL {1}\nACABADO {2}, Fy = {3} MPa \nREGLAMENTO NSR-10 / AISI S100-12", report['perfil']['tipoPerfil']['descripcion'], report['perfil']['nombre'], report['perfil']['acabadoPerfil']['descripcion'], report['perfil']['material']['fy']);

	/****************************************/
	/*         Tabla de propiedades         */
	/****************************************/
	// insertar titulo tabla propiedades
	dd.titulo_tabla("TABLA DE PROPIEDADES GEOMÉTRICAS");
	var contenido_tabla_propiedades =
			{
				table: {
					// headers are automatically repeated if the table spans over multiple pages
					// you can declare how many rows should be treated as headers
					headerRows: 0,
					widths: ['28%', '18%', '18%', '18%', '18%'],
					body: []
				},
				layout: dd.mergeAll(dd.layout_lineas_grises, dd.layout_no_padding, dd.layout_lineas_delgadas)
			};

	// Ancho de columnas de las propiedades de la tabla
	var ancho = 4; // ancho tabla = 5, menos la columna de imagen es 4

	// Titulo del cuadro
	var titulo_encabezado_tabla = dd.get_array(ancho + 1);
	titulo_encabezado_tabla[1] = {text: report['perfil']['nombre'], style: 'tableHeader', colSpan: ancho};
	contenido_tabla_propiedades['table']['body'][contenido_tabla_propiedades['table']['body'].length] = titulo_encabezado_tabla;

	var keys_propiedades_tabla = ["A", "B", "C", "e", "Peso Propio", "Area", "Cw", "ro", "Ix", "Sx(t)", "Sx(b)", "rx", "Iy", "Sy(l)", "Sy(r)", "ry", "J", "j", "Xcg", "Ycg"];
	var N = keys_propiedades_tabla.length;

	// iterar de 4 en 4 propiedades
	for (var i = 0; i < N; i += ancho)
	{
		var encabezado = dd.get_array(ancho + 1);
		var valores = dd.get_array(ancho + 1);

		// llenar los valores
		for (var j = 0; (j < ancho) && (j + i < N); j++)
		{
			var find = keys_propiedades_tabla[j + i];
			var found = dd.buscar_key(report['propiedadesGeometricas'], 'key', find);
			var item = {key: '?' + find + '?', magnitud: '', valor: {text:'*???*', style:'no_encontrado'}};
			if (found['item'] != null)
			{
				item = found['item'];
			}
			var formato_encabezado = (item['magnitud'] != '') ? "{0} ({1})" : "{0}";
			encabezado[j + 1] = {text: dd.format(formato_encabezado, item['key'], item['magnitud']), style: 'tableHeader'};
			valores[j + 1] = {text: item['valor'], style: 'tableCell'};
		}
		contenido_tabla_propiedades['table']['body'][contenido_tabla_propiedades['table']['body'].length] = encabezado;
		contenido_tabla_propiedades['table']['body'][contenido_tabla_propiedades['table']['body'].length] = valores;
	}

	// inserto imagen con RowSpan dinamico
	var imagen_tabla = dd.getImage(report['imagenOrientacion']);
	contenido_tabla_propiedades['table']['body'][0][0] = dd.mergeAll(imagen_tabla, {alignment: 'center', rowSpan: contenido_tabla_propiedades['table']['body'].length});

	// inserto el contenido de la tabla
	dd.add_content(contenido_tabla_propiedades);

	/****************************************/
	/*          MIEMBROS A TENSION          */
	/****************************************/
	var solicitacion = dd.buscar_key(report['solicitaciones'], 'idSolicitacion', 4);
	if(solicitacion['item'] != null)
	{
		// pagina 1
		var variables = solicitacion['item']['resultado']['listaVariables'];

		dd.indent = 0;
		dd.titulo_h1("DISEÑO DE MIEMBROS A TENSIÓN");
		dd.add_content_margin("El diseño debe cumplir:");
		dd.add_cumplimiento(solicitacion['item'], '≤1');
		dd.add_content_margin("donde:");
		dd.add_variable(dd.buscar_key(variables, 'key', 'Tu'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Ot'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Tn'));

		dd.indent = 1;
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_Tn'));
		dd.add_content_margin("donde:");
		dd.add_variable(dd.buscar_key(variables, 'key', 'Ag'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Fy'));
	}

	/****************************************/
	/*         MIEMBROS A COMPRESION        */
	/****************************************/
	var solicitacion = dd.buscar_key(report['solicitaciones'], 'idSolicitacion', 1);
	if(solicitacion['item'] != null)
	{
		var variables = solicitacion['item']['resultado']['listaVariables'];

		// pagina 2
		dd.indent = 0;
		dd.titulo_h1("DISEÑO DE MIEMBROS A COMPRESIÓN");
		dd.add_content_margin("El diseño debe cumplir:");
		dd.add_cumplimiento(solicitacion['item'], '≤1');
		dd.add_content_margin("donde:");
		dd.add_variable(dd.buscar_key(variables, 'key', 'Pu'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Oc_Torsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Pn_Torsional'));

		dd.indent = 1;
		dd.titulo_h2("Resistencia de miembros a pandeo lateral torsional");
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_Pn_Torsional'));
		dd.add_content_margin("donde:");
		dd.add_variable(dd.buscar_key(variables, 'key', 'Ae_Torsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Fn_Torsional'));

		dd.indent = 2;
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Para_Ac_1_Torsional'), '≤1');
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_Fn_Torsional'));
		dd.add_content_margin("donde:");
		dd.add_variable(dd.buscar_key(variables, 'key', 'Fy_Torsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Ac_Torsional'));

		dd.indent = 3;
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_Ac_Torsional'));
		dd.add_content_margin("donde:");
		dd.add_variable(dd.buscar_key(variables, 'key', 'Fy_Torsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Fe_Torsional'));

		dd.indent = 4;
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_Fe_Torsional'));
		dd.add_content_margin("donde:");
		dd.add_variable(dd.buscar_key(variables, 'key', 'Cb_Torsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Sf_Torsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'E_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Ky_Torsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Ly_Torsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Iye_Torsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Kx_Torsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Lx_Torsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'rx_Torsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'ry_Torsional'));
		dd.new_line();


		dd.indent = 1;
		dd.titulo_h2("Resistencia de miembros a pandeo distorsional");
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Para_Ad_2_Distorsional') , '>1');
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_Pn_Distorsional'));
		dd.add_content_margin("donde:");
		dd.add_variable(dd.buscar_key(variables, 'key', 'Ad_Distorsional'));

		dd.indent = 2;
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_Ad_Distorsional'));
		dd.add_content_margin("donde:");
		dd.add_variable(dd.buscar_key(variables, 'key', 'Py_Distorsional'));
		dd.indent = 3;
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_Py_Distorsional'));
		dd.add_content_margin("donde:");
		dd.add_variable(dd.buscar_key(variables, 'key', 'Ag_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Fy_Distorsional'));
		dd.new_line();

		dd.indent = 2;
		dd.add_content_margin("donde:");
		dd.add_variable(dd.buscar_key(variables, 'key', 'Pcrd_Distorsional'));
		dd.indent = 3;
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_Pcrd_Distorsional'));
		dd.add_content_margin("donde:");
		dd.add_variable(dd.buscar_key(variables, 'key', 'Ag_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Fd_Distorsional'));
		dd.indent = 4;
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_Fd_Distorsional'));
		dd.add_content_margin("donde:");
		dd.add_variable(dd.buscar_key(variables, 'key', 'kOfe_Distorsional'));
		// pagina 5
		dd.indent = 5;
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_Kofe_Distorsional'));
		dd.add_content_margin("donde:");
		dd.add_variable(dd.buscar_key(variables, 'key', 'E_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'G_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Jf_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'xo_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'hx_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Ixf_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Iyf_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Ixyf_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Cwf_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'L_Distorsional'));
		dd.new_line();

		dd.indent = 4;
		dd.add_variable(dd.buscar_key(variables, 'key', 'kOwe_Distorsional'));
		dd.indent = 5;
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_Kowe_Distorsional'));
		dd.add_content_margin("donde:");
		dd.add_variable(dd.buscar_key(variables, 'key', 'E_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 't_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'u_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'ho_Distorsional'));
		dd.new_line();

		dd.indent = 4;
		dd.add_variable(dd.buscar_key(variables, 'key', 'kO_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'kOfg_Distorsional'));
		dd.indent = 5;
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_Kofg_Distorsional'));
		dd.add_content_margin("donde:");
		dd.add_variable(dd.buscar_key(variables, 'key', 'L_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Af_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'xo_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'hx_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Ixf_Distorsional'));
		// pagina 6
		dd.add_variable(dd.buscar_key(variables, 'key', 'Iyf_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Ixyf_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'yo_Distorsional'));
		dd.new_line();

		dd.indent = 4;
		dd.add_variable(dd.buscar_key(variables, 'key', 'kOwg_Distorsional'));
		dd.indent = 5;
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_Kowg_Distorsional'));
		dd.add_content_margin("donde:");
		dd.add_variable(dd.buscar_key(variables, 'key', 'L_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Lcr_Distorsional'));
		dd.indent = 6;
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_Lcr_Distorsional'));
		dd.add_content_margin("donde:");
		dd.add_variable(dd.buscar_key(variables, 'key', 'ho_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'u_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 't_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Ixf_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Iyf_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Ixyf_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'xo_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'hx_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Cwf_Distorsional'));
		dd.new_line();

		dd.indent = 5;
		dd.add_variable(dd.buscar_key(variables, 'key', 'Lm_Distorsional'));

		// pagina 7

	}

	/****************************************/
	/*          MIEMBROS A FLEXION          */
	/****************************************/
	var solicitacion = dd.buscar_key(report['solicitaciones'], 'idSolicitacion', 2);
	if(solicitacion['item'] != null)
	{
		// pagina 1
		var variables = solicitacion['item']['resultado']['listaVariables'];

		dd.indent = 0;
		dd.titulo_h1("DISEÑO DE MIEMBROS A FELEXIÓN");
		dd.add_content_margin("El diseño debe cumplir:");
		dd.add_cumplimiento(solicitacion['item'], '≤1');
		dd.add_content_margin("donde:");
		dd.add_variable(dd.buscar_key(variables, 'key', 'Mu'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Ob_Torsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Mn_Torsional'));

		dd.indent = 1;
		dd.titulo_h2("Resistencia nominal de la sección");
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_Mn_Seccion'));
		dd.add_content_margin("donde:");
		dd.add_variable(dd.buscar_key(variables, 'key', 'Se_Torsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Fy_Torsional'));

		dd.titulo_h2("Resistencia de miembros a pandeo lateral torsional");
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_Mn_Torsional'));
		dd.add_content_margin("donde:");
		dd.add_variable(dd.buscar_key(variables, 'key', 'Sc_Torsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Fc_Torsional'));
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_Para_Ad_2'), '≥1');
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_Fc_Torsional'));
		dd.add_content_margin("donde:");
		dd.add_variable(dd.buscar_key(variables, 'key', 'Fy_Torsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Fe_Torsional'));

		dd.indent = 2;
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_Fe_Torsional'));
		dd.add_content_margin("donde:");
		dd.add_variable(dd.buscar_key(variables, 'key', 'Cb_Torsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'A_Torsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Sf_Torsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'ro_Torsional'));

		dd.indent = 3;
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_Ro_Torsional'));
		dd.add_content_margin("donde:");
		dd.add_variable(dd.buscar_key(variables, 'key', 'rx_Torsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'ry_Torsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'xo_Torsional'));

		dd.new_line();
		dd.indent = 2;
		dd.add_variable(dd.buscar_key(variables, 'key', 'oey_Torsional'));
		dd.indent = 3;
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_Oey_Torsional'));
		dd.add_content_margin("donde:");
		dd.add_variable(dd.buscar_key(variables, 'key', 'E_Torsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Ky_Torsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Ly_Torsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'ry_Torsional'));

		dd.new_line();
		dd.indent = 2;
		dd.add_variable(dd.buscar_key(variables, 'key', 'ot_Torsional'));
		dd.indent = 3;
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_Ot_Torsional'));
		dd.add_content_margin("donde:");
		dd.add_variable(dd.buscar_key(variables, 'key', 'A_Torsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'ro_Torsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'G_Torsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'J_Torsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'E_Torsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Cw_Torsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Kt_Torsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Lt_Torsional'));

		dd.new_line();
		dd.indent = 1;
		dd.titulo_h2("Resistencia de miembros a pandeo distorisional");
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_Para_Ad_1'),'>1');
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_Mn_Distorsional'));
		dd.add_content_margin("donde:");
		dd.add_variable(dd.buscar_key(variables, 'key', 'My_Distorsional'));
		dd.indent = 2;
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_My_Distorsional'));
		dd.add_content_margin("donde:");
		dd.add_variable(dd.buscar_key(variables, 'key', 'Sfy_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Fy_Distorsional'));

		dd.new_line();
		dd.indent = 1;
		dd.add_variable(dd.buscar_key(variables, 'key', 'Mcrd_Distorsional'));
		dd.indent = 2;
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_Mcrd_Distorsional'));
		dd.add_content_margin("donde:");
		dd.add_variable(dd.buscar_key(variables, 'key', 'Sf_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Fd_Distorsional'));
		dd.indent = 3;
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_Fd_Distorsional'));
		dd.add_content_margin("donde:");
		dd.add_variable(dd.buscar_key(variables, 'key', 'B_Distorsional'));
		dd.indent = 4;
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_B_Distorsional'));
		dd.add_content_margin("donde:");
		dd.add_variable(dd.buscar_key(variables, 'key', 'L_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Lcr_Distorsional'));
		dd.indent = 5;
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_Lcr_Distorsional'));
		dd.add_content_margin("donde:");
		dd.add_variable(dd.buscar_key(variables, 'key', 'ho_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'u_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 't_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Ixf_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Iyf_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Ixyf_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'xo_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'hx_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Cwf_Distorsional'));

		dd.new_line();
		dd.indent = 4;
		dd.add_variable(dd.buscar_key(variables, 'key', 'Lm_Distorsional'));

		dd.new_line();
		dd.indent = 3;
		dd.add_variable(dd.buscar_key(variables, 'key', 'kOfe_Distorsional'));
		dd.indent = 4;
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_kOfe_Distorsional'));
		dd.add_content_margin("donde:");
		dd.add_variable(dd.buscar_key(variables, 'key', 'E_Torsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'G_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Jf_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'xo_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'hx_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Ixf_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Iyf_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Ixyf_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Cwf_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'L_Distorsional'));

		dd.new_line();
		dd.indent = 3;
		dd.add_variable(dd.buscar_key(variables, 'key', 'kOwe_Distorsional'));
		dd.indent = 4;
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_kOwe_Distorsional'));
		dd.add_content_margin("donde:");
		dd.add_variable(dd.buscar_key(variables, 'key', 'E_Torsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 't_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'u_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'ho_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'L_Distorsional'));

		dd.new_line();
		dd.indent = 3;
		dd.add_variable(dd.buscar_key(variables, 'key', 'kO_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'kOfg_Distorsional'));
		dd.indent = 4;
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_kOfg_Distorsional'));
		dd.add_content_margin("donde:");
		dd.add_variable(dd.buscar_key(variables, 'key', 'L_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Af_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'xo_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'hx_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Ixf_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Iyf_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Ixyf_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'yo_Distorsional'));

		dd.new_line();
		dd.indent = 3;
		dd.add_variable(dd.buscar_key(variables, 'key', 'kOwg_Distorsional'));
		dd.indent = 4;
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_kOwg_Distorsional'));
		dd.add_content_margin("donde:");
		dd.add_variable(dd.buscar_key(variables, 'key', 'ho_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 't_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Eweb_Distorsional'));
		dd.indent = 5;
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_Eweb_Distorsional'));
		dd.add_content_margin("donde:");
		dd.add_variable(dd.buscar_key(variables, 'key', 'f1_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'f2_Distorsional'));
		dd.new_line();
		dd.indent = 4;
		dd.add_variable(dd.buscar_key(variables, 'key', 'L_Distorsional'));

		dd.new_line();
		dd.indent = 1;
		dd.add_variable(dd.buscar_key(variables, 'key', 'Ad_Distorsional'));
		dd.indent = 2;
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_Ad_Distorsional'));
		dd.add_content_margin("donde:");
		dd.add_variable(dd.buscar_key(variables, 'key', 'My_Distorsional'));
		dd.indent = 3;
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_My_Distorsional'));
		dd.add_content_margin("donde:");
		dd.add_variable(dd.buscar_key(variables, 'key', 'Sfy_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Fy_Distorsional'));

		dd.new_line();
		dd.indent = 2;
		dd.add_variable(dd.buscar_key(variables, 'key', 'Mcrd_Distorsional'));
		dd.indent = 3;
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_Mcrd_Distorsional'));
		dd.add_content_margin("donde:");
		dd.add_variable(dd.buscar_key(variables, 'key', 'Sf_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Fd_Distorsional'));
		dd.indent = 4;
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_Fd_Distorsional'));
		dd.add_content_margin("donde:");
		dd.add_variable(dd.buscar_key(variables, 'key', 'B_Distorsional'));
		dd.indent = 5;
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_B_Distorsional'));
		dd.add_content_margin("donde:");
		dd.add_variable(dd.buscar_key(variables, 'key', 'L_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Lcr_Distorsional'));
		dd.indent = 6;
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_Lcr_Distorsional'));
		dd.add_content_margin("donde:");
		dd.add_variable(dd.buscar_key(variables, 'key', 'ho_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'u_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 't_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Ixf_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Iyf_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Ixyf_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'xo_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'hx_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Cwf_Distorsional'));

		dd.new_line();
		dd.indent = 5;
		dd.add_variable(dd.buscar_key(variables, 'key', 'Lm_Distorsional'));

		dd.new_line();
		dd.indent = 4;
		dd.add_variable(dd.buscar_key(variables, 'key', 'kOfe_Distorsional'));
		dd.indent = 5;
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_kOfe_Distorsional'));
		dd.add_content_margin("donde:");
		dd.add_variable(dd.buscar_key(variables, 'key', 'E_Torsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'G_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Jf_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'xo_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'hx_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Ixf_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Iyf_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Ixyf_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Cwf_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'L_Distorsional'));

		dd.new_line();
		dd.indent = 4;
		dd.add_variable(dd.buscar_key(variables, 'key', 'kOwe_Distorsional'));
		dd.indent = 5;
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_kOwe_Distorsional'));
		dd.add_content_margin("donde:");
		dd.add_variable(dd.buscar_key(variables, 'key', 'E_Torsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 't_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'u_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'ho_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'L_Distorsional'));

		dd.new_line();
		dd.indent = 4;
		dd.add_variable(dd.buscar_key(variables, 'key', 'kO_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'kOfg_Distorsional'));
		dd.indent = 5;
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_kOfg_Distorsional'));
		dd.add_content_margin("donde:");
		dd.add_variable(dd.buscar_key(variables, 'key', 'L_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Af_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'xo_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'hx_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Ixf_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Iyf_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Ixyf_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'yo_Distorsional'));

		dd.new_line();
		dd.indent = 4;
		dd.add_variable(dd.buscar_key(variables, 'key', 'kOwg_Distorsional'));
		dd.indent = 5;
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_kOwg_Distorsional'));
		dd.add_content_margin("donde:");
		dd.add_variable(dd.buscar_key(variables, 'key', 'ho_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 't_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Eweb_Distorsional'));
		dd.indent = 6;
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_Eweb_Distorsional'));
		dd.add_content_margin("donde:");
		dd.add_variable(dd.buscar_key(variables, 'key', 'f1_Distorsional'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'f2_Distorsional'));
		dd.new_line();
		dd.indent = 5;
		dd.add_variable(dd.buscar_key(variables, 'key', 'L_Distorsional'));
	}

	/****************************************/
	/*     DISENO DE MIEMBROS A CORTANTE    */
	/****************************************/
	var solicitacion = dd.buscar_key(report['solicitaciones'], 'idSolicitacion', 3);
	if(solicitacion['item'] != null)
	{
		// pagina 1
		var variables = solicitacion['item']['resultado']['listaVariables'];

		dd.indent = 0;
		dd.titulo_h1("DISEÑO DE MIEMBROS A CORTANTE");
		dd.add_content_margin("El diseño debe cumplir:");
		dd.add_cumplimiento(solicitacion['item'], '≤1');
		dd.add_content_margin("donde:");
		dd.add_variable(dd.buscar_key(variables, 'key', 'Vu'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Ov'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Vn'));

		dd.indent = 1;
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_Vn'));
		dd.add_content_margin("donde:");
		dd.add_variable(dd.buscar_key(variables, 'key', 'Aw'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Fv'));
		dd.indent = 2;
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_Para_t⁄h_1'), '<1');
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_Fv'));
		dd.add_content_margin("donde:");
		dd.add_variable(dd.buscar_key(variables, 'key', 'h'));
		dd.add_variable(dd.buscar_key(variables, 'key', 't'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'E'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Kv'));
		//dd.add_variable(dd.buscar_key(variables, 'key', 'Fy'));
	}

	/**************************************************************************************/
	/*     DISENO DE MIEMBROS SOLICITADOS POR FUERZAS COMBINADAS DE FLEXIÓN Y CORTANTE    */
	/**************************************************************************************/
	var solicitacion = dd.buscar_key(report['solicitaciones'], 'idSolicitacion', 6);
	if(solicitacion['item'] != null)
	{
		// pagina 1
		var variables = solicitacion['item']['resultado']['listaVariables'];

		dd.indent = 0;
		dd.titulo_h1("DISEÑO DE MIEMBROS SOLICITADOS POR FUERZAS COMBINADAS\nDE FLEXIÓN Y CORTANTE");
		dd.add_content_margin("El diseño debe cumplir:");
		dd.add_cumplimiento(solicitacion['item'], '≤1');
		dd.add_content_margin("donde:");
		dd.add_variable(dd.buscar_key(variables, 'key', 'Mu'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Ob'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Mn'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Vu'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Ov'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Vn'));
	}

	/********************************************/
	/*     DISENO DE MIEMBROS A ARRUGAMIENTO    */
	/********************************************/
	var solicitacion = dd.buscar_key(report['solicitaciones'], 'idSolicitacion', 5);
	if(solicitacion['item'] != null)
	{
		// pagina 1
		var variables = solicitacion['item']['resultado']['listaVariables'];

		dd.indent = 0;
		dd.titulo_h1("DISEÑO DE MIEMBROS A ARRUGAMIENTO");
		dd.add_content_margin("El diseño debe cumplir:");
		dd.add_cumplimiento(solicitacion['item'], '≤1');
		dd.add_content_margin("donde:");
		dd.add_variable(dd.buscar_key(variables, 'key', 'Pu'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Ow'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Pn'));
		dd.indent = 1;
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_Pn'));
		dd.add_content_margin("donde:");
		dd.add_variable(dd.buscar_key(variables, 'key', 'C'));
		dd.add_variable(dd.buscar_key(variables, 'key', 't'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Fy'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'O'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'R'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'CR'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'N'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'CN'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'h'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Ch'));
		dd.new_line();
		dd.indent = 0;
		dd.add_content_margin("Los factores de seguridad y coeficientes ɸw, C, CR, CN y Ch son tomadas de la  tabla C3.4.1-3 del AISI S100-12 o F.4.3.3-3 del NSR-10.");
	}

	/******************************************************************************************/
	/*     DISENO DE MIEMBROS SOLICITADOS POR FUERZAS COMBINADAS DE ARRUGAMIENTO Y FLEXION    */
	/******************************************************************************************/
	var solicitacion = dd.buscar_key(report['solicitaciones'], 'idSolicitacion', 7);
	if(solicitacion['item'] != null)
	{
		// pagina 1
		var variables = solicitacion['item']['resultado']['listaVariables'];

		dd.indent = 0;
		dd.titulo_h1("DISEÑO DE MIEMBROS SOLICITADOS POR FUERZAS COMBINADAS DE\nARRUGAMIENTO Y FLEXIÓN");
		dd.add_content_margin("El diseño debe cumplir:");
		dd.add_cumplimiento(solicitacion['item'], '≤1.33');
		dd.add_content_margin("donde:");
		dd.add_variable(dd.buscar_key(variables, 'key', 'O'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Pu'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Mu'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Pn'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Mnxo'));
	}

	/****************************************/
	/*               Finalizo               */
	/****************************************/
	dd.pushStack();
}
