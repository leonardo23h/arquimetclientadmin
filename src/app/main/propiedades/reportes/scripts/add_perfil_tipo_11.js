function add_perfil_tipo_11(report, dd)
{
	/****************************************/
	/*               Titulo                 */
	/****************************************/
	dd.new_page();
	dd.indent = 0;
	dd.titulo_principal("REPORTE ACADÉMICO PARA {0} \nCOMPROBACIÓN DE PERFIL {1}\nACABADO {2}, Fy = {3} MPa \nREGLAMENTO NSR-10 / AISI S100-12", report['perfil']['tipoPerfil']['descripcion'], report['perfil']['nombre'], report['perfil']['acabadoPerfil']['descripcion'], report['perfil']['material']['fy']);

	/****************************************/
	/*         Tabla de propiedades         */
	/****************************************/
	// insertar titulo tabla propiedades
	dd.titulo_tabla("TABLA DE PROPIEDADES GEOMÉTRICAS");
	var contenido_tabla_propiedades =
			{
				table: {
					// headers are automatically repeated if the table spans over multiple pages
					// you can declare how many rows should be treated as headers
					headerRows: 0,
					widths: ['28%', '18%', '18%', '18%', '18%'],
					body: []
				},
				layout: dd.mergeAll(dd.layout_lineas_grises, dd.layout_no_padding, dd.layout_lineas_delgadas)
			};

	// Ancho de columnas de las propiedades de la tabla
	var ancho = 4; // ancho tabla = 5, menos la columna de imagen es 4

	// Titulo del cuadro
	var titulo_encabezado_tabla = dd.get_array(ancho + 1);
	titulo_encabezado_tabla[1] = {text: report['perfil']['nombre'], style: 'tableHeader', colSpan: ancho};
	contenido_tabla_propiedades['table']['body'][contenido_tabla_propiedades['table']['body'].length] = titulo_encabezado_tabla;

	var keys_propiedades_tabla = ["d", "h", "Tw", "-", "Bf", "Tf", "Peso Propio", "Area", "Ix", "Sx", "Zx", "rx","Iy","Sy","Zy","ry"];
	var N = keys_propiedades_tabla.length;

	// iterar de 4 en 4 propiedades
	for (var i = 0; i < N; i += ancho)
	{
		var encabezado = dd.get_array(ancho + 1);
		var valores = dd.get_array(ancho + 1);

		// llenar los valores
		for (var j = 0; (j < ancho) && (j + i < N); j++)
		{
			var find = keys_propiedades_tabla[j + i];
			var item = {key: '?' + find + '?', magnitud: '', valor: {text:'*???*', style:'no_encontrado'}};

			if(find == '-')
			{
				item = {key: '-', magnitud: '', valor: '-'};
			}
			else
			{
				var found = dd.buscar_key(report['propiedadesGeometricas'], 'key', find);
				if (found['item'] != null)
				{
					item = found['item'];
				}
			}
			var formato_encabezado = (item['magnitud'] != '') ? "{0} ({1})" : "{0}";
			encabezado[j + 1] = {text: dd.format(formato_encabezado, item['key'], item['magnitud']), style: 'tableHeader'};
			valores[j + 1] = {text: item['valor'], style: 'tableCell'};
		}
		contenido_tabla_propiedades['table']['body'][contenido_tabla_propiedades['table']['body'].length] = encabezado;
		contenido_tabla_propiedades['table']['body'][contenido_tabla_propiedades['table']['body'].length] = valores;
	}

	// inserto imagen con RowSpan dinamico
	var imagen_tabla = dd.getImage(report['imagenOrientacion']);
	contenido_tabla_propiedades['table']['body'][0][0] = dd.mergeAll(imagen_tabla, {alignment: 'center', rowSpan: contenido_tabla_propiedades['table']['body'].length});

	// inserto el contenido de la tabla
	dd.add_content(contenido_tabla_propiedades);

	/****************************************/
	/*          MIEMBROS A TENSION          */
	/****************************************/
	var solicitacion = dd.buscar_key(report['solicitaciones'], 'idSolicitacion', 4);
	if(solicitacion['item'] != null)
	{
		// pagina 1
		var variables = solicitacion['item']['resultado']['listaVariables'];

		dd.indent = 0;
		dd.titulo_h1("DISEÑO DE MIEMBROS A TENSIÓN");

		dd.add_content_margin("El diseño debe cumplir:");
		dd.add_cumplimiento(solicitacion['item'], '≤1');
		dd.add_content_margin("donde:");
		dd.add_variable(dd.buscar_key(variables, 'key', 'Tu'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Ot'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Tn'));
		dd.indent = 1;
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_Tn'));
		dd.add_content_margin("donde:");
		dd.add_variable(dd.buscar_key(variables, 'key', 'Ag'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Fy'));

	}

	/****************************************/
	/*         MIEMBROS A COMPRESION        */
	/****************************************/
	var solicitacion = dd.buscar_key(report['solicitaciones'], 'idSolicitacion', 1);
	if(solicitacion['item'] != null)
	{
		var variables = solicitacion['item']['resultado']['listaVariables'];

		// pagina 2
		dd.indent = 0;
		dd.titulo_h1("DISEÑO DE MIEMBROS A COMPRESIÓN");
		dd.add_content_margin("El diseño debe cumplir:");
		dd.add_cumplimiento(solicitacion['item'], '≤1');
		dd.add_content_margin("donde:");
		dd.add_variable(dd.buscar_key(variables, 'key', 'Pu'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Oc'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Pn'));
		dd.add_content_margin("La sección clasifica como un miembro sin elementos esbeltos según la tabla F.2.2.4–1a del NSR-10 o B4.1a del AISC 360-10, debido a que:");
		dd.add_content_margin("El ala no es esbelta:");
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_Ala'),'≤1');
		dd.add_content_margin("Y el alma no es esbelta:");
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_Alma'),'≤1');
		dd.add_content_margin("donde:");
		dd.add_variables(dd.buscar_keys(variables, 'key', ['b','t','h','tw','E','Fy']));

		dd.indent = 0;
		dd.titulo_h2("Resistencia a pandeo por flexión de miembros sin elementos esbeltos.");
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_Pn'));
		dd.add_content_margin("donde:");
		dd.add_variables(dd.buscar_keys(variables, 'key', ['Ag', 'Fcr']));

		dd.indent = 1;
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_Para_Frc'), '≤1');
		dd.add_content_margin("donde:");
		dd.add_variable(dd.buscar_key(variables, 'key', 'Fy'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Fe'));

		dd.indent = 2;
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_Fe'));
		dd.add_content_margin("donde:");
		dd.add_variables(dd.buscar_keys(variables, 'key', ['KL','r','E']));

		dd.indent = 0;
		dd.titulo_h2("Pandeo por torsión y pandeo por flexo-torsión de miembros sin elementos esbeltos.");
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_Pn'));
		dd.add_content_margin("donde:");
		dd.add_variables(dd.buscar_keys(variables, 'key', ['Ag', 'Fcr']));

		dd.indent = 1;
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_Para_Frc'), '≤1');
		dd.add_content_margin("donde:");
		dd.add_variable(dd.buscar_key(variables, 'key', 'Fy'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Fe'));

		dd.indent = 2;
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_Fe_Diferente'));
		dd.add_content_margin("donde:");
		dd.add_variables(dd.buscar_keys(variables, 'key', ['KzL','E','Cw','G','Ix','Iy','J']));
	}

	/****************************************/
	/*          MIEMBROS A FLEXION          */
	/****************************************/
	var solicitacion = dd.buscar_key(report['solicitaciones'], 'idSolicitacion', 2);
	if(solicitacion['item'] != null)
	{
		// pagina 1
		var variables = solicitacion['item']['resultado']['listaVariables'];

		dd.indent = 0;
		dd.titulo_h1("DISEÑO DE MIEMBROS A FELEXIÓN");
		dd.add_content_margin("El diseño debe cumplir:");
		dd.add_cumplimiento(solicitacion['item'], '≤1');
		dd.add_content_margin("donde:");
		dd.add_variable(dd.buscar_key(variables, 'key', 'Mu'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Ob'));
		dd.add_variable(dd.buscar_key(variables, 'key', 'Mn'));
		dd.add_content_margin('La sección clasifica una sección compacta según la tabla F.2.2.4–1b o B4.1b del AISC 360-10, debido a que:');
		dd.add_content_margin("El ala es compacta:");
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_Ala'),'≤1');
		dd.add_content_margin("Y el alma es compacta:");
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_Alma'),'≤1');
		dd.add_content_margin("donde:");
		dd.add_variables(dd.buscar_keys(variables, 'key', ['b','t','h','tw','E','Fy']));
		dd.add_content_margin('El momento nominal será el menor de los estados límites de Plastificación de la sección y Pandeo lateral-torsional.');

		dd.new_line();
		dd.titulo_h2('Plastificación de la sección (Momento Plástico)');
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_Mn'));
		dd.add_content_margin("donde:");
		dd.add_variables(dd.buscar_keys(variables, 'key', ['Fy','Zx']));

		dd.new_line();
		dd.titulo_h2('Pandeo lateral-torsional.');
		dd.add_content_margin('No se aplica el estado límite de pandeo lateral-torsional porque:');
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_PandeoLateralTorsional'),'≤1');
		dd.add_content_margin("donde:");
		dd.add_variables(dd.buscar_keys(variables, 'key', ['Lb','Lp']));
		dd.indent = 1;
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_Lp'));
		dd.add_content_margin("donde:");
		dd.add_variables(dd.buscar_keys(variables, 'key', ['ry','E','Fy']));

	}

	/****************************************/
	/*     DISENO DE MIEMBROS A CORTANTE    */
	/****************************************/
	var solicitacion = dd.buscar_key(report['solicitaciones'], 'idSolicitacion', 3);
	if(solicitacion['item'] != null)
	{
		// pagina 1
		var variables = solicitacion['item']['resultado']['listaVariables'];

		dd.indent = 0;
		dd.titulo_h1("DISEÑO DE MIEMBROS A CORTANTE");
		dd.add_content_margin("El diseño debe cumplir:");
		dd.add_cumplimiento(solicitacion['item'], '≤1');
		dd.add_content_margin("donde:");
		dd.add_variables(dd.buscar_keys(variables, 'key', ['Vu','Ov','Vn']));
		dd.add_content_margin('El cortante nominal para el estado límite de fluencia por cortante y pandeo por cortante se calculan con:');
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_Vn'));
		dd.add_content_margin("donde:");
		dd.add_variables(dd.buscar_keys(variables, 'key', ['Fy','Aw']));

		dd.indent = 1;
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_Aw'));
		dd.add_content_margin("donde:");
		dd.add_variables(dd.buscar_keys(variables, 'key', ['tw','d']));
		dd.new_line();
		dd.indent = 0;
		dd.add_variables(dd.buscar_keys(variables, 'key', ['Cv']));
		dd.indent = 1;
		dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Imagen_Para_Cv'),'≥1');
		dd.add_content_margin('donde:');
		dd.add_variables(dd.buscar_keys(variables, 'key', ['E','Fy','h','tw']));
	}

	/****************************************/
	/*               Finalizo               */
	/****************************************/
	dd.pushStack();
}
