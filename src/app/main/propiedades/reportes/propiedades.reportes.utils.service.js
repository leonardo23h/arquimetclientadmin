/**
 * Created by stackpointer on 16/08/17.
 */
(function (angular) {
  "use strict";

  angular
    .module('app.propiedades')
    .service('utilsService',[
      'solicitacionesService',
      '$filter',
      'printableChart',
      utilsService
    ]);

  function utilsService(solicitacionesService, $filter, printableChart) {

    var vm = this;

    vm.printableChart = printableChart;
    vm.getEncabezado = getEncabezado;
    vm.getGraficaResultados = getGraficaResultados;
    vm.solicitacionesService = solicitacionesService;
    vm.getSolicitacion =  getSolicitacion;
    vm.stringToFloat = stringToFloat;
    vm.getSolicitacionDiseno = getSolicitacionDiseno;
    vm.getTableTipoUno = getTableTipoUno;
    vm.getTablaTipoDos = getTablaTipoDos;
    vm.getTableTipoTres = getTableTipoTres;
    // Styles del reporte
    vm.style = {
                    h1: {
                        margin: [0, 30, 0, 0],
                        fontSize: 16,
                        bold: true
                    },
                    h2: {
                        margin: [0, 0, 0, 20],
                        fontSize: 14,
                        bold: true,
                        alignment: 'center'
                    },
                    h3:{
                        margin: [0, 20, 0, 20],
                        fontSize: 12,
                        bold: true,
                        alignment: 'center'
                    },
                    texto: {
                        fontSize: 10,
                        bold: false,
                    },
                    encabezado: {
                        alignment : 'center',
                        margin: [80, 0, 0, 40]

                    },
                    tableBodyTitle:{
                        fontSize: 10,
                        bold: false

                    },
                    tableBody: {
                        fontSize: 6,
                        bold: false,
                        margin: [0, 2, 0, 2],
                        alignment:'center'
                    },
                    tableBodyDos:{
                        fontSize: 9,
                        bold: false,
                        alignment:'center',
                        margin: [0, 10, 0, 5]
                    }

                };




    // Funcion que construye y devuelve el HEADER del reporte
    function getEncabezado(json){
        var texto = 'REPORTE TÉCNICO PARA ' + json.tipoPerfil.descripcion.toUpperCase();
        var obj = {
            style: 'encabezado',
            layout: 'noBorders',
			table: {
    			body: [
					[{stack:[{image: json.imagenOrientacion, width: 70}]}, {text: texto, style: 'h1'}],
				]
    		}

        };
        return obj;
    }


    // Funcion que construye y devuelve La grafica del reporte
    function getGraficaResultados(){
      var texto = {text: 'GRÁFICA DE RESULTADOS', style: 'h3'};
      var imagen = {image: vm.printableChart.imgsrc, width: 525, height:235};

      var obj = {
        stack: [
          texto,
          imagen
        ]
      };
      return obj;
    }

    // Funcion que devuelve el nombre de una solicitacion segun si Id
    function getSolicitacion(id){
        var descripcion = '';
        vm.solicitacionesService.list.forEach(function(row) {
            if(id === row.codigo){
                descripcion = row.descripcion.toUpperCase();
            }
        });
        return descripcion;
    }



    // Funcion que convierte un Float a String
    function stringToFloat(valor){
        var number = parseFloat(valor)
        return number;
    }


    // Function que devuelve un array con la filas en columnas que se visualizara en la sesion SOLICITACIONES DE DISEÑO
    function getSolicitacionDiseno(configuracion){
        var fila = [];

        configuracion.forEach(function(row){
            var col = {columns: []};
            var valor = "";

            switch (row.key)
            {
              case "cargasOpuestasEspaciadasA":
                if(row.value === "True")
                {
                  col.columns.push({text: row.descripcion, style: 'texto'}, {});
                }
                break;
              case "sujetoAlApoyoA":
                if(row.value === "True")
                {
                  col.columns.push({text: row.descripcion, style: 'texto'}, {});
                }
                break;
              case "localizacionApoyoExteriorA":
                if(row.value === "True")
                {
                  col.columns.push({text: row.descripcion, style: 'texto'}, {});
                }
                break;
              case "cargasOpuestasEspaciadasAyF":
                if(row.value === "True")
                {
                  col.columns.push({text: row.descripcion, style: 'texto'}, {});
                }
                break;
              case "sujetoAlApoyoAyF":
                if(row.value === "True")
                {
                  col.columns.push({text: row.descripcion, style: 'texto'}, {});
                }
                break;
              case "localizacionApoyoExteriorAyF":
                if(row.value === "True")
                {
                  col.columns.push({text: row.descripcion, style: 'texto'}, {});
                }
                break;
              default:
                valor = $filter('round')(row.value, 2)+row.unidad;
                col.columns.push({text: row.descripcion, style: 'texto'}, {text: valor,  alignment : 'right', style: 'texto'});
                break;
            }

            fila.push(col);
        });
        return fila;
    }

    /* Devuelve una tabla de 8 columnas. Se utiliza para las siguientes licitaciones:
        COMPRESION
        FLEXION
        ARRUGAMIENTO
    */
    function getTableTipoTres(data, configuracion, perfil, solicitacionId){
        var body = [];

        var header1 = [{text:perfil.toUpperCase(), style: 'tableBodyTitle', colSpan: 8,  bold:true, alignment:'center'},{},{},{},{},{},{},{}];
        body.push(header1); // Se agrega el header al body de la tabla

        // Recorremos los acabados
        data.perfiles.forEach(function(row){
            var img = row.imagenCumplimiento;
            var dataRow = [];
            var textHeader2 = row.perfil.nombre + ' Fluencia ' + row.perfil.material.fy + ' MPa ' + row.perfil.material.acabado + ' NSR-10 / AISI S100-12';
            var cel = {stack:[
                                {
                                table: {
                                    body: [
                                    [{text:textHeader2, style:'tableBodyTitle', bold: true, alignment:'center'}, {image:img, width: 10, alignment:'center'}]
                                    ]
                                },
                                layout: 'noBorders',
                                margin: [70, 0, 0, 0]
                                }
                            ], alignment:'center', colSpan: 8};



           var header2 = [cel,{},{},{},{},{},{},{}];
            body.push(header2);
            dataRow = dataRow.concat(buildFila(row.resultado[0], configuracion[1].key, configuracion[0].unidad, solicitacionId));
            body.push(dataRow);
            dataRow = [];
            dataRow = dataRow.concat(buildFila(row.resultado[1], vm.solicitacionesService.selected.chart.options.chart.yAxis1.axisLabel, configuracion[1].unidad, solicitacionId));
            body.push(dataRow);
        });

        var tabla = {
              style: 'tableBody',
              alignment : 'center',
              table: {
                  widths: ['14%', '12%', '12%', '12%', '12%', '12%', '12%', '12%'],
                  headerRows:1 ,
                  // keepWithHeaderRows: 1,
                  body: body
                }
              };

        return tabla;
    }

    // Devuelve una fila de 8 columnas. Solo se utiliza para generar filas de la tabla tipo 3
    function buildFila(resultado, titulo, magnitud, solicitacionId){
        var fila = [];
        var texto = titulo + '('+ magnitud + ')';
        fila.push({text:texto , style: 'tableBody', bold: true, alignment:'left'});

        switch (solicitacionId){
          case 5:
            fila.push({text:$filter('round')(resultado[0],2),  style: 'tableBody'});
            fila.push({text:$filter('round')(resultado[3],2), style: 'tableBody'});
            fila.push({text:$filter('round')(resultado[6],2),  style: 'tableBody'});
            fila.push({text:$filter('round')(resultado[9],2),  style: 'tableBody'});
            fila.push({text:$filter('round')(resultado[12],2),  style: 'tableBody'});
            fila.push({text:$filter('round')(resultado[15],2),  style: 'tableBody'});
            fila.push({text:$filter('round')(resultado[resultado.length - 1],2), style: 'tableBody'});
            break;
          default:
            fila.push({text:$filter('round')(resultado[0],2),  style: 'tableBody'});
            fila.push({text:$filter('round')(resultado[8],2), style: 'tableBody'});
            fila.push({text:$filter('round')(resultado[8 * 2],2),  style: 'tableBody'});
            fila.push({text:$filter('round')(resultado[8 * 3],2),  style: 'tableBody'});
            fila.push({text:$filter('round')(resultado[8 * 4],2),  style: 'tableBody'});
            fila.push({text:$filter('round')(resultado[8 * 5],2),  style: 'tableBody'});
            fila.push({text:$filter('round')(resultado[resultado.length - 1],2), style: 'tableBody'});
            break;
        }

        return fila;
    }


    /* Retorna una tabla de 4 columnas las cuales se utilizan para las siguientes licitaciones:
        FLEXION Y CORTANTE
        ARRUGAMIENTO Y FLEXION
    */
    function getTablaTipoDos(data, titulos){
        var body = [];
        var header = buildHeader(titulos);
        // Se agrega la fila del HEADER de la tabla.
        body.push(header);
        // Se recorren los acabados y se arman las filas en la tabla
        data.perfiles.forEach(function(row) {
            var img = row.imagenCumplimiento;
            var dataRow = [];
            // Texto de la columna PERFIL
            var textPerfiles = row.perfil.nombre + '\n Fluencia ' + row.perfil.material.fy + ' MPa ' + row.perfil.material.acabado + '\n NSR-10 / AISI S100-12';
            // Celda de la columna PERFIL
            var celPerfil = {text:textPerfiles, style: 'tableBodyDos', margin: [0, 0, 0, 0], alignment:'left'};
            // Celda de la columna Mu
            var celMu = {text:$filter('round')(row.resultado[0][1],2),  style: 'tableBodyDos'};
            // Celda de la columna  Vu - Pu
            var celVu = {text:$filter('round')(row.resultado[1][1],2),  style: 'tableBodyDos'};
            // Celda de la columna EFICIENCIA
            var celEficiencia = {stack:[
                    {
                    table: {
                        body: [
                        [{text:$filter('round')(row.factor, 2), style:'tableBodyDos', margin: [0, 0, 0, 0], alignment:'left'}, {image:img, width: 10, alignment:'center'}]
                        ]
                    },
                    layout: 'noBorders',
                    margin: [40, 9, 0, 0]
                    }
                ], alignment:'center'};
            dataRow.push(celPerfil); // Se agrega la celda PERFIL
            dataRow.push(celEficiencia); // Se agrega la celda EFICIENCIA
            dataRow.push(celMu); // Se agrega la celda Mu
            dataRow.push(celVu);// Se agrega la celda Vu - Pu
            body.push(dataRow);
        });


        var tabla = {
              alignment : 'center',
              style: 'tableBody',
              table: {
                  widths: ['40%', '20%', '20%', '20%'],
                  headerRows:1 ,
                  // keepWithHeaderRows: 1,
                  body: body
                }
              };

        return tabla;
    }

    /**
     *  Devuelve una tabla de 3 columnas. Se utiliza para las siguientes licitaciones:
     *  TENSION
     *  CORTANTE
     */
    function getTableTipoUno(data, titulos){

        var body = [];
        var header = buildHeader(titulos);
        // Se agrega la fila del HEADER de la tabla.
        body.push(header);
        data.perfiles.forEach(function(row) {
          var img = row.imagenCumplimiento;
          var dataRow = [];
          // Texto de la columna PERFIL
          var textPerfiles = row.perfil.nombre + '\n Fluencia ' + row.perfil.material.fy + ' MPa ' + row.perfil.material.acabado + '\n NSR-10 / AISI S100-12';
          var celPerfil = {text:textPerfiles, style: 'tableBodyDos', alignment:'left',  margin: [0, 0, 0, 0]};
          var celTn = {text:$filter('round')(row.resultado[1][1],2),  style: 'tableBodyDos', alignment:'center', margin: [0, 10, 0, 5]};
          var celEficiencia = {stack:[
                {
                  table: {
                    body: [
                      [{text:$filter('round')(row.factor, 2),  style: 'tableBodyDos',  margin: [0, 0, 0, 0], alignment:'left'}, {image:img, width:10, alignment:'center',  margin: [0, 0, 0, 0]}]
                    ]
                  },
                  layout: 'noBorders',
                  margin: [60, 10, 0, 0]
                }
              ], alignment:'center'};
          dataRow.push(celPerfil); // Se agrega la celda PERFIL
          dataRow.push(celEficiencia); // Se agrega la celda EFICIENCIA
          dataRow.push(celTn); // Se agrega la celda Tn
          body.push(dataRow);
        });

        var tabla = {
              alignment : 'center',
              style: 'tableBody',
              table: {
                  widths: ['40%', '30%', '30%'],
                  headerRows:1 ,
                  // keepWithHeaderRows: 1,
                  body: body
                }
              };

        return tabla;
    }


    // Devuelve un array de objetos, los cuales representan los titulos de cada columna de una tabla
    function buildHeader(titulos){
        var header = [];
        titulos.forEach(function(row){
            header.push({text: row, style: 'tableBodyTitle', bold:true, alignment:'center'});
        });
        return header;
    }

  }
})(window.angular);
