/**
 * Created by stackpointer on 16/08/17.
 */

(function (angular) {
  "use strict";

  angular
    .module('app.propiedades')
    .service('reporteService', [
      'solicitacionesService',
      'utilsService',
      'propiedades',
     reporteService
    ]);



  function reporteService(solicitacionesService, utilsService, propiedades) {

    var vm = this;

    //functions
    vm.solicitaciones = solicitacionesService;
    vm.utilsService = utilsService;
    vm.getReporte = getReporte;
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    function getReporte(isTecnico, perfiles, unidadId, solicitacion, orientacion) {
      switch(isTecnico)
      {
        case true:
          buildReporteTecnico();
          break;
        case false:
          buildReporteAcademico(perfiles, unidadId, solicitacion, orientacion);
          break
      }
    }

    function buildReporteAcademico(perfiles, unidadId, solicitacion, orientacion) {
      var r = new reports();
      propiedades.calcularAcademico(perfiles, unidadId, solicitacion, orientacion)
        .then(function (solicitacion) {
          r.add_json(solicitacion.reporteAcademico);
          pdfMake.createPdf(r.output()).open();
        });

    }

    function buildReporteTecnico(){
      var solicitacionId = vm.solicitaciones.selected.reporteTecnico.resultado[0].solicitacionId;
      var json = vm.solicitaciones.selected.reporteTecnico;
      switch(solicitacionId){
        case 1:
          buildReporteTres(solicitacionId);
          break;
        case 2:
          buildReporteTres(solicitacionId);
          break;
        case 3:
          var titulos = [json.tipoPerfil.descripcion.toUpperCase(),'EFICIENCIA','Vn(Kn)'];
          buildReporteUno(titulos);
          break;
        case 4:
          var titulos = [json.tipoPerfil.descripcion.toUpperCase(),'EFICIENCIA','Tn(Kn)'];
          buildReporteUno(titulos);
          break;
        case 5:
          buildReporteTres(solicitacionId);
          break;
        case 6:
          var titulos = ['PERFIL','EFICIENCIA','Mu/Mn','Vu/Vn'];
          buildReporteDos(titulos);
          break;
        case 7:
          var titulos = ['PERFIL','EFICIENCIA','Mu/Mn','Pu/Pn'];
          buildReporteDos(titulos);
          break;
      }

    }


    // Funcion que construye un reporte que contiene una tabla de 8 columnas
    function buildReporteTres(solicitacionId){
      var json = vm.solicitaciones.selected.reporteTecnico;
      var calculo = json.resultado[0];

      // Construccion de la Tabla
      var tabla = vm.utilsService.getTableTipoTres(calculo, json.configuracion, json.tipoPerfil.descripcion, solicitacionId);
      var encabezado  = vm.utilsService.getEncabezado(json);
      var subtitulo = 'DISEÑO DE MIEMBROS A '+ vm.utilsService.getSolicitacion(calculo.solicitacionId);
      var graficaResultados = vm.utilsService.getGraficaResultados();
      var content = [];
      // Se agrega el encabezado
      content.push(encabezado);
      // Se agrega el subtitulo
      content.push({text: subtitulo, style:'h2'});
      // Se agrega el titulo de la seccion de Solicitud de diseño
      content.push({text: 'SOLICITACIONES DE DISEÑO', style: 'h3'});
      // Se agrega el contenido de la Seccion de SOLICITUD DE DISEÑO
      content = content.concat(vm.utilsService.getSolicitacionDiseno(json.configuracion));
      // se agrega la grafica de resultados
      content.push(graficaResultados);
      content.push({text: 'TABLA DE RESULTADOS', style: 'h3'});
      // Se agrega la Tabla
      content.push(tabla);

      var dd = {
        content: content,
        styles: vm.utilsService.style
      };

      pdfMake.createPdf(dd).open();
    }

    // Funcion que construye un reporte que contiene una tabla de 4 columnas
    function buildReporteDos(titulos){
      var json = vm.solicitaciones.selected.reporteTecnico;
        var calculo = json.resultado[0];

        // Construccion de la Tabla
        var tabla = vm.utilsService.getTablaTipoDos(calculo, titulos);
        var encabezado  = vm.utilsService.getEncabezado(json);
        var subtitulo = 'DISEÑO DE MIEMBROS SOLICITADOS POR FUERZAS COMBINADAS DE '+ vm.utilsService.getSolicitacion(calculo.solicitacionId);
        var graficaResultados = vm.utilsService.getGraficaResultados();
        var content = [];
        // Se agrega el encabezado
        content.push(encabezado);
        // Se agrega el subtitulo
        content.push({text: subtitulo, style:'h2'});
        // Se agrega el titulo de la seccion de Solicitud de diseño
        content.push({text: 'SOLICITACIONES DE DISEÑO', style: 'h3'});
        // Se agrega el contenido de la Seccion de SOLICITUD DE DISEÑO
        content = content.concat(vm.utilsService.getSolicitacionDiseno(json.configuracion));
        content.push(graficaResultados);
        content.push({text: 'TABLA DE RESULTADOS', style: 'h3'});
        // Se agrega la Tabla
        content.push(tabla);
        var dd = {
          content: content,
          styles: vm.utilsService.style
        };

        pdfMake.createPdf(dd).open();
    }

    // Funcion que construye un reporte que contiene una tabla de 3 columnas
    function buildReporteUno(titulos){
      var json = vm.solicitaciones.selected.reporteTecnico;
      var calculo = json.resultado[0];

      // Construccion de la Tabla
      var tabla = vm.utilsService.getTableTipoUno(calculo, titulos);
      var encabezado  = vm.utilsService.getEncabezado(json);
      var subtitulo = 'DISEÑO DE MIEMBROS A '+ vm.utilsService.getSolicitacion(calculo.solicitacionId);
      var graficaResultados = vm.utilsService.getGraficaResultados();
      var content = [];
      // Se agrega el encabezado
      content.push(encabezado);
      // Se agrega el subtitulo
      content.push({text: subtitulo, style:'h2'});
      // Se agrega el titulo de la seccion de Solicitud de diseño
      content.push({text: 'SOLICITACIONES DE DISEÑO', style: 'h3'});
      // Se agrega el contenido de la Seccion de SOLICITUD DE DISEÑO
      content = content.concat(vm.utilsService.getSolicitacionDiseno(json.configuracion));
      content.push(graficaResultados);
      content.push({text: 'TABLA DE RESULTADOS', style: 'h3'});
      // Se agrega la Tabla
      content.push(tabla);

      var dd = {
        content: content,
        styles: vm.utilsService.style
      };
      pdfMake.createPdf(dd).open();
    }
  }
})(window.angular);
