/**
 * Created by stackpointer on 25/04/17.
 */
/**
 * Created by stackpointer on 5/04/17.
 */
/**
 * Created by aesleider on 30/03/2017.
 */
(function (angular) {
  'use strict';
  angular
    .module('app.propiedades')
    .controller('TabPropDesignController',[
      'unidadesService',
      'propiedades',
      'perfileria',
      'solicitacionesService',
      'configSolicitaciones',
      'reporteService',
      'printableChart',
      '$uibModal',
      TabPropDesignController
    ]);

  function TabPropDesignController(unidadesService, propiedades, perfileria, solicitacionesService,  configSolicitaciones, reporteService, printableChart, $uibModal) {

    var vm = this;

    vm.perfileria = perfileria;
    vm.solicitaciones = solicitacionesService;
    vm.configSolicitaciones = configSolicitaciones;
    vm.unidades = unidadesService;
    vm.printableChart = printableChart;

    vm.perfileria.getAcabados(1)
      .then(function (vmA) { return vm.solicitaciones.reqSolicitaciones(vmA.perfilCurrentType) })
      .then(function (vmS) { calcular() });

    //inject properties

    /*
     * functions
     * */
    vm.getAcabados = getAcabados;
    vm.exportData = propiedades.exportData;
    vm.solicitacionChange = solicitacionChange;
    vm.openConfigReporte = openConfigReporte;

      /*******************************************************************/

    function getAcabados(id)
    {
      vm.perfileria.getAcabados(id)
        .then(function (vmA) { return vm.solicitaciones.reqSolicitaciones(vmA.perfilCurrentType) })
        .then(function (vmS) { calcular() })
    }

    function solicitacionChange()
    {
      calcular();
    }

    function calcular() {

      var perfiles = perfileria.selectedPerfiles,
        unidadId = unidadesService.unitSelected.codigo,
        solicitacion= solicitacionesService.selected,
        orientacion = vm.perfileria.orientaciones.selected;

      propiedades.calcular(perfiles, unidadId, solicitacion, orientacion)
        .then(function (solicitacion) {
          solicitacion.chart.scope.api.refresh();
          setTimeout(function () {
            vm.printableChart.setSVG();
          },  3000);
        });
    }

    function openConfigReporte()
    {
      var parentElem = angular.element(document.body);
      var modalInstance = $uibModal.open({
        animation: true,
        templateUrl: 'app/main/propiedades/modals/configReporte/configReporte.html',
        controller: 'configReporteInstanceController',
        controllerAs: 'vm',
        size: 'md',
        appendTo: parentElem,
        resolve: {
          solicitaciones: solicitacionesService
        }
      });

      modalInstance.result.then(function (configReport) {
        //luego de obtener configuraciones para reporte.
        var perfiles = perfileria.selectedPerfiles,
          unidadId = vm.unidades.unitSelected.codigo,
          solicitacion= solicitacionesService.selected,
          orientacion = perfileria.orientaciones.selected;

        reporteService.getReporte(configReport.isTecnico, perfiles, unidadId, solicitacion, orientacion);

      }, function () {
        //luego de cancelar generar reporte.
      });

    }

  }

})(window.angular);
