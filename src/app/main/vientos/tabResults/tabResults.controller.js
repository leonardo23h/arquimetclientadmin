/**
 * Created by stackpointer on 5/04/17.
 */
(function (angular) {
  'use strict';
  angular
    .module('app.vientos')
    .controller('TabVientosResultsController',[
      'vientosData',
      'vientosX3dom',
      TabVientosResultsController
    ]);

  function TabVientosResultsController(vientosData, vientosX3dom) {
    var vm = this;

    //injects
    vm.x3d = vientosX3dom.x3d;
    vm.scene = vm.x3d.scene;
    vm.superTransform = vm.scene.superTransform;

    //data
    vm.vientosData = vientosData;
    vm.propiedades = vm.vientosData.data.propiedades;
    vm.informacion = vm.vientosData.data.informacion;
    vm.isCollapsed = {};

    // functions
    //vm.validateNumber = onlyNumbers.validateNumber;
    vm.calcularA = calcularA;


    //////////////////////////////////////////////////////////////////////
    function calcularA(ev) {

      vm.informacion.anchoEdificacion < vm.informacion.largoEdificacion ?
        vm.informacion.a = vm.informacion.anchoEdificacion * 0.1:
        vm.informacion.a = vm.informacion.largoEdificacion * 0.1;

    }


  }

})(window.angular);
