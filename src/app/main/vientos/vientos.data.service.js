/**
 * Created by stackpointer on 24/06/17.
 */
(function(angular) {
  'use strict';

  angular
    .module('app.vientos')
    .factory('vientosData', [
      'unidadesService',
      'vientosService',
      vientosDataService
    ]);

  function vientosDataService(unidadesService, vientosService) {
    var vm = {};

    //data
    vm.data = {};
    vm.data.propiedades = {};

    vm.data.propiedades.direccionViento = 'N';
    vm.data.propiedades.tipoCubierta = '1A';
    vm.data.propiedades.tipoEdificacion = 'C';
    vm.data.propiedades.exposicion = 'B';
    vm.data.propiedades.ocupacion = 'I';
    vm.data.propiedades.region = 'NPH';
    vm.data.propiedades.diseno = 'SPRFV';
    vm.data.propiedades.flujoViento = 'L';

    vm.data.informacion = {};
    vm.data.informacion.zona = '1';
    vm.data.informacion.anchoEdificacion = 9;
    vm.data.informacion.largoEdificacion = 18;
    vm.data.informacion.alturaCornisa = 5;
    vm.data.informacion.alturaCumbrera = 5;
    vm.data.informacion.factorTopografico = 1;
    vm.data.informacion.velocidad = 36.1;
    vm.data.informacion.coeficientePerdido = 0.072;
    vm.data.informacion.exponentePerdido = 0.8;
    vm.data.informacion.relacionAmortiguamiento = 0.05;
    vm.data.informacion.areaEfectiva = 1;
    vm.data.informacion.factorEfectoRafaga = 0.85;
    vm.data.informacion.a = 1;

    vm.calculos = {};
    vm.calculos.results = [];
    vm.calculos.reporte = {};

    //methods
    vm.calcular = obtenerResultados;

    return vm;

    ////////////////////////////////////////////////////////////////
    function obtenerResultados() {
      vm.data.propiedades.unidadDeMedida = unidadesService.unitSelected.codigo;

      return calcular(vm.data)
        .then(function (response) {
          vm.calculos.results = response.resultado;
          vm.calculos.reporte = response.reporte;
          return vm.calculos;
        })
    }

    function calcular(json) {
      return vientosService.calcular(json)
        .then(calcularComplete)
        .catch(calcularFailed);

      function calcularComplete(response) {
        return response;
      }

      function calcularFailed(err) {
        return err;
      }
    }
  }
})(window.angular);
