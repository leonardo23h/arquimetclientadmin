/**
 * Created by stackpointer on 7/07/17.
 */

(function (angular) {
  'use strict';

  angular
    .module('app.vientos')
    .controller('MapaVelocidadModalInstanceController', ['$uibModalInstance' ,MapaVelocidadModalInstanceController]);

  function MapaVelocidadModalInstanceController($uibModalInstance) {
    var vm = this;

    vm.ok = function () {
      $uibModalInstance.close();
    };

  }
})(window.angular);
