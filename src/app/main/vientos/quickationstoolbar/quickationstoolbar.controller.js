/**
 * Created by stackpointer on 16/05/17.
 */

(function() {
  'use strict';

  angular
    .module('app.vientos')
    .controller('VientosQuickationstoolbarController', [
      'unidadesService',
      'vientosData',
      'reporteVientosService',
      VientosQuickationstoolbarController
    ]);

  /** @ngInject */
  function VientosQuickationstoolbarController(unidadesService, vientosData, reporteVientosService) {
    var vm = this;

    vm.unidades = unidadesService;
    vm.vientosData = vientosData;


    //vm.openGenerarReporte = openGenerarReporte;

    (function () {
      vm.unidades.reqUnidades();
    })();

    vm.calcular = calcular;
    vm.downloadPDF = downloadPDF;

    /*******************************************************************/
    function calcular() {
      vm.vientosData.calcular();
    }

    function downloadPDF()
    {
      reporteVientosService.getReporte();
    }

  }
})();

