(function (angular) {
  'use strict';

  angular
    .module('app.vientos')
    .service('vientosCubiertaService', [
      'transformService',
      'unAguaService',
      'dosAguasService',
      vientosCubiertaService
    ]);

  function vientosCubiertaService(transformService, unAguaService, dosAguasService)
  {
    var vm = this;

    //injects

    //methods
    vm.newInstance = __newCubierta;

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * retorna un objeto Edificacion.
     * Cada caracteristica puede ser un subObjeto debido a que tiene otras caracteristicas dentro (una cubierta a un agua, o dos aguas).
     *
     * @param nameSpace
     * @param largo
     * @param ancho
     * @param alto
     * @param cornisa
     * @public
     */
    function __newCubierta(nameSpace, largo, ancho, alto, cornisa)
    {
      var cubierta = newShape(nameSpace, largo, ancho, alto, cornisa);

      //events
      cubierta.onClick = __onClick;

      //data

      //Initialization UnAgua Object
      cubierta.unAgua = unAguaService.newInstance(nameSpace + "__UnAgua", largo, ancho, alto, cornisa);
      cubierta.unAgua.onClick = agua_onClick;

      //Initialization DosAguas Object
      cubierta.dosAguas = dosAguasService.newInstance(nameSpace + "__DosAguas", largo, ancho, alto, cornisa);
      cubierta.dosAguas.onClick = agua_onClick;

      //methods
      cubierta.setLongitud = __setLongitud;
      cubierta.setAncho = __setAncho;
      cubierta.setAlto = __setAlto;
      cubierta.setCornisa = __setCornisa;
      cubierta.setColor = __setColor;

      return cubierta;

      ////////////////////////////////////////////////////////////
      /**
       * Funcion para establecer la longitud(largo) de la cubierta.
       *
       * @param value --> valor a establecer.
       * @param actionCall --> funcion callback que retorna el valor establecido.  es opcional.
       */
      function __setLongitud(value, actionCall)
      {
        cubierta.dosAguas.setLongitud(value);
        cubierta.unAgua.setLongitud(value);

        if (typeof actionCall !== 'undefined')
          actionCall(cubierta.unAgua.scale.z);
      }

      /**
       * funcion para establecer la longitud del ancho de la cubierta.
       *
       * @param value --> valor a establecer.
       * @param actionCall --> funcion callback que retorna el valor establecido. es opcional.
       */
      function __setAncho(value, actionCall)
      {
        cubierta.dosAguas.setAncho(value);
        cubierta.unAgua.setAncho(value);

        if (typeof actionCall !== 'undefined')
          actionCall(cubierta.unAgua.scale.x);
      }

      /**
       * Funcion para establecer el alto (cumbrera) de la cubierta.
       *
       * @param value --> valor a establecer
       * @param actionCall --> funcion callback que retorna el valor establecido.  es opcional.
       */
      function __setAlto(value, actionCall)
      {
        cubierta.dosAguas.setAlto(value);
        cubierta.unAgua.setAlto(value);

        if (typeof actionCall !== 'undefined')
          actionCall(cubierta.unAgua.scale.y);
      }

      /**
       * funcion para establecer la altura de la cornisa.  ubica la cubierta tan arriba como se establezca la cornisa.
       *
       * @param value --> valor a establecer
       * @param actionCall --> funcion callback que retorna el valor establecido. es opcional
       */
      function __setCornisa(value, actionCall)
      {
        cubierta.dosAguas.setCornisa(value);
        cubierta.unAgua.setCornisa(value);

        if (typeof actionCall !== 'undefined')
          actionCall(cubierta.unAgua.translation.y);
      }

      /**
       * funcion para establecer el color de los diferentes objetos que puedan componer la cubierta.
       *
       * @param color --> el color en string con formato rgb. es decir (r g b)
       * @private
       */
      function __setColor(color)
      {
        var id = cubierta.nameSpace + '__MA_Shape';

        /*
        * cambia el color del elemento
        * */
        document.getElementById(id).setAttribute('diffuseColor', color);
      }


      function agua_onClick(agua)
      {
        cubierta.onClick(agua)
      }

      /**
       * esta funcion puede sobreescribirse. esta funcion es invocada cuando el evento click de alguno de los elementos de la cubierta es disparado.
       *
       * @param agua
       * @public
       */
      function __onClick(agua)
      {
        console.log('Event Click is Done!', agua.nameSpace)
      }

    }

    /**
     * crea una instancia de una nueva forma para x3d con las propiedades necesarias y las sobreescribe.
     *
     * @param nameSpace --> string que identifica de manera unica el objeto x3d dentro del html.
     * @param largo --> establece la longitud del objeto en z.
     * @param alto --> establece la longitud del objeto en y.
     * @param cornisa --> establece la ubicacion del objeto en y.
     */
    function newShape(nameSpace, largo, alto, cornisa)
    {
      var shape = transformService.newInstance();

      //@Override data
      shape.nameSpace = nameSpace;
      shape.scale.y = alto;
      shape.scale.z = largo;
      shape.translation.y = cornisa;
      shape.selected = false;
      shape.tipo = "vientos_cubierta";

      return shape;
    }

  }

})(window.angular);
