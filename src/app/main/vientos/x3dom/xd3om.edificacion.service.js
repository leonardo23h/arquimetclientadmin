(function (angular) {
  'use strict';

  angular
    .module('app.vientos')
    .service('edificacionService', [
      'transformService',
      edificacionService
    ]);

  function edificacionService(transformService)
  {
    var vm = this;

    //injects

    //methods
    vm.newInstance = __newEdificacionService;

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * retorna un objeto Edificacion.
     * Cada caracteristica puede ser un subObjeto debido a que tiene otras caracteristicas dentro.
     */
    function __newEdificacionService(nameSpace, largo, ancho, alto)
    {
      var edificacion = newShape(nameSpace, largo, ancho, alto);

      //data

      //methods
      edificacion.setLongitud = setLongitud;
      edificacion.setAncho = setAncho;
      edificacion.setAlto = setAlto;
      edificacion.setColor = __setColor;
      edificacion.onClick = __onClick;

      setTimeout(__initialize, 1000);

      return edificacion;

      ////////////////////////////////////////////////////////////
      function setLongitud(value, actionCall)
      {
        edificacion.scale.z = edificacion.toScale(value);

        if (typeof actionCall !== 'undefined')
          actionCall(edificacion.toMeters(edificacion.scale.z));
      }

      function setAncho(value, actionCall)
      {
        edificacion.scale.x = edificacion.toScale(value);

        if (typeof actionCall !== 'undefined')
          actionCall(edificacion.toMeters(edificacion.scale.x));
      }

      function setAlto(value, actionCall)
      {
        edificacion.scale.y = edificacion.toScale(value);

        if (typeof actionCall !== 'undefined')
          actionCall(edificacion.toMeters(edificacion.scale.y));
      }

      function __handlerClick()
      {
        edificacion.onClick(edificacion);
      }

      function __setColor(color)
      {
        var id = edificacion.nameSpace + '__MA_Shape';

        /*
        * cambia el color del elemento
        * */
        document.getElementById(id).setAttribute('diffuseColor', color);
      }

      function __onClick(edificacion)
      {
        console.log('Event Click is Done!', edificacion.nameSpace)
      }

      function __initialize()
      {
        angular.element(document.getElementById(edificacion.nameSpace)).on('click', __handlerClick);
      }
    }

    function newShape(nameSpace, largo, ancho, alto)
    {
      var shape = transformService.newInstance();

      //@Override data
      shape.nameSpace = nameSpace;
      shape.scale.x = shape.toScale(ancho);
      shape.scale.y = shape.toScale(alto);
      shape.scale.z = shape.toScale(largo);
      shape.selected = false;
      shape.tipo = "vientos_edificacion";

      return shape;
    }

  }

})(window.angular);
