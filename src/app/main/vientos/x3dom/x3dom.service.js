/**
 * Created by stackpointer on 24/07/17.
 */

/**
 * Created by aesleider on 13/03/2017.
 */
(function (angular) {
  'use strict';

  angular
    .module('app.vientos')
    .factory('vientosX3dom', [
      'x3dService',
      'disenoVientos',
      vientosX3dom
    ]);

  function vientosX3dom(x3dService, disenoVientos)
  {
    var vm = {};

    //data
    vm.x3d = x3dService.newInstance("x3domVientosCentralSceneView");//construye una instancia del objeto x3d con el id especificado.

    vm.x3d.scene.superTransform = disenoVientos;

    //methods


    return vm;
    /////////////////////////////////////////////// Functions  ////////////////////////////////////////////////////


  }

})(window.angular);
