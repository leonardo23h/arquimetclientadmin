(function (angular) {
  'use strict';

  angular
    .module('app.vientos')
    .service('dosAguasService', [
      'transformService',
      dosAguasService
    ]);

  function dosAguasService(transformService)
  {
    var vm = this;

    //injects

    //methods
    vm.newInstance = __newDosAguas;

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * retorna un objeto Metaldeck (Concreto).
     * Cada caracteristica puede ser un subObjeto debido a que tiene otras caracteristicas dentro.
     */
    function __newDosAguas(nameSpace, largo, ancho, alto, cornisa)
    {
      var dosAguas = newShape(nameSpace, largo, ancho, alto, cornisa);

      //data

      //methods
      dosAguas.setLongitud = setLongitud;
      dosAguas.setAncho = setAncho;
      dosAguas.setAlto = setAlto;
      dosAguas.setCornisa = setCornisa;
      dosAguas.setColor = __setColor;
      dosAguas.onClick = __onClick;

      setTimeout(__initialize, 1000);

      return dosAguas;

      ////////////////////////////////////////////////////////////
      function setLongitud(value, actionCall)
      {
        dosAguas.scale.z = dosAguas.toScale(value);

        if (typeof actionCall !== 'undefined')
          actionCall(dosAguas.toMeters(dosAguas.scale.z));
      }

      function setAncho(value, actionCall)
      {
        dosAguas.scale.x = dosAguas.toScale(value);

        if (typeof actionCall !== 'undefined')
          actionCall(dosAguas.toMeters(dosAguas.scale.x));
      }

      function setAlto(value, actionCall)
      {
        dosAguas.scale.y = dosAguas.toScale(value);

        if (typeof actionCall !== 'undefined')
          actionCall(dosAguas.toMeters(dosAguas.scale.y));
      }

      function setCornisa(value, actionCall)
      {
        dosAguas.translation.y = dosAguas.toScale(value);
        retranslation();

        if (typeof actionCall !== 'undefined')
          actionCall(dosAguas.translation.y);
      }

      function retranslation()
      {
        dosAguas.translation.y =  parseFloat(dosAguas.translation.y) + parseFloat(dosAguas.scale.y);
      }

      function __handlerClick()
      {
        dosAguas.onClick(dosAguas);
      }

      function __setColor(color)
      {
        var id = dosAguas.nameSpace + '__MA_Shape';

        /*
        * cambia el color del elemento
        * */
        document.getElementById(id).setAttribute('diffuseColor', color);
      }

      function __onClick(dosAguas)
      {
        console.log('Event Click is Done!', dosAguas.nameSpace)
      }

      function __initialize()
      {
        angular.element(document.getElementById(dosAguas.nameSpace)).on('click', __handlerClick);
      }
    }

    function newShape(nameSpace, largo, ancho, alto, cornisa)
    {
      var shape = transformService.newInstance();

      //@Override data
      shape.nameSpace = nameSpace;
      shape.scale.x = shape.toScale(ancho);
      shape.scale.y = shape.toScale(alto);
      shape.scale.z = shape.toScale(largo);
      shape.translation.y = cornisa;
      shape.selected = false;
      shape.tipo = "cubierta_dos_aguas";

      return shape;
    }

  }

})(window.angular);
