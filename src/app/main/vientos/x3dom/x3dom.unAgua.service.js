(function (angular) {
  'use strict';

  angular
    .module('app.vientos')
    .service('unAguaService', [
      'transformService',
      unAguaService
    ]);

  function unAguaService(transformService)
  {
    var vm = this;

    //injects

    //methods
    vm.newInstance = __newUnAgua;

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * retorna un objeto Metaldeck (Concreto).
     * Cada caracteristica puede ser un subObjeto debido a que tiene otras caracteristicas dentro.
     */
    function __newUnAgua(nameSpace, largo, ancho, alto, cornisa)
    {
      var unAgua = newShape(nameSpace, largo, ancho, alto, cornisa);

      //data

      //methods
      unAgua.setLongitud = setLongitud;
      unAgua.setAncho = setAncho;
      unAgua.setAlto = setAlto;
      unAgua.setCornisa = setCornisa;
      unAgua.setColor = __setColor;
      unAgua.onClick = __onClick;

      setTimeout(__initialize, 1000);

      return unAgua;

      ////////////////////////////////////////////////////////////
      function setLongitud(value, actionCall)
      {
        unAgua.scale.z = unAgua.toScale(value);

        if (typeof actionCall !== 'undefined')
          actionCall(unAgua.toMeters(unAgua.scale.z));
      }

      function setAncho(value, actionCall)
      {
        unAgua.scale.x = unAgua.toScale(value);

        if (typeof actionCall !== 'undefined')
          actionCall(unAgua.toMeters(unAgua.scale.x));
      }

      function setAlto(value, actionCall)
      {
        unAgua.scale.y = unAgua.toScale(value);

        if (typeof actionCall !== 'undefined')
          actionCall(unAgua.toMeters(unAgua.scale.y));
      }

      function setCornisa(value, actionCall)
      {
        unAgua.translation.y = unAgua.toScale(value);
        retranslation();

        if (typeof actionCall !== 'undefined')
          actionCall(unAgua.translation.y);
      }

      function retranslation()
      {
        unAgua.translation.y =  parseFloat(unAgua.translation.y) + parseFloat(unAgua.scale.y);
      }

      function __handlerClick()
      {
        unAgua.onClick(unAgua);
      }

      function __setColor(color)
      {
        var id = unAgua.nameSpace + '__MA_Shape';

        /*
        * cambia el color del elemento
        * */
        document.getElementById(id).setAttribute('diffuseColor', color);
      }

      function __onClick(dosAguas)
      {
        console.log('Event Click is Done!', dosAguas.nameSpace)
      }

      function __initialize()
      {
        angular.element(document.getElementById(unAgua.nameSpace)).on('click', __handlerClick);
      }
    }

    function newShape(nameSpace, largo, ancho, alto, cornisa)
    {
      var shape = transformService.newInstance();

      //@Override data
      shape.nameSpace = nameSpace;
      shape.scale.x = shape.toScale(ancho);
      shape.scale.y = shape.toScale(alto);
      shape.scale.z = shape.toScale(largo);
      shape.translation.y = cornisa;
      shape.selected = false;
      shape.tipo = "cubierta_dos_aguas";

      return shape;
    }

  }

})(window.angular);
