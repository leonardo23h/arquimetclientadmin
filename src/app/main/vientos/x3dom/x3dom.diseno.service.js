/**
 * Created by aesleider on 13/03/2017.
 */
(function (angular) {
  'use strict';

  angular
    .module('app.vientos')
    .factory('disenoVientos', [
      'transformService',
      'edificacionService',
      'vientosCubiertaService',
      disenoMetaldeck
    ]);

  function disenoMetaldeck(transformService, edificacionService, vientosCubiertaService)
  {
    var vm = transformService.newInstance();//construye una instancia del objeto x3d con el id especificado.

    //events
    vm.onInit = __onInit;
    vm.onClick = __onClick;

    //data
    _initialize();

    //methods
    vm.typeOf = __typeOf;
    vm.setLongitud = __setLongitud;
    vm.setAncho = __setAncho;
    vm.setCornisa = __setCornisa;
    vm.setCumbrera = __setCumbrera;


    return vm;

    /////////////////////////////////////////////// Functions  ////////////////////////////////////////////////////
    function _initialize()
    {
      //////////////////// objects instances ////////////////////

      //estructura
      vm.edificacion = {};
      vm.cubierta = {};

      //////////////////// objects init ////////////////////
      vm.edificacion = edificacionService.newInstance("Vientos_Edificacion", 18, 9, 5);
      vm.edificacion.onClick = edificacion_onClick;

      vm.cubierta = vientosCubiertaService.newInstance("Vientos_Cubierta", 18, 9, 5, 5);
      vm.cubierta.onClick = cubierta_onClick;

      //lights

      //execute onInit step.
      vm.onInit();

    }

    function __setLongitud(value, actionCall)
    {
      vm.cubierta.setLongitud(value);
      vm.edificacion.setLongitud(value);

      if (typeof actionCall !== 'undefined')
        actionCall(vm.edificacion.scale.z);
    }

    function __setAncho(value, actionCall)
    {
      vm.cubierta.setAncho(value);
      vm.edificacion.setAncho(value);

      if (typeof actionCall !== 'undefined')
        actionCall(vm.edificacion.scale.x);
    }

    function __setCornisa(value, actionCall)
    {
      vm.cubierta.setCornisa(value);
      vm.edificacion.setAlto(value);

      if (typeof actionCall !== 'undefined')
        actionCall(unAgua.translation.y);
    }

    function __setCumbrera(value, actionCall)
    {
      vm.cubierta.setAlto(value);

      if (typeof actionCall !== 'undefined')
        actionCall(unAgua.translation.y);
    }

    //funcion que se ejecuta luego de hacer click en
    function edificacion_onClick(edificacion)
    {
      vm.onClick(edificacion);
    }

    //funcion que se ejecuta luego de hacer click en
    function cubierta_onClick(edificacion)
    {
      vm.onClick(edificacion);
    }

    //funcion que se ejecuta luego de cambiar el largo de una luz
    function edificacion_onChangeLong(edificacion)
    {

    }

    function __typeOf(shape)
    {
      var type = "";
      shape.nameSpace === vm.edificacion.nameSpace ?
        type = "edificacion" :
        type = "cubierta";

      return type;
    }

    ////////////////////////////// EVENTS //////////////////////////////
    function __onInit()
    {
      console.log('Initializing design!')
    }

    function __onClick(shape)
    {
      console.log("Click on " + __typeOf(shape) + " From Design")
    }

  }

})(window.angular);
