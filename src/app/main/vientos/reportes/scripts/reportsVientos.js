function reportsVientos()
{
	this.dd = new DocumentDefinition();

	// esta funcion esta escrita
	//this.dd.getImagenDinamica = buscarImagenDinamica;

	this.add_json = function(jsonDecoded)
	{
		for(var i in jsonDecoded['reporte']['tecnico'])
		{
			this.add_reporte(jsonDecoded['reporte']['tecnico'][i]);
		}
	};

	this.add_reporte = function(reporte)
	{
		// verificar si el perfil es soportado
		var functionName = 'add_perfil_vientos';
		if( typeof window[functionName] != 'undefined' )
		{
			window[functionName](reporte, this.dd);
		}
		else
		{

		}
	};

	this.output = function()
	{
		return this.dd.output();
	};
}

