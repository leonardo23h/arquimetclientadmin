/**
 * 
 * @param {type} report
 * @param {DocumentDefinition} dd
 * @returns {undefined}
 */
function add_perfil_vientos(report, dd)
{
	/****************************************/
	/*               Titulo                 */
	/****************************************/
	dd.new_page();
	dd.indent = 0;

	for(var ii in report)
	{
		var objstmp = report[ii];
		if(objstmp instanceof Array)
		{
			for(var jj in objstmp)
			{
				dd.addObject(objstmp[jj]);
			}
		}
		else if(objstmp instanceof Object)
		{
			dd.addObject(objstmp);
		}

		
	}


	
	/****************************************/
	/*               Finalizo               */
	/****************************************/
	dd.indent = 0;
	dd.pushStack();
}
