/**
 * Created by stackpointer on 16/08/17.
 */

(function (angular) {
  "use strict";

  angular
    .module('app.propiedades')
    .service('reporteVientosService', [
      'vientosData',
      reporteVientosService
    ]);


  function reporteVientosService(vientosData)
  {

    var vm = this;

    //functions
    vm.getReporte = getReporte;

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    function getReporte()
    {
      buildReporteAcademico();
    }

    function buildReporteAcademico()
    {
      var r = new reportsVientos();
      vientosData.calcular()
        .then(function (calculos) {
          r.add_json(calculos);
          pdfMake.createPdf(r.output()).open();
        });

    }

  }
})(window.angular);
