/**
 * Created by stackpointer on 5/04/17.
 */
(function (angular) {
  'use strict';
  angular
    .module('app.vientos')
    .controller('SceneVientosController', [
      'vientosData',
      'vientosX3dom',
      SceneMetalController
    ]);

  function SceneMetalController(vientosData, vientosX3dom) {

    var vm = this;

    //inject properties
    vm.dataVientos = vientosData;

    vm.x3d = vientosX3dom.x3d;
    vm.orientationBx = vm.x3d.orientationBx;
    vm.axes = vm.x3d.axes;
    vm.scene = vm.x3d.scene;
    vm.superTransform = vm.scene.superTransform;


    //data
    vm.sliderZooming = {
      minValue:1,
      maxValue:100,
      value: 95,
      options: {
        showSelectionBar: true,
        floor:1,
        ceil:100,
        step: 1,
        vertical: true,
        onChange: function () {
          vm.x3d.zoom(vm.sliderZooming.value, 70, 85);
        }
      }

    };

    /*******************************************************************/

  }

})(window.angular);
