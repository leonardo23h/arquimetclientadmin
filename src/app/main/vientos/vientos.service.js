/**
 * Created by stackpointer on 23/06/17.
 */
(function (angular) {
  'use strict';

  angular
    .module('app.vientos')
    .service('vientosService',[
      'api',
      vientosService
    ]);

  function vientosService(api) {
    var vm = this;

    //methods
    vm.calcular = calcular;

    ////////////////////////////////////////////////////////////////////////////////////////////

    function calcular(json) {
      return api.vientos.calcular(json)
        .then(calcularComplete)
        .catch(calcularFailed);

      function calcularComplete(response) {
        return response.data;
      }

      function calcularFailed(err) {
        return err;
      }
    }
  }
})(window.angular);
