/**
 * Created by stackpointer on 23/06/17.
 */
(function (angular) {
  'use strict';
  angular
    .module('app.vientos')
    .controller('VientosController',[
      'activeItem',
      vientosController
    ]);

  function vientosController(activeItem) {

    var vm = this;

    vm.activeItem = activeItem;

    vm.activeItem.setActive("Vientos");
    /*
     * functions
     * */

    /*******************************************************************/

  }

})(window.angular);
