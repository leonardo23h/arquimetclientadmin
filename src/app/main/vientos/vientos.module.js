/**
 * Created by stackpointer on 23/06/17.
 */

(function (angular)
{
  'use strict';

  angular
    .module('app.vientos', [])
    .config(config);

  /** @ngInject */
  function config($stateProvider)
  {

    $stateProvider.state('app.vientos', {
      url    : '/vientos',
      views  : {
        'content@app': {
          templateUrl: 'app/main/vientos/vientos.html',
          controller : 'VientosController as vm'
        },
        'quickationstoolbar@app': {
          templateUrl: 'app/main/vientos/quickationstoolbar/quickationstoolbar.html',
          controller : 'VientosQuickationstoolbarController as vm'
        },
        'design@app.vientos':{
          templateUrl: 'app/main/vientos/tabDesign/tabDesign.html',
          controller : 'TabVientosDesignController as vm'
        },
        'results@app.vientos':{
          templateUrl: 'app/main/vientos/tabResults/tabResults.html',
          controller : 'TabVientosResultsController as vm'
        },
        'scene@app.vientos':{
          templateUrl: 'app/main/vientos/scene/scene.html',
          controller : 'SceneVientosController as vm'
        }
      }
    });

  }

})(window.angular);
