/**
 * Created by stackpointer on 5/04/17.
 */

(function (angular) {
  'use strict';
  angular
    .module('app.vientos')
    .controller('TabVientosDesignController',[
      'vientosData',
      '$uibModal',
      'reporteVientosService',
      TabVientosDesignController
    ]);

  function TabVientosDesignController(vientosData, $uibModal, reporteVientosService) {

    var vm = this;
    vm.vientosData = vientosData;
    vm.propiedades = vm.vientosData.data.propiedades;

    //inject properties

    vm.hrefDownload = "data:application/octet-stream," + encodeURIComponent();

    /*
     * functions
     * */
    vm.calcular = calcular;
    vm.downloadPDF = downloadPDF;
    vm.openMapaVelocidad = openMapaVelocidad;

    /*******************************************************************/
    function calcular()
    {
      vm.vientosData.calcular();
    }

    function openMapaVelocidad() {

      var parentElem = angular.element(document.body);
      var modalInstance = $uibModal.open({
        animation: true,
        templateUrl: 'app/main/vientos/modals/mapa-velocidad/mapa-velocidad.html',
        controller: 'MapaVelocidadModalInstanceController',
        controllerAs: 'vm',
        size: 'md',
        appendTo: parentElem
      });

    }

    function downloadPDF()
    {
      reporteVientosService.getReporte();
    }


  }

})(window.angular);
