/**
 * Created by aesleider on 31/03/2017.
 */
(function (angular) {
  'use strict';

  angular
    .module('app.metaldeck')
    .controller('CargaDistMetalModalInstanceController', CargaDistMetalModalInstanceController);

  function CargaDistMetalModalInstanceController($uibModalInstance, unidadesService) {
    var vm = this;

    vm.carga = {};
    vm.carga.cargaType = 'muerta';
    vm.carga.cargaExistente = false;
    vm.carga.remove = false;
    vm.carga.valor = 0;


    vm.unidades = unidadesService;
    vm.carga.unidades = angular.copy(vm.unidades.list[7]);
    vm.ok = function () {
      $uibModalInstance.close(vm.carga);
    };

    vm.cancel = function () {
      $uibModalInstance.dismiss('cancel');
    };
  }
})(window.angular);
