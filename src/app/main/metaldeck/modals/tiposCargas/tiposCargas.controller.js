(function (angular) {
  'use strict';

  angular
    .module('app.metaldeck')
    .controller('TiposCargasInstanceController', [
      '$uibModalInstance',
      'cargasViewer',
      TiposCargasInstanceController
    ])

  function TiposCargasInstanceController($uibModalInstance, cargasViewer)
  {
    var vm = this;

    //data
    vm.cargasViewer = cargasViewer;
    vm.cargasViewer.removerCargas = false;

    //methods
    vm.ok = ok;
    vm.cancel = cancel;
    vm.chngShowVivasDistribuidas = chngShowVivasDistribuidas;
    vm.chngShowVivasPuntuales = chngShowVivasPuntuales;
    vm.chngShowMuertasDistribuidas = chngShowMuertasDistribuidas;
    vm.chngShowMuertasPuntuales = chngShowMuertasPuntuales;
    vm.chngRemoverCargas = chngRemoverCargas;

    ////////////////////////////////////////////////////////////////////////////
    function ok()
    {
      $uibModalInstance.close(vm.cargasViewer);
    }

    function cancel()
    {
      $uibModalInstance.dismiss();
    }

    function chngShowVivasDistribuidas()
    {
      if (vm.cargasViewer.showVivasDistribuidas === true && vm.cargasViewer.showMuertasDistribuidas === true && vm.cargasViewer.removerCargas === false)
      {
        vm.cargasViewer.showMuertasDistribuidas = false
      }else if(vm.cargasViewer.removerCargas === true){

        setFalseShowCargas();
      }
    }

    function chngShowVivasPuntuales()
    {
      if (vm.cargasViewer.showVivasPuntuales === true && vm.cargasViewer.showMuertasPuntuales === true && vm.cargasViewer.removerCargas === false)
      {
        vm.cargasViewer.showMuertasPuntuales = false
      }else if(vm.cargasViewer.removerCargas === true){

        setFalseShowCargas();
      }
    }

    function chngShowMuertasDistribuidas()
    {
      if (vm.cargasViewer.showVivasDistribuidas === true && vm.cargasViewer.showMuertasDistribuidas === true && vm.cargasViewer.removerCargas === false)
      {
        vm.cargasViewer.showVivasDistribuidas = false
      }else if(vm.cargasViewer.removerCargas === true){

        setFalseShowCargas();
      }
    }

    function chngShowMuertasPuntuales()
    {
      if (vm.cargasViewer.showVivasPuntuales === true && vm.cargasViewer.showMuertasPuntuales === true && vm.cargasViewer.removerCargas === false)
      {
        vm.cargasViewer.showVivasPuntuales = false
      }else if(vm.cargasViewer.removerCargas === true){

        setFalseShowCargas();
      }
    }

    function chngRemoverCargas()
    {
      if (vm.cargasViewer.removerCargas === true)
      {
        setFalseShowCargas();
      }
    }

    function setFalseShowCargas()
    {
      vm.cargasViewer.showMuertasDistribuidas = false;
      vm.cargasViewer.showMuertasPuntuales = false;
      vm.cargasViewer.showVivasDistribuidas = false;
      vm.cargasViewer.showVivasPuntuales = false;
    }

  }

})(window.angular);
