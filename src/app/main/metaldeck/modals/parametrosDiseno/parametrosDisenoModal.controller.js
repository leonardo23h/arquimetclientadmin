/**
 * Created by stackpointer on 4/04/17.
 */
(function (angular) {
  'use strict';

  angular
    .module('app.metaldeck')
    .controller('ParamsDisenoModalInstanceController', ParamsDisenoModalInstanceController);

  function ParamsDisenoModalInstanceController($uibModalInstance, dataMetaldeck)
  {
    var vm = this;

    //injects
    vm.dataMetaldeck = {};

    vm.dataMetaldeck.soloFormaleta = dataMetaldeck.data.estructura.soloFormaleta && dataMetaldeck.data.estructura.diseno === 1 && dataMetaldeck.data.estructura.momentosNegativos === false;
    vm.dataMetaldeck.momentosNegativos = dataMetaldeck.data.estructura.momentosNegativos === true || dataMetaldeck.data.estructura.diseno === 2;
    vm.dataMetaldeck.fy = dataMetaldeck.data.estructura.fy;
    vm.dataMetaldeck.diseno = dataMetaldeck.data.estructura.diseno;
    vm.dataMetaldeck.malla = dataMetaldeck.data.estructura.malla;
    vm.dataMetaldeck.defInstConstLong = dataMetaldeck.data.estructura.defInstConstLong;
    vm.dataMetaldeck.cargaVivaDistConst = dataMetaldeck.data.estructura.cargaVivaDistConst;
    vm.dataMetaldeck.defInstConstCarg = dataMetaldeck.data.estructura.defInstConstCarg;
    vm.dataMetaldeck.defSistLosaComp = dataMetaldeck.data.estructura.defSistLosaComp;
    vm.dataMetaldeck.parametroFc = dataMetaldeck.data.estructura.parametroFc;
    vm.dataMetaldeck.firstSupport = dataMetaldeck.data.estructura.apoyos[0].voladizo;
    vm.dataMetaldeck.lastSupport = dataMetaldeck.data.estructura.apoyos[dataMetaldeck.data.estructura.apoyos.length - 1].voladizo;


    //data

    //methods
    vm.ok = ok;
    vm.cancel = cancel;
    vm.chngSoloFormaleta = chngSoloFormaleta;
    vm.chngMomentosNegativos = chngMomentosNegativos;
    vm.chngFc = chngFc;
    vm.chngFirstSupport = chngfirstSupport;
    vm.chngLastSupport = chngLastSupport;

    //////////////////////////////////////////////////////////////////////////////////////////
    function ok()
    {
      dataMetaldeck.data.estructura.soloFormaleta = vm.dataMetaldeck.soloFormaleta;
      dataMetaldeck.data.estructura.momentosNegativos = vm.dataMetaldeck.momentosNegativos;
      dataMetaldeck.data.estructura.fy = vm.dataMetaldeck.fy;
      dataMetaldeck.data.estructura.diseno = vm.dataMetaldeck.diseno;
      dataMetaldeck.data.estructura.malla = vm.dataMetaldeck.malla;
      dataMetaldeck.data.estructura.defInstConstLong = vm.dataMetaldeck.defInstConstLong;
      dataMetaldeck.data.estructura.cargaVivaDistConst = vm.dataMetaldeck.cargaVivaDistConst;
      dataMetaldeck.data.estructura.defInstConstCarg = vm.dataMetaldeck.defInstConstCarg;
      dataMetaldeck.data.estructura.defSistLosaComp = vm.dataMetaldeck.defSistLosaComp;
      dataMetaldeck.data.estructura.parametroFc = vm.dataMetaldeck.parametroFc;
      dataMetaldeck.setVoladizos(vm.dataMetaldeck.firstSupport, vm.dataMetaldeck.lastSupport);

      $uibModalInstance.close();
    }

    function cancel()
    {
      $uibModalInstance.dismiss('cancel');
    }

    function chngSoloFormaleta()
    {
      if (!hasVoladizos()) {
        if (vm.dataMetaldeck.soloFormaleta === true && vm.dataMetaldeck.momentosNegativos === true) {
          vm.dataMetaldeck.momentosNegativos = false;
        }
      } else {
        vm.dataMetaldeck.soloFormaleta = false;
      }
      setDiseno();
    }

    function chngMomentosNegativos()
    {
      if (!hasVoladizos()) {
        if (vm.dataMetaldeck.soloFormaleta === true && vm.dataMetaldeck.momentosNegativos === true) {
          vm.dataMetaldeck.soloFormaleta = false;
        }
      } else {
        vm.dataMetaldeck.momentosNegativos = true;
      }

      setDiseno();

    }

    function chngFc()
    {
      if (vm.dataMetaldeck.parametroFc < 2109208.65) {
        vm.dataMetaldeck.parametroFc = 2109208.65;
      }

      if (vm.dataMetaldeck.parametroFc > 4218417.30) {
        vm.dataMetaldeck.parametroFc = 4218417.30;
      }
    }

    function chngfirstSupport()
    {
      switch (dataMetaldeck.data.estructura.luces.length) {
        case 1:
          if (vm.dataMetaldeck.firstSupport === true) {
            vm.dataMetaldeck.firstSupport = false;
          }
          break;

        case 2:
          if (vm.dataMetaldeck.firstSupport === true && vm.dataMetaldeck.lastSupport === true) {
            vm.dataMetaldeck.lastSupport = false;
          }
          vm.dataMetaldeck.soloFormaleta = false;
          vm.dataMetaldeck.momentosNegativos = true;
          break;

        default:
          vm.dataMetaldeck.soloFormaleta = false;
          vm.dataMetaldeck.momentosNegativos = true;
          break;

      }
      setDiseno();
    }

    function chngLastSupport()
    {
      switch (dataMetaldeck.data.estructura.luces.length) {
        case 1:
          if (vm.dataMetaldeck.lastSupport === true) {
            vm.dataMetaldeck.lastSupport = false;
          }
          break;

        case 2:
          if (vm.dataMetaldeck.firstSupport === true && vm.dataMetaldeck.lastSupport === true) {
            vm.dataMetaldeck.firstSupport = false;
          }
          vm.dataMetaldeck.soloFormaleta = false;
          vm.dataMetaldeck.momentosNegativos = true;
          break;

        default:
          vm.dataMetaldeck.soloFormaleta = false;
          vm.dataMetaldeck.momentosNegativos = true;
          break;

      }

      setDiseno();

    }

    function hasVoladizos()
    {
      var hasVoladizo = false;

      if (vm.dataMetaldeck.lastSupport === true || vm.dataMetaldeck.firstSupport === true) {
        hasVoladizo = true;
      }

      return hasVoladizo;
    }

    function setDiseno()
    {
      switch (dataMetaldeck.data.estructura.diseno){
        case 1:
          if (vm.dataMetaldeck.momentosNegativos)
            vm.dataMetaldeck.diseno = 2 ;
          else
            vm.dataMetaldeck.diseno = 1;
          break;
        case 2:
          if (!vm.dataMetaldeck.momentosNegativos)
            vm.dataMetaldeck.diseno = 2 ;
          else
            vm.dataMetaldeck.diseno = 2;
          break;
      }

    }

  }
})(window.angular);
