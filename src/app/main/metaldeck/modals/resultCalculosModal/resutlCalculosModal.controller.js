/**
 * Created by stackpointer on 11/04/17.
 */
(function (angular) {
  'use strict';

  angular
    .module('app.metaldeck')
    .controller('resultCalculosModalInstanceController', resultCalculosModalInstanceController);

  function resultCalculosModalInstanceController(report) {
    var vm = this;
    vm.report = report;

    vm.metaldeckStyle = {
      "width": "80%",
      "height": "30px"
    };

    vm.topLineStyle = {
      "width": "98.5%",
      "height": "10px",
      "background-color": "gray"
    };

    vm.rellenoStyle = {
      "width": "5px",
      "height": "10px",
      "background-color": "red"
    };

  }
})(window.angular);
