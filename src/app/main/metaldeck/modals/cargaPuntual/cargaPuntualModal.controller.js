/**
 * Created by stackpointer on 20/04/17.
 */
(function (angular) {
  'use strict';

  angular
    .module('app.metaldeck')
    .controller('CargaPuntMetalModalInstanceController', CargaPuntMetalModalInstanceController);

  function CargaPuntMetalModalInstanceController($uibModalInstance, unidadesService) {
    var vm = this;
    vm.carga = {};
    vm.carga.cargaType = 'muerta';
    vm.carga.cargaExistente = false;
    vm.carga.remove = false;
    vm.carga.valor = 0;
    vm.carga.distanciaX = 1;
    vm.carga.anchoB = 0.1;

    vm.unidades = unidadesService;
    vm.carga.unidades = angular.copy(vm.unidades.list[7]);
    vm.ok = function () {
      $uibModalInstance.close(vm.carga);
    };

    vm.cancel = function () {
      $uibModalInstance.dismiss('cancel');
    };
  }
})(window.angular);
