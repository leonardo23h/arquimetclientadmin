(function (angular) {
  'use strict';

  angular
    .module('app.metaldeck')
    .controller('configReporteMetalInstanceController', [
      '$uibModalInstance',
      configReporteInstanceController
    ]);

  function configReporteInstanceController($uibModalInstance)
  {
    var vm = this;

    //data
    vm.isTecnico = true;

    //methods
    vm.ok = ok;
    vm.cancel = cancel;

    ////////////////////////////////////////////////////////////////////////////
    function ok(isTecnico)
    {
      $uibModalInstance.close(isTecnico);
    }

    function cancel()
    {
      $uibModalInstance.dismiss();
    }

  }

})(window.angular);
