/**
 * Created by stackpointer on 5/04/17.
 */
(function (angular) {
  'use strict';
  angular
    .module('app.metaldeck')
    .controller('TabMetalResultsController',[
      'unidadesService',
      'acabadosService',
      'dataMetaldeckService',
      'checkList',
      TabMetalResultsController
    ]);

  function TabMetalResultsController(unidadesService, acabadosService, dataMetaldeckService, checkList) {
    var vm = this;

    //data
    vm.unidades = unidadesService;
    vm.acabados = acabadosService;
    vm.dataMetaldeck = dataMetaldeckService;
    vm.acabadosCollapsed = [];
    vm.checkAll = false;

    // functions
    vm.isCollapsed = isCollapsedGroup;
    vm.isCheckAll = checkall;
    vm.isCheckallByGrupo = isCheckallByGrupo;
    vm.atLeasOne = checkList.atLeasOne;
    vm.exists = checkList.exists;
    vm.content = content;
    vm.toggleSelectMetaldeck = toggleSelectMetaldeck;


    //////////////////////////////////////////////////////////////////////

    function isCollapsedGroup(key)
    {

      vm.isCollapsed[key] = ! vm.isCollapsed[key];

      Object.keys(vm.isCollapsed).forEach(function (t) {
        if(t !== key){
          vm.isCollapsed[t] = true;
        }
      });
    }

    function checkall(typeAcabado) {
      if (typeAcabado.isCheckAll) {
        checkList.selectPerfiles(vm.dataMetaldeck.data.metaldecks, typeAcabado.metaldecks);
      } else {
        checkList.deselectPerfiles(vm.dataMetaldeck.data.metaldecks, typeAcabado.metaldecks, vm.acabados.restartLaminaDefault);
      }
    }

    function isCheckallByGrupo(ev, grupo)
    {
      if (ev.target.checked === true) {
        checkList.selectPerfiles(vm.dataMetaldeck.data.metaldecks, grupo);
      } else {
        checkList.deselectPerfiles(vm.dataMetaldeck.data.metaldecks, grupo, vm.acabados.restartLaminaDefault);
      }
    }

    function content(list, items) {
      var length = items.length,
        count = 0,
        check = false;

      for(var i = 0; i < length; i++)
      {
        if ( list.indexOf(items[i]) > -1 )
        {
          count++;
        }
      }

      if(count === length)check = true;

      return check;
    }

    function toggleSelectMetaldeck(item, event) {

      var index = vm.dataMetaldeck.data.metaldecks.indexOf(item);

      if(event.target.checked === true && ! index > -1) //index > -1 means exist
      {
        vm.dataMetaldeck.data.metaldecks.push(item);
      }
      else if (event.target.checked === false && index > -1 )
      {
        vm.dataMetaldeck.data.metaldecks.splice(vm.dataMetaldeck.data.metaldecks.indexOf(item), 1);
      }

      if (vm.dataMetaldeck.data.metaldecks.length == 0){
        vm.acabados.restartLaminaDefault();
      }
    }

  }

})(window.angular);
