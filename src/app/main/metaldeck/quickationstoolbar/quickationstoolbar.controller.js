/**
 * Created by stackpointer on 16/05/17.
 */

(function() {
  'use strict';

  angular
    .module('app.metaldeck')
    .controller('MetaldeckQuickationstoolbarController', [
      'unidadesService',
      'acabadosService',
      'dataMetaldeckService',
      'reporteMetaldeckService',
      '$uibModal',
      MetaldeckQuickationstoolbarController
    ]);

  /** @ngInject */
  function MetaldeckQuickationstoolbarController(unidadesService, acabadosService, dataMetaldeckService, reporteMetaldeckService, $uibModal) {
    var vm = this;

    //injects
    vm.unidades = unidadesService;
    vm.acabados = acabadosService;
    vm.dataMetaldeck = dataMetaldeckService;

    //methods
    vm.chngUnidades = chngUnidades;
    vm.calcular = calcular;
    vm.downloadPdf = openConfigReporte;

    (function () {
      vm.unidades.reqUnidades()
        .then(function (unidades) {
          vm.acabados.getAcabados(unidades.unitSelected.codigo);
        });
    })();

    function chngUnidades(idUnidad)
    {
      getAcabados(idUnidad)
    }

    function openConfigReporte()
    {
      var parentElem = angular.element(document.body);
      var modalInstance = $uibModal.open({
        animation: true,
        templateUrl: 'app/main/metaldeck/modals/configReporte/configReporte.html',
        controller: 'configReporteMetalInstanceController',
        controllerAs: 'vm',
        size: 'md',
        appendTo: parentElem,
        resolve: {
        }
      });

      modalInstance.result.then(function (isTecnico) {
        //luego de obtener configuraciones para reporte.
        reporteMetaldeckService.getReporte(isTecnico);

      }, function () {
        //luego de cancelar generar reporte.
      });

    }

    function getAcabados(idUnidad)
    {
      return vm.acabados.getAcabados(idUnidad)
        .then(function (response) {
          return response;
        })
    }

    function calcular() {
      vm.dataMetaldeck.calcular();
    }

  }
})();

