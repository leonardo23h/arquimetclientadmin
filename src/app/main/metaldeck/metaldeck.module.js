/**
 * Created by aesleider on 30/03/2017.
 */

(function (angular)
{
  'use strict';

  angular
    .module('app.metaldeck', ['toggle-switch'])
    .config(config);

  /** @ngInject */
  function config($stateProvider)
  {

    $stateProvider.state('app.metaldeck', {
      url    : '/metaldeck',
      views  : {
        'content@app': {
          templateUrl: 'app/main/metaldeck/metaldeck.html',
          controller : 'MetaldeckController as vm'
        },
        'quickationstoolbar@app': {
          templateUrl: 'app/main/metaldeck/quickationstoolbar/quickationstoolbar.html',
          controller : 'MetaldeckQuickationstoolbarController as vm'
        },
        'design@app.metaldeck':{
          templateUrl: 'app/main/metaldeck/tabDesign/tabDesign.html',
          controller : 'TabMetalDesignController as vm'
        },
        'results@app.metaldeck':{
          templateUrl: 'app/main/metaldeck/tabResults/tabResults.html',
          controller : 'TabMetalResultsController as vm'
        },
        'scene@app.metaldeck':{
          templateUrl: 'app/main/metaldeck/scene/scene.html',
          controller : 'SceneMetalController as vm'
        }
      }
    });

  }

})(window.angular);
