function reportsMetaldeck()
{
	this.dd = new DocumentDefinition();
  this.dd.getImagenDinamica = buscarImagenDinamica;

	// esta funcion esta escrita
	//this.dd.getImagenDinamica = buscarImagenDinamica;

	this.add_json = function(jsonDecoded)
	{
		// anadir todos los reportes recibidos
		for(var i in jsonDecoded['reporte'])
		{
			this.add_reporte(jsonDecoded['reporte'][i]);
		}
	};

	this.add_reporte = function(reporte)
	{
		// verificar si el perfil es soportado
		var functionName = 'add_perfil_metaldeck_academico';
		if( typeof window[functionName] != 'undefined' )
		{
			window[functionName](reporte, this.dd);
		}
		else
		{

		}
	};

	this.output = function()
	{
		return this.dd.output();
	};
}

