/**
 * 
 * @param {type} report
 * @param {DocumentDefinition} dd
 * @returns {undefined}
 */
function add_perfil_metaldeck_academico(report, dd)
{
	/****************************************/
	/*               Titulo                 */
	/****************************************/
	dd.new_page();
	dd.indent = 0;
	dd.titulo_principal("REPORTE ACADÉMICO PARA METALDECK\n {0} \nDebe apuntalarse durante el fraguado\nREGLAMENTO NSR-10 / ANSI-SDI C-2011", report['metaldeck']['descripcion']);


	dd.titulo_h2('DATOS DE ENTRADA');
	dd.titulo_h3('SECCIÓN LONGITUDINAL');
	
	dd.add_imagen_dinamica('imagenDinamica1');

	var columnas_geometrias = {'luz':'Vano','longitud':'Longitud','cargaViva':'Carga viva','cargaMuerta':'Carga muerta\nsobreimpuesta','pesoConcreto':'Peso del concreto','pesoLamina':'Peso de la lámina'};
	var formatos_geometrias = {};
	dd.add_tabla_datos('GEOMETRÍA Y CARGAS DISTRIBUIDAS', columnas_geometrias, report['geometrias'], formatos_geometrias, true);

	var columnas_apoyos = {key:'Titulo','valor':'Valor'};
	var formatos_apoyos = {valor:'{valor} ({magnitud})'};
	dd.add_tabla_datos('APOYOS', columnas_apoyos, report['apoyos'], formatos_apoyos, false);

	var columnas_parametros = {key:'Titulo','valor':'Valor'};
	var formatos_parametros = {key:'{key} ({magnitud})'};
	var adiciona_parametros = {key:{alignment:'left'}};
	dd.add_tabla_datos('PARÁMETROS DE DISEÑO', columnas_parametros, report['parametros'], formatos_parametros, false, adiciona_parametros);

	var variables = report['listaVariables'];


	/****************************************/
	/*    DISEÑO EN ETAPA CONSTRUCTIVA      */
	/****************************************/
	dd.titulo_h1_left('DISEÑO EN ETAPA CONSTRUCTIVA');
	dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'ImagenDisenoEtapaConstructiva'), null, null, null, null, 0.95);
	dd.add_content_margin('Nomenclatura:');
	dd.add_variables(dd.buscar_keys(variables, 'key', ['b','dd','e','y','R','hw','Awebs','Abf','S','As','I','Iep','Ien','Seps','Sepi','Sens','Seni','WL','OvVn']));
	dd.new_line();
	dd.add_content('En esta etapa el concreto no ha fraguado y la lámina de Metaldeck debe ser capaz de soportar su peso propio, el peso del concreto y las cargas de construcción. Si la lámina de Metaldeck no cumple  con  alguno  de  los  estados  límite  de  resistencia  y  de  servicio  (deflexión)  es  necesario apuntalar.')
	
	/****************************************/
	/*  ANÁLISIS DE LA LÁMINA DE METALDECK  */
	/****************************************/
	dd.titulo_h1_left('ANÁLISIS DE LA LÁMINA DE METALDECK');
	dd.add_content('La rigidez “EI” utilizada en el Análisis Estructural es el producto del módulo de elasticidad de la lámina y el momento de inercia efectivo.');
	dd.add_variables(dd.buscar_keys(variables, 'key', ['E','Ie']));
	
	/*********************************************************/
	/*  Combinaciones de carga para revisión de resistencia  */
	/*********************************************************/
	dd.titulo_h1_left('Combinaciones de carga para revisión de resistencia');
	dd.add_content('La rigidez “EI” utilizada en el Análisis Estructural es el producto del módulo de elasticidad de la lámina y el momento de inercia efectivo.');
	dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Formula_Combo1'));
	dd.add_content_margin("donde:");
	dd.add_variables(dd.buscar_keys(variables, 'key', ['WC1']));
	dd.indent = 1;
	dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Formula_Wc1'));
	dd.add_content_margin("donde:");
	dd.add_variables(dd.buscar_keys(variables, 'key', ['WC','WCmin']));
	dd.indent = 0;
	dd.new_line();
	dd.add_variable(dd.buscar_key(variables, 'key', 'WL'));
	dd.add_content('L: Carga de trabajo por personal y equipos de construcción. Tomada como la más severa entre:');
	dd.indent = 1;
	dd.new_line();
	dd.add_variables(dd.buscar_keys(variables, 'key', ['CDm','CPm']));
	dd.indent = 0;
	
	/*******************************************************/
	/* Combinaciones de carga para revisión de deflexiones */
	/*******************************************************/
	dd.titulo_h1_left('Combinaciones de carga para revisión de deflexiones');
	dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Formula_ComboServicio'));
	dd.add_content_margin("donde:");
	dd.add_variables(dd.buscar_keys(variables, 'key', ['WC1']));
	dd.indent = 1;
	dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Formula_Wc1'));
	dd.add_content_margin("donde:");
	dd.add_variables(dd.buscar_keys(variables, 'key', ['WC','WCmin']));
	dd.indent = 0;
	dd.new_line();
	dd.add_variable(dd.buscar_key(variables, 'key', 'WL'));
	dd.new_line();
	dd.add_content('En el cálculo de las deflexiones no se toman en cuenta las cargas de construcción debido a su naturaleza temporal.');
	
		/*********************************/
	/* DISEÑO DE LA LÁMINA METALDECK */
	/*********************************/
	dd.titulo_h1_left('DISEÑO DE LA LÁMINA METALDECK');
	dd.titulo_h1_left('Revisión por estado límite de resistencia');
	dd.add_content('Para el estado límite de resistencia se tiene en cuenta el efecto combinado de momento y cortante indicado en el numeral F.4.3.3.3 del reglamento NSR-10 y C3.3 del AISI S100-12.');
	dd.add_variables(dd.buscar_keys(variables, 'key', ['PosicionEstadoLimiteResistencia','ApoyoEstadoLimiteServicio','ComboEstadoLimiteServicio']));
	dd.add_content_margin("El diseño debe cumplir:");
	dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Formula_CumplimientoEstadoLimiteResistencia'), '', -1, null, dd.buscar_key(variables, 'key', 'ImagenCumplimientoEstadoLimiteResistencia'));
	dd.add_content_margin("donde:");
	dd.add_variables(dd.buscar_keys(variables, 'key', ['Mu','Vu','Mn']));
	dd.indent = 1;
	dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Formula_Mn'));
	dd.add_content_margin("donde:");
	dd.add_variables(dd.buscar_keys(variables, 'key', ['Se','Fy']));
	dd.indent = 0;
	dd.new_line();
	dd.add_variables(dd.buscar_keys(variables, 'key', ['Ob','ovVn']));

	dd.titulo_h1_left('Revisión por estado límite de servicio');
	dd.add_variables(dd.buscar_keys(variables, 'key', ['PosicionEstadoLimiteResistencia','ApoyoEstadoLimiteServicio','ComboEstadoLimiteServicio']));
	dd.add_content_margin("El diseño debe cumplir:");
	dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Formula_CumplimientoEstadoLimiteServicio'), '', -1, null, dd.buscar_key(variables, 'key', 'ImagenCumplimientoEstadoLimiteServicio'));
	dd.add_content_margin("donde:");
	dd.add_variables(dd.buscar_keys(variables, 'key', ['A','Amax']));
	dd.indent = 1;
	dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Formula_Amax'));
	dd.add_content_margin("donde:");
	dd.add_variables(dd.buscar_keys(variables, 'key', ['L','A','B']));
	dd.indent = 0;

	dd.titulo_h1_left('SISTEMA DE APUNTALAMIENTO');
	dd.add_content('Es necesario apuntalar en etapa constructiva. El sistema de apuntalamiento se muestra a continuación:');
	dd.add_imagen_dinamica('imagenDinamica2', 1.15);

	
	dd.titulo_h1_left('DISEÑO EN ETAPA DE SERVICIO');
	dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'ImagenDisenoEtapaServicio'));
	dd.add_content('Nomenclatura:');
	dd.add_variables(dd.buscar_keys(variables, 'key', ['yc','Ic','Sc','yu','Iu','Iprom','Wt']));
	dd.new_line();
	dd.add_content('En esta etapa el concreto ya ha fraguado y el sistema lámina Metaldeck-concreto trabaja como unidad compuesta.')

	dd.titulo_h1_left('ANÁLISIS ESTRUCTURAL');
	dd.add_content_margin('La rigidez “EI” utilizada en el Análisis Estructural es el producto del módulo de elasticidad de la lámina y el momento de inercia promedio entre la sección agrietada y no agrietada.');
	dd.add_variables(dd.buscar_keys(variables, 'key', ['E','Iprom']));

	dd.titulo_h1_left('Cargas muertas sobreimpuestas');
	dd.add_imagen_dinamica('cargaMuerta');
	dd.titulo_h1('Carga muerta');
	dd.add_content('La carga puntual muerta debe ser distribuida sobre un ancho efectivo tal como se calcula a continuación.');
	
	dd.titulo_h1_left('Ancho de distribución efectivo para flexión y deflexión');
	dd.add_content('Esta carga puntual es utilizada para calcular el diagrama de momento y la deflexión del sistema.');
	dd.add_italics('No hay cargas muertas sobreimpuestas asignadas.');

	dd.titulo_h1_left('Ancho de distribución efectivo para cortante');
	dd.add_content('Esta carga puntual es utilizada para calcular el diagrama de cortante del sistema.');
	dd.add_italics('No hay cargas muertas sobreimpuestas asignadas.');

	dd.titulo_h1_left('Cargas vivas');
	dd.add_imagen_dinamica('cargaViva');
	dd.titulo_h1('Carga viva');
	dd.add_content('La carga puntual viva debe ser distribuida sobre un ancho efectivo tal como se calcula a continuación.')

	dd.titulo_h1_left('Ancho de distribución efectivo para flexión y deflexión');
	dd.add_content('Esta carga puntual es utilizada para calcular el diagrama de momento y la deflexión del sistema.');
	dd.add_italics('No hay cargas muertas sobreimpuestas asignadas.');

	dd.titulo_h1_left('Ancho de distribución efectivo para cortante');
	dd.add_content('Esta carga puntual es utilizada para calcular el diagrama de cortante del sistema.');
	dd.add_italics('No hay cargas muertas sobreimpuestas asignadas.');

	dd.titulo_h1_left('Solicitaciones de Diseño');
	dd.add_variables(dd.buscar_keys(variables, 'key', ['PosicionEstadoLimiteResistencia','ApoypEstadoLimiteResistencia']));

	dd.add_imagen_dinamica('diagramaCortante');
	dd.titulo_h1('Diagramas de cortante');
	dd.add_variables(dd.buscar_keys(variables, 'key', ['PosicionEstadoLimiteResistencia','ApoypEstadoLimiteResistencia']));
	
	dd.add_imagen_dinamica('diagramaMomentoPositivo');
	dd.titulo_h1('Diagrama de Momento Positivo');
	dd.add_variables(dd.buscar_keys(variables, 'key', ['PosicionEstadoLimiteResistencia','ApoypEstadoLimiteResistencia']));
	
	dd.add_imagen_dinamica('diagramaMomentoNegativo');
	dd.titulo_h1('Diagrama de Momento Negativo');
	dd.add_variables(dd.buscar_keys(variables, 'key', ['PosicionEstadoLimiteResistencia','ApoypEstadoLimiteResistencia']));
	
	dd.add_imagen_dinamica('deflexionSistema');
	dd.titulo_h1('Deflexión del sistema');
	dd.titulo_h1_left('DISEÑO ESTRUCTURAL');
	dd.titulo_h1_left('Revisión a momento positivo');
	dd.add_content('Diseño por Esfuerzos Admisibles (DEA)');
	dd.add_content_margin("El diseño debe cumplir:");
	dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Formula_CumplimientoMomentoPositivo'), '', -1, null, dd.buscar_key(variables, 'key', 'ImagenCumplimientoMomentoPositivo'));
	dd.add_content_margin("donde:");
	dd.add_variables(dd.buscar_keys(variables, 'key', ['Mact','Madm']));
	dd.indent = 1;
	dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Formula_Madm'));
	dd.add_content_margin("donde:");
	dd.add_variables(dd.buscar_keys(variables, 'key', ['C','Fy','Sc']));
	dd.indent = 0;
	
	dd.titulo_h1_left('Refuerzo por retracción y temperatura');
	dd.add_content('El refuerzo por retracción y temperatura deberá consistir de una malla electro-soldada o barras de refuerzo, con un área mínima calculada como se muestra a continuación:');
	dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Formula_As'));
	dd.add_content_margin("donde:");
	dd.add_variables(dd.buscar_keys(variables, 'key', ['Ac','Asmin']));
	var columnas_malla = {'diametro':'Diametro','espaciamiento':'Espaciamiento'};
	dd.add_tabla_datos("MALLA DE REFUERZO POR TEMPERATURA", columnas_malla, report['mallaRefuerzoTemp'])


	dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'ImagenMallaRefuerzo'), null, null, null, null, 0.8);
	dd.titulo_h1('Esquema de la malla de refuerzo');
	dd.titulo_h1_left('Revisión a momento negativo');
	dd.add_content_margin("El diseño debe cumplir:");
	dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Formula_CumplimientoMomentoNegativo'), '', -1, null, dd.buscar_key(variables, 'key', 'ImagenCumplimientoMomentoNegativo'));
	dd.add_content_margin("donde:");
	dd.add_variables(dd.buscar_keys(variables, 'key', ['Mu','Mn']));
	dd.indent = 1;
	dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Formula_RevisionMn'));
	dd.add_variable(dd.buscar_key(variables, 'key', 'T'));
	dd.indent = 2;
	dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Formula_T'));
	dd.add_content_margin("donde:");
	dd.add_variables(dd.buscar_keys(variables, 'key', ['Fy','As']));
	dd.indent = 1;
	dd.new_line();
	dd.add_variable(dd.buscar_key(variables, 'key', 'd'));
	dd.indent = 2;
	dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Formula_d'));
	dd.add_content_margin("donde:");
	dd.add_variables(dd.buscar_keys(variables, 'key', ['h','rec','y']));
	dd.indent = 0;
	dd.new_line();
	dd.add_variable(dd.buscar_key(variables, 'key', 'O'));
	dd.new_line();
	
	var columnas_despiece = {'apoyo':'Diametro (m)','espaciamientoRefuerzo':'Espaciamiento del Refuerzo (m)'};
	dd.add_tabla_datos("DESPIECE DEL SISTEMA EN VARILLA #3 (ɸ = 3/8”)", columnas_despiece, report['despieceVarilla']);
	dd.titulo_h1('Esquema del despiece');
	dd.titulo_h1_left('Revisión a cortante');
	dd.add_content_margin("El diseño debe cumplir:");
	dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Formula_CumplimientoCortante'), '', -1, null, dd.buscar_key(variables, 'key', 'ImagenCumplimientoCortante'));
	dd.add_variables(dd.buscar_keys(variables, 'key', ['Vu','ɸVn']));
	dd.indent = 1;
	dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Formula_OVn'));
	dd.add_content_margin("donde:");
	dd.add_variables(dd.buscar_keys(variables, 'key', ['ɸVtab','ɸc','Vc']));
	dd.indent = 2;
	dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Formula_Vc'));
	dd.add_content_margin("donde:");
	dd.add_variables(dd.buscar_keys(variables, 'key', ['f’c','Ac']));
	dd.indent = 0;
	dd.new_line();
	
	dd.titulo_h1_left('Revisión para estado límite de servicio');
	dd.add_content('Las deflexiones del sistema de losa compuesto deben ser menor a la deflexión máxima permitida calculada a continuación:');
	dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Formula_CumplimientoLimiteServicio'), '', -1, null, dd.buscar_key(variables, 'key', 'ImagenCumplimientoLimiteServicio'));
	dd.add_content_margin("donde:");
	dd.add_variables(dd.buscar_keys(variables, 'key', ['A','Amax']));
	dd.indent = 1;
	dd.add_imagen_variable(dd.buscar_key(variables, 'key', 'Formula_Amax'));
	dd.add_content_margin("donde:");
	dd.add_variables(dd.buscar_keys(variables, 'key', ['L','Lambda']));
	dd.indent = 0;
	
	/****************************************/
	/*               Finalizo               */
	/****************************************/
	dd.pushStack();
}
