/**
 * Created by stackpointer on 16/08/17.
 */

(function (angular) {
  "use strict";

  angular
    .module('app.metaldeck')
    .service('reporteMetaldeckService', [
      'dataMetaldeckService',
      'svgToCanvas',
      'svgLucesService',
      reporteMetaldeckService
    ]);


  function reporteMetaldeckService(dataMetaldeckService, svgToCanvas, svgLucesService)
  {

    var vm = this;

    //data

    // Styles del reporte
    vm.style = {
      h1: {
        margin: [0, 0, 0, 0],
        fontSize: 16,
        bold: true
      },
      h2: {
        margin: [0, 0, 0, 0],
        fontSize: 14,
        bold: true,
        alignment: 'center'
      },
      h3: {
        margin: [0, 0, 0, 0],
        fontSize: 12,
        bold: true,
        alignment: 'center'
      },
      texto: {
        fontSize: 10,
        bold: false
      },
      textoFigura: {
        fontSize: 10,
        bold: false
      },
      encabezado: {
        alignment: 'center',
        margin: [0, 0, 0, 20],
        bold: true

      },
      table: {
        margin: [0, 15, 0, 0]
      },
      tableBodyTitle: {
        fontSize: 12,
        bold: false

      },
      tableBody: {
        fontSize: 10,
        bold: false,
        margin: [0, 2, 0, 2],
        alignment: 'center'
      }

    };

    //functions

    vm.getReporte = getReporte;
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /*
    *
    * Funcion que abre un documento de tipo pdf.
    *
    * @params json -> son los resultados con la informacion necesaria para construir el reporte.
    *
    * */
    function getReporte(isTecnico)
    {
      switch (isTecnico) {
        case true:
          buildReporte(buildReporteTecnico);
          break;
        case false:
          buildReporte(buildReporteAcademico);
          break
      }
    }


    /**
     * Funcion para construir un reporte luego de calcular
     *
     * @param buildReport --> funcion callback a ejecutar luego de calcular
     * @returns {*}  --> promesa retornada
     */
    function buildReporte(buildReport)
    {
      return dataMetaldeckService.calcular()
        .then(buildReport);
    }

    function buildReporteTecnico(json)
    {
      var report = {}, content = [];

      report.styles = vm.style;
      // [left, top, right, bottom] or [horizontal, vertical] or just a number for equal margins
      report.pageMargins = [40, 60, 40, 60];

      buildDocuments(json.reporte, content, 0)
        .then(function (documents) {
          report.content = content; // se obtienen todos los documentos posibles para anexar al reporte final para mostrar
          pdfMake.createPdf(report).open()
        });
    }

    function buildReporteAcademico(json)
    {
      //logica para construir reporte academico
      var r = new reportsMetaldeck();
      r.add_json(json);
      pdfMake.createPdf(r.output()).open();

    }


    /*
    *
    * funcion para construir un documento por cada metaldeck evaluado para convertirlo posteriormente en un solo reporte.
    *
    * @params metaldecks -> array con los resultados de cada metaldeck apartir del cual se construiran los diferentes documentos a reportar.
    * @param documents -> conjunto de documentos, debido a que pueden ser varios metaldecks evaluados
    *
    * */
    function buildDocuments(metaldecks, documents, index)
    {
      if (index < metaldecks.length) {
        return buildDocumentByMetaldeck(metaldecks[index])
          .then(function (documento) {
            //agregar reporte para cada metaldeck en una pagina nueva.
            if (index + 1 < metaldecks.length) documento.pageBreak = "after";

            documents.push(documento);// se agrega el documento de un metaldeck construido por buildDocumentByMetaldeck Function.

            buildDocuments(metaldecks, documents, index + 1)

          })
      } else {
        return documents;
      }
    }

    /*
    *
    * Funcion para construir un documento del resultado de un metaldeck evaluado
    *
    * @params obj -> representa el resultado de un metaldeck evaluado.
    *
    * */
    function buildDocumentByMetaldeck(obj)
    {
      var document = {}, stack = [],
        encabezado = buildEncabezado(obj.metaldeck);

      stack.push(encabezado);

      /*
      *
      * se evalua si una tabla estará incluida dentro del documento mediante una funcion tableIsIncludable
      *
      * */

      return buildSeccionLongitudinal(obj.geometrias, obj.diametroVarilla, stack)
        .then(function () {
          return tableIsIncludable(obj.geometrias, stack, buildGeometriaTable)
        })
        .then(function () {
          return tableIsIncludable(obj.apoyos, stack, buildApoyosTable)
        })
        .then(function () {
          return tableIsIncludable(obj.geometrias, stack, buildCargasPuntualesTable)
        })
        .then(function () {
          return tableIsIncludable(obj.parametros, stack, buildParametrosDisenoTable)
        })
        .then(function () {
          return tableIsIncludable(obj.etapaConstructiva, stack, buildEtapaConstructivaTable)
        })
        .then(function () {
          return tableIsIncludable(obj.etapaServicio, stack, buildEtapaServicio)
        })
        .then(function () {

          return tableIsIncludable(obj.mallaRefuerzoTemp, stack, buildMallaRefuerzo)
            .then(function () {
              stack.push(buildEsquemaMallaRefuerzo(obj.urls))
            })

        })
        .then(function () {
          return tableIsIncludable(obj.diametroVarilla, stack, buildVarillaRefuerzo)
        })
        .then(function () {
          document.stack = stack;
          return document;
        });

    }

    /**
     *
     * funcion para validar si una tabla es incluida en una seccion del documento.
     *
     *
     * @param data -> Propiedad de donde se toma la informacion
     * @param content -> stack del documento en donde se agrega la tabla
     * @param getTable -> en caso de que @data pueda incluirse entonces se obtiene la tabla mediante la funcion callback data como getTable.
     *
     */

    function tableIsIncludable(data, content, getTable)
    {
      return new Promise(function (resolve, reject) {

        //si la propiedad no viene incluida, no viene definida dentro del json, entonces no es posible agregarla como una tabla en la seccion del documento.
        if (typeof data !== 'undefined') {
          if (validateSucces(data)) {
            var table = getTable(data);
            content.push(table);
            resolve(true)
          }
          else {
            resolve(false)
          }
        } else {
          reject(false)
        }

        function validateSucces(x)
        {
          var succes = false;

          if (typeof x === "string"){
            if(x.length > 0 && x !== "" && x !== null){
              succes = true;
            }
          }else if (data.length > 0) {
            succes = true;
          }

          return succes;
        }

      });
    }

    function buildEncabezado(metaldeck)
    {
      var encabezado = {}, stack = [], texto = {};

      texto.titulo = {};
      texto.titulo.text = 'REPORTE TÉCNICO PARA METALDECK';
      texto.titulo.style = 'h1';

      texto.metaldeck = {};
      texto.metaldeck.text = metaldeck.descripcion + " h = " + metaldeck.losa + " mm";
      texto.metaldeck.style = 'h1';

      texto.reglamento = {};
      texto.reglamento.text = 'REGLAMENTO NSR-10 / ANSI-SDI C-2011';
      texto.reglamento.style = 'h1';

      stack.push(texto.titulo, texto.metaldeck, texto.reglamento);

      encabezado.stack = stack;
      encabezado.style = 'encabezado';

      return encabezado;
    }

    function buildSeccionLongitudinal(geometrias, despieceVarillas, content)
    {
      var seccionLongitudinal = {}, stack = [], figura = {};

      figura.titulo = {};
      figura.titulo.text = "SECCIÓN LONGITUDINAL";
      figura.titulo.margin = [0, 15];
      figura.titulo.style = 'texto';

      figura.img = {};
      figura.img.width = svgLucesService.W(geometrias.length) * 0.5;
      figura.img.margin = [ (svgLucesService.W(5) - svgLucesService.W(geometrias.length))* 0.5/2, 0 ];

      var svg = svgLucesService.getSvg(geometrias, isNecessaryMomentosNegativos(despieceVarillas), dataMetaldeckService.data.estructura.apoyos);

      return svgToCanvas.getPngUrl(svg.outerHTML, svgLucesService.W(geometrias.length), svgLucesService.H())
        .then(function (pngUrl) {
          figura.img.image = pngUrl;
          stack.push(figura.titulo, figura.img);
          seccionLongitudinal.stack = stack;
          content.push(seccionLongitudinal);
          return seccionLongitudinal;
        });
    }

    function isNecessaryMomentosNegativos(despieceVarillas)
    {
      return despieceVarillas.length > 0 && despieceVarillas !== "" && despieceVarillas !== null;
    }

    /**
     * Devuelve una tabla para GEOMETRIAS Y CARGAS DISTRIBUIDAS.
     *
     * @param geometrias
     * @returns {{}}
     */
    function buildGeometriaTable(geometrias)
    {
      var tableContent = {}, table = {}, title = {}, rowTitle = [], rowHeader = [], body = [],
        headerVano = {}, headerLongitud = {}, headerCargaViva = {}, headerCargaMuerta = {}, headerPesoConcreto = {},
        headerPesoLamina = {};

      tableContent.style = "table";
      tableContent.margin = [40, 15, 0, 0];
      tableContent.table = table;

      table.widths = ['auto', 'auto', 'auto', 'auto', 'auto', 'auto'];
      table.body = body;

      title.text = "GEOMETRÍA Y CARGAS DISTRIBUIDAS";
      title.style = "tableBodyTitle";
      title.colSpan = 6;
      title.bold = true;
      title.alignment = "center";

      headerVano.text = "Vano";
      headerVano.style = "tableBodyTitle";
      headerVano.bold = false;
      headerVano.alignment = "center";

      headerLongitud.text = "Longitud";
      headerLongitud.style = "tableBodyTitle";
      headerLongitud.bold = false;
      headerLongitud.alignment = "center";


      headerCargaViva.text = "Carga Viva";
      headerCargaViva.style = "tableBodyTitle";
      headerCargaViva.bold = false;
      headerCargaViva.alignment = "center";

      headerCargaMuerta.text = "Carga Muerta";
      headerCargaMuerta.style = "tableBodyTitle";
      headerCargaMuerta.bold = false;
      headerCargaMuerta.alignment = "center";

      headerPesoConcreto.text = "Peso del Concreto";
      headerPesoConcreto.style = "tableBodyTitle";
      headerPesoConcreto.bold = false;
      headerPesoConcreto.alignment = "center";

      headerPesoLamina.text = "Peso de Lámina";
      headerPesoLamina.style = "tableBodyTitle";
      headerPesoLamina.bold = false;
      headerPesoLamina.alignment = "center";


      rowTitle.push(title, {}, {}, {}, {}, {});
      rowHeader.push(headerVano, headerLongitud, headerCargaViva, headerCargaMuerta, headerPesoConcreto, headerPesoLamina);

      body.push(rowTitle, rowHeader);

      geometrias.forEach(function (geometria) {
        body.push([
          {text: geometria.luz, style: 'tableBody'},
          {text: geometria.longitud, style: 'tableBody'},
          {text: geometria.cargaViva, style: 'tableBody'},
          {text: geometria.cargaMuerta, style: 'tableBody'},
          {text: geometria.pesoConcreto, style: 'tableBody'},
          {text: geometria.pesoLamina, style: 'tableBody'}
        ])
      });


      return tableContent;
    }

    function buildApoyosTable(apoyos)
    {
      var tableContent = {}, table = {}, title = {}, rowTitle = [], body = [];

      tableContent.style = "table";
      tableContent.margin = [210, 15, 0, 0];
      tableContent.table = table;

      table.widths = ['auto', 'auto', 'auto'];
      table.headerRows = 2;
      table.body = body;

      title.text = "APOYOS";
      title.style = "tableBodyTitle";
      title.colSpan = 3;
      title.bold = true;
      title.alignment = "center";


      rowTitle.push(title, {}, {});

      body.push(rowTitle);

      apoyos.forEach(function (apoyo) {
        body.push([
          {text: apoyo.key, style: 'tableBody'},
          {text: apoyo.valor, style: 'tableBody'},
          {text: apoyo.magnitud, style: 'tableBody'}
        ])
      });


      return tableContent;
    }

    function buildCargasPuntualesTable(geometrias)
    {
      var tableContent = {}, table = {}, title = {}, rowTitle = [], rowHeaderCasoCarga = [], rowHeaderMuertaViva = [],
        rowHeadersPXB = [],
        body = [], headerCasoCarga = {}, headerVano = {}, headerMuerta = {}, headerMuertaP = {}, headerMuertaX = {},
        headerMuertaB = {}, headerVivaP = {}, headerViva = {}, headerVivaX = {}, headerVivaB = {};

      tableContent.style = "table";
      tableContent.table = table;

      table.widths = ['auto', 'auto', 'auto', 'auto', 'auto', 'auto', 'auto'];
      tableContent.margin = [110, 15, 0, 0];
      table.body = body;

      title.text = "CARGAS PUNTUALES";
      title.style = "tableBodyTitle";
      title.colSpan = 7;
      title.bold = true;
      title.alignment = "center";

      headerVano.text = "Vano";
      headerVano.style = "tableBodyTitle";
      headerVano.rowSpan = 3;
      headerVano.alignment = "center";

      headerCasoCarga.text = "Caso Carga";
      headerCasoCarga.style = "tableBodyTitle";
      headerCasoCarga.colSpan = 6;
      headerCasoCarga.alignment = "center";

      headerMuerta.text = "Muerta";
      headerMuerta.style = "tableBodyTitle";
      headerMuerta.colSpan = 3;
      headerMuerta.alignment = "center";

      headerMuertaP.text = "P";
      headerMuertaP.style = "tableBodyTitle";
      headerMuertaP.alignment = "center";

      headerMuertaX.text = "X";
      headerMuertaX.style = "tableBodyTitle";
      headerMuertaX.alignment = "center";

      headerMuertaB.text = "B";
      headerMuertaB.style = "tableBodyTitle";
      headerMuertaB.alignment = "center";

      headerViva.text = "Viva";
      headerViva.style = "tableBodyTitle";
      headerViva.colSpan = 3;
      headerViva.alignment = "center";

      headerVivaP.text = "P";
      headerVivaP.style = "tableBodyTitle";
      headerVivaP.alignment = "center";

      headerVivaX.text = "X";
      headerVivaX.style = "tableBodyTitle";
      headerVivaX.alignment = "center";

      headerVivaB.text = "B";
      headerVivaB.style = "tableBodyTitle";
      headerVivaB.alignment = "center";


      rowTitle.push(title, {}, {}, {}, {}, {}, {});
      rowHeaderCasoCarga.push(headerVano, headerCasoCarga, {}, {}, {}, {}, {});
      rowHeaderMuertaViva.push({}, headerMuerta, {}, {}, headerViva, {}, {});
      rowHeadersPXB.push({}, headerMuertaP, headerMuertaX, headerMuertaB, headerVivaP, headerVivaX, headerVivaB);

      body.push(rowTitle, rowHeaderCasoCarga, rowHeaderMuertaViva, rowHeadersPXB);

      geometrias.forEach(function (geometria) {
        body.push([
          {text: geometria.luz, style: 'tableBody'},
          {text: geometria.cargaMuertaP, style: 'tableBody'},
          {text: geometria.cargaMuertaPX, style: 'tableBody'},
          {text: geometria.cargaMuertaPB, style: 'tableBody'},
          {text: geometria.cargaVivaP, style: 'tableBody'},
          {text: geometria.cargaVivaPX, style: 'tableBody'},
          {text: geometria.cargaVivaPB, style: 'tableBody'}
        ])
      });

      return tableContent;
    }

    function buildParametrosDisenoTable(parametros)
    {
      var tableContent = {}, table = {}, title = {}, rowTitle = [], body = [];

      tableContent.style = "table";
      tableContent.margin = [110, 15, 0, 0];
      tableContent.table = table;

      table.widths = ['auto', 'auto', 'auto'];
      table.headerRows = 2;
      table.body = body;

      title.text = "PARAMETROS DE DISEÑO";
      title.style = "tableBodyTitle";
      title.colSpan = 3;
      title.bold = true;
      title.alignment = "center";


      rowTitle.push(title, {}, {});

      body.push(rowTitle);

      parametros.forEach(function (parametro) {
        body.push([
          {text: parametro.key, style: 'tableBody'},
          {text: parametro.valor, style: 'tableBody'},
          {text: parametro.magnitud, style: 'tableBody'}
        ])
      });


      return tableContent;
    }

    function buildEtapaConstructivaTable(etapaConstructiva)
    {
      var tableContent = {}, table = {}, title = {}, rowTitle = [], rowHeader = [], body = [],
        headerSolicitacion = {}, headerResistente = {}, headerCalculadaRequerida = {}, headerLuzApoyo = {};

      tableContent.style = "table";
      tableContent.margin = [80, 15, 0, 0];
      tableContent.table = table;

      table.widths = ['25%', 'auto', '20%', '15%'];
      table.headerRows = 2;
      table.body = body;

      title.text = "DISEÑO ETAPA CONSTRUCTIVA";
      title.style = "tableBodyTitle";
      title.colSpan = 4;
      title.bold = true;
      title.alignment = "center";

      headerSolicitacion.text = "Solicitación";
      headerSolicitacion.style = "tableBodyTitle";
      headerSolicitacion.alignment = "center";

      headerResistente.text = "Resistente";
      headerResistente.style = "tableBodyTitle";
      headerResistente.alignment = "center";


      headerCalculadaRequerida.text = "Calculada/Requerida";
      headerCalculadaRequerida.style = "tableBodyTitle";
      headerCalculadaRequerida.alignment = "center";

      headerLuzApoyo.text = "Luz/Apoyo";
      headerLuzApoyo.style = "tableBodyTitle";
      headerLuzApoyo.alignment = "center";

      rowTitle.push(title, {}, {}, {});
      rowHeader.push(headerSolicitacion, headerResistente, headerCalculadaRequerida, headerLuzApoyo);

      body.push(rowTitle, rowHeader);

      etapaConstructiva.forEach(function (item) {
        body.push([
          {text: item.solicitacion + getMagnitudPartText(item.magnitud), style: 'tableBody'},
          {text: item.resistente, style: 'tableBody'},
          {text: item.calculada, style: 'tableBody'},
          {text: item.luz, style: 'tableBody'}
        ]);
      });

      function getMagnitudPartText(magnitud)
      {
        return magnitud === "" ? '' : ' (' + magnitud + ')';
      }


      return tableContent;
    }

    function buildEtapaServicio(etapaServicio)
    {
      var tableContent = {}, table = {}, title = {}, rowTitle = [], rowHeader = [], body = [],
        headerSolicitacion = {}, headerResistente = {}, headerCalculadaRequerida = {}, headerLuzApoyo = {};

      tableContent.style = "table";
      tableContent.margin = [80, 15, 0, 0];
      tableContent.table = table;

      table.widths = ['25%', 'auto', '20%', '15%'];
      table.headerRows = 2;
      table.body = body;

      title.text = "DISEÑO ETAPA SERVICIO CON ";
      title.text = dataMetaldeckService.data.estructura.diseno === 1 ?
        title.text + "ESFUERZO ADMISIBLE":
        title.text + "FACTORES DE CARGA";
      title.style = "tableBodyTitle";
      title.colSpan = 4;
      title.bold = true;
      title.alignment = "center";

      headerSolicitacion.text = "Solicitacion";
      headerSolicitacion.style = "tableBodyTitle";
      headerSolicitacion.alignment = "center";

      headerResistente.text = "Resistente";
      headerResistente.style = "tableBodyTitle";
      headerResistente.alignment = "center";


      headerCalculadaRequerida.text = "Calculada/Requerida";
      headerCalculadaRequerida.style = "tableBodyTitle";
      headerCalculadaRequerida.alignment = "center";

      headerLuzApoyo.text = "Luz/Apoyo";
      headerLuzApoyo.style = "tableBodyTitle";
      headerLuzApoyo.alignment = "center";

      rowTitle.push(title, {}, {}, {});
      rowHeader.push(headerSolicitacion, headerResistente, headerCalculadaRequerida, headerLuzApoyo);

      body.push(rowTitle, rowHeader);

      etapaServicio.forEach(function (item) {
        body.push([
          {text: item.solicitacion + ' (' + item.magnitud + ')', style: 'tableBody'},
          {text: item.resistente, style: 'tableBody'},
          {text: item.calculada, style: 'tableBody'},
          {text: item.luz, style: 'tableBody'}
        ])
      });


      return tableContent;
    }

    function buildMallaRefuerzo(mallaRefuerzoTemp)
    {
      var tableContent = {}, table = {}, title = {}, rowTitle = [], rowHeader = [], body = [],
        headerDiametro = {}, headerEspaciamiento = {};

      tableContent.style = "table";
      tableContent.margin = [130, 15, 0, 0];
      tableContent.table = table;

      table.widths = ['auto', 'auto'];
      table.headerRows = 2;
      table.body = body;

      title.text = "MALLA DE REFUERZO POR TEMPERATURA";
      title.style = "tableBodyTitle";
      title.colSpan = 2;
      title.bold = true;
      title.alignment = "center";

      headerDiametro.text = "Diámetro";
      headerDiametro.style = "tableBodyTitle";
      headerDiametro.alignment = "center";

      headerEspaciamiento.text = "Espaciamiento";
      headerEspaciamiento.style = "tableBodyTitle";
      headerEspaciamiento.alignment = "center";

      rowTitle.push(title, {});
      rowHeader.push(headerDiametro, headerEspaciamiento);

      body.push(rowTitle, rowHeader);

      mallaRefuerzoTemp.forEach(function (item) {
        body.push([
          {text: item.diametro, style: 'tableBody'},
          {text: item.espaciamiento, style: 'tableBody'}
        ])
      });


      return tableContent;
    }

    function buildEsquemaMallaRefuerzo(esquema)
    {
      var esquemaMallaRefuerzo = {}, stack = [], figura = {};

      esquema.forEach(function (item) {
        figura.titulo = {};
        figura.titulo.text = item.key;
        figura.titulo.style = 'textoFigura';

        figura.imagen = {};
        figura.imagen.image = item.valor;
        figura.imagen.width = 480;
        figura.imagen.margin = [15, 15, 0, 0];

        stack.push(figura.imagen, figura.titulo);


      });

      esquemaMallaRefuerzo.stack = stack;

      return esquemaMallaRefuerzo;
    }

    function buildVarillaRefuerzo(varilla)
    {
      var tableContent = {}, table = {}, title = {}, rowTitle = [], body = [],
        diametroVarilla = {};

      tableContent.style = "table";
      tableContent.table = table;
      tableContent.margin = [130, 15, 0, 0];

      table.widths = ['auto', 70];
      table.body = body;

      title.text = "VARILLA DE REFUERZO";
      title.style = "tableBodyTitle";
      title.bold = true;
      title.alignment = "center";

      diametroVarilla.text = varilla;
      diametroVarilla.style = "tableBodyTitle";
      diametroVarilla.alignment = "center";

      rowTitle.push(title, diametroVarilla);

      body.push(rowTitle);


      return tableContent;
    }

  }
})(window.angular);
