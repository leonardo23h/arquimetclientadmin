/**
 * Created by stackpointer on 5/04/17.
 */
/**
 * Created by aesleider on 30/03/2017.
 */
(function (angular) {
  'use strict';
  angular
    .module('app.metaldeck')
    .controller('TabMetalDesignController', [
      'dataMetaldeckService',
      'metaldeckX3dom',
      '$uibModal',
      '$log',
      'reporteMetaldeckService',
      TabMetalDesignController
    ]);

  function TabMetalDesignController(dataMetaldeckService, metaldeckX3dom, $uibModal, $log, reporteMetaldeckService)
  {

    var vm = this;

    //inject properties
    vm.dataMetaldeck = dataMetaldeckService;

    vm.x3d = metaldeckX3dom.x3d;
    vm.scene = vm.x3d.scene;
    vm.superTransform = vm.scene.superTransform;

    vm.metalZ = 0;
    vm.cargasViewer = {};

    vm.cargasViewer.mostrarLucesSeleccionadas = false;
    vm.cargasViewer.showVivasDistribuidas = false;
    vm.cargasViewer.showVivasPuntuales = false;
    vm.cargasViewer.showMuertasDistribuidas = false;
    vm.cargasViewer.showMuertasPuntuales = false;
    vm.cargasViewer.removerCargas = false;

    vm.hrefDownload = "data:application/octet-stream," + encodeURIComponent(vm.dataMetaldeck.data);

    /*
     * functions
     * */
    vm.downloadData = downloadData;
    vm.calcular = calcular;
    vm.openLongVano = openLongVano;
    vm.openCargaDistribuida = openCargaDistribuida;
    vm.openCargaPuntual = openCargaPuntual;
    vm.openParamsDiseno = openParamsDiseno;
    vm.openTiposCargas = openTiposCargas;
    vm.openConfigReporte = openConfigReporte;
    vm.downloadPdf = openConfigReporte;


    /*******************************************************************/

    (function () {
      vm.dataMetaldeck.getJsonStoraged();

    })();

    function downloadData()
    {
      var contenidoDeArchivo = vm.dataMetaldeck.data;
      var elem = angular.element('<a/>');

      elem.attr({
        href: 'data:attachment/octet-stream;charset=utf-8,' + encodeURI(JSON.stringify(contenidoDeArchivo)),
        download: "data_metaldeck_calculo.json"
      })[0].click();
    }

    function anyTransformSelected()
    {
      var validar = false;
      if (vm.superTransform.lights.selecteds.length > 0)
        validar = true;

      return validar;
    }

    function openNoTransformSelected()
    {
      var parentElem = angular.element(document.body);
      $uibModal.open({
        animation: true,
        templateUrl: 'app/main/metaldeck/modals/noTransformSelected/noTransformSelected.html',
        controller: 'noTransformSelected',
        controllerAs: 'vm',
        size: 'sm',
        appendTo: parentElem
      });
    }

    function openLongVano()
    {
      if (anyTransformSelected()) {
        var parentElem = angular.element(document.body);
        var modalInstance = $uibModal.open({
          animation: true,
          templateUrl: 'app/main/metaldeck/modals/longitudVano/longitudVanoModal.html',
          controller: 'LongVanoModalInstanceController',
          controllerAs: 'vm',
          size: 'sm',
          appendTo: parentElem,
          resolve: {
            metalZ: function () {
              vm.metalZ = parseFloat(vm.superTransform.lights.selecteds[0].getSize());
              return vm.metalZ;
            }
          }
        });

        modalInstance.result.then(function (metalZ) {
          vm.metalZ = metalZ;
          vm.dataMetaldeck.chngLightSizeBySelecteds(metalZ);
        }, function () {
          $log.info('Modal dismissed at: ' + new Date());
        });
      } else {
        openNoTransformSelected();
      }

    }

    function openCargaDistribuida()
    {

      if (anyTransformSelected()) {
        var parentElem = angular.element(document.body);
        var modalInstance = $uibModal.open({
          animation: true,
          templateUrl: 'app/main/metaldeck/modals/cargaDistribuida/cargaDistribuidaModal.html',
          controller: 'CargaDistMetalModalInstanceController',
          controllerAs: 'vm',
          size: 'md',
          appendTo: parentElem
        });

        modalInstance.result.then(function (carga) {
          vm.dataMetaldeck.aggCargaDistribuida(carga)
        }, function () {
          $log.info('Modal dismissed at: ' + new Date());
        });
      }
      else {
        openNoTransformSelected();
      }

    }

    function openCargaPuntual()
    {

      if (anyTransformSelected()) {
        var parentElem = angular.element(document.body);
        var modalInstance = $uibModal.open({
          animation: true,
          templateUrl: 'app/main/metaldeck/modals/cargaPuntual/cargaPuntualModal.html',
          controller: 'CargaPuntMetalModalInstanceController',
          controllerAs: 'vm',
          size: 'md',
          appendTo: parentElem
        });

        modalInstance.result.then(function (carga) {
          vm.dataMetaldeck.aggCargaPuntual(carga)
        }, function () {
          $log.info('Modal dismissed at: ' + new Date());
        });
      }
      else {
        openNoTransformSelected();
      }
    }

    function openParamsDiseno()
    {

      var parentElem = angular.element(document.body);
      var modalInstance = $uibModal.open({
        animation: true,
        templateUrl: 'app/main/metaldeck/modals/parametrosDiseno/parametrosDisenoModal.html',
        controller: 'ParamsDisenoModalInstanceController',
        controllerAs: 'vm',
        size: 'md',
        appendTo: parentElem,
        resolve: {
          dataMetaldeck: vm.dataMetaldeck
        }
      });

      modalInstance.result.then(function () {
        //luego de obtener configuraciones para reporte.
        console.log("dataMetaldeck", vm.dataMetaldeck.data.estructura)

        if (vm.dataMetaldeck.data.estructura.soloFormaleta === true){
          vm.dataMetaldeck.removeAllCargas();
        }

      }, function () {
        //luego de cancelar generar reporte.
      });
    }

    function openTiposCargas()
    {
      var parentElem = angular.element(document.body);
      var modalInstance = $uibModal.open({
        animation: true,
        templateUrl: 'app/main/metaldeck/modals/tiposCargas/tiposCargas.html',
        controller: 'TiposCargasInstanceController',
        controllerAs: 'vm',
        size: 'md',
        appendTo: parentElem,
        resolve: {
          cargasViewer: vm.cargasViewer
        }
      });

      modalInstance.result.then(function (cargasViewer) {
        //logic...
        if(cargasViewer.removerCargas === true) {
          vm.dataMetaldeck.removeAllCargas();
        }else {
          switch (cargasViewer.mostrarLucesSeleccionadas) {
            case true:
              vm.dataMetaldeck.showCargasBySeletedLuces(cargasViewer.showMuertasDistribuidas, cargasViewer.showVivasDistribuidas, cargasViewer.showMuertasPuntuales, cargasViewer.showVivasPuntuales);
              break;
            case false:
              vm.dataMetaldeck.showCargasByAllLuces(cargasViewer.showMuertasDistribuidas, cargasViewer.showVivasDistribuidas, cargasViewer.showMuertasPuntuales, cargasViewer.showVivasPuntuales);
              break;
          }
        }

      }, function () {
        $log.info('Modal dismissed at: ' + new Date());
      });
    }


    function openConfigReporte()
    {
      var parentElem = angular.element(document.body);
      var modalInstance = $uibModal.open({
        animation: true,
        templateUrl: 'app/main/metaldeck/modals/configReporte/configReporte.html',
        controller: 'configReporteMetalInstanceController',
        controllerAs: 'vm',
        size: 'md',
        appendTo: parentElem,
        resolve: {}
      });

      modalInstance.result.then(function (isTecnico) {
        //luego de obtener configuraciones para reporte.
        reporteMetaldeckService.getReporte(isTecnico);

      }, function () {
        //luego de cancelar generar reporte.
      });

    }

    function calcular()
    {
      vm.dataMetaldeck.calcular();
    }

  }

})(window.angular);
