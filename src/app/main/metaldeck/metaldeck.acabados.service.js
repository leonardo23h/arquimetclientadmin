/**
 * Created by stackpointer on 6/04/17.
 */
(function (angular) {
  'use strict';

  angular
    .module('app.metaldeck')
    .factory('acabadosService',
      [
        'api',
        'dataMetaldeckService',
        acabadosService
      ]);

  function acabadosService(api, dataMetaldeckService) {
    var vm = this;

    //data
    vm.acabados = [];
    vm.dataMetaldeck = dataMetaldeckService;
    //functions
    vm.getAcabados = getAcabados;
    vm.restartLaminaDefault = restartLaminaDefault;


    return vm;
    ///////////////////////////////////////////////////////////////////////////////
    function getAcabados(idUnidad) {
      return api.metaldeck.acabados.get(idUnidad)
        .then(function (response) {
          vm.acabados = response.data;
          vm.dataMetaldeck.data.metaldecks = [];
          vm.dataMetaldeck.data.metaldecks.push(vm.acabados[1].metaldecks[0]);
          return vm.acabados;
        })
    }

    function restartLaminaDefault()
    {
      if(vm.dataMetaldeck.data.metaldecks.length === 0){
        vm.dataMetaldeck.data.metaldecks.push(vm.acabados[1].metaldecks[0]);
      }
    }

  }
})(window.angular);
