/**
 * Created by stackpointer on 5/04/17.
 */
(function (angular) {
  'use strict';
  angular
    .module('app.metaldeck')
    .controller('SceneMetalController', [
      'dataMetaldeckService',
      'metaldeckX3dom',
      SceneMetalController
    ]);

  function SceneMetalController(dataMetaldeckService, metaldeckX3dom) {

    var vm = this;

    //inject properties
    vm.dataMetaldeck = dataMetaldeckService;

    vm.x3d = metaldeckX3dom.x3d;
    vm.orientationBx = vm.x3d.orientationBx;
    vm.axes = vm.x3d.axes;
    vm.scene = vm.x3d.scene;
    vm.superTransform = vm.scene.superTransform;

    //data
    vm.sliderZooming = {
      minValue:1,
      maxValue:100,
      value: 95,
      options: {
        showSelectionBar: true,
        floor:1,
        ceil:100,
        step: 1,
        vertical: true,
        onChange: function () {
          vm.x3d.zoom(vm.sliderZooming.value, 80, 95);
        }
      }

    };

    /*******************************************************************/

  }

})(window.angular);
