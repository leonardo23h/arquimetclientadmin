/**
 * Created by stackpointer on 7/04/17.
 */
(function (angular) {
  'use strict';

  angular
    .module('app.metaldeck')
    .factory('dataMetaldeckService', [
      'api',
      'metaldeckX3dom',
      'unidadesService',
      dataMetaldeckService
    ]);

  function dataMetaldeckService(api, metaldeckX3dom, unidadesService)
  {
    var vm = {};

    vm.data = {};

    vm.unidades = unidadesService;
    vm.x3d = metaldeckX3dom.x3d;
    vm.scene = vm.x3d.scene;
    vm.superTransform = vm.scene.superTransform;

    //data

    //functions
    vm.superTransform.onAddLight = __onAddLight;
    vm.superTransform.onRemoveLight = __onRemoveLight;
    vm.calcular = calcular;
    vm.getJsonStoraged = getJsonStoraged;
    vm.chngLightSizeBySelecteds = chngLightSizeBySelecteds;
    vm.aggLuz = aggLuz;
    vm.rmvLuz = rmvLuz;
    vm.aggCargaDistribuida = aggCargaDistribuida;
    vm.aggCargaPuntual = aggCargaPuntual;
    vm.showCargasByAllLuces = showCargasByAllLuces;
    vm.showCargasBySeletedLuces = showCargasBySeletedLuces;
    vm.removeAllCargas = removeAllCargas;
    vm.setVoladizos = setVoladizos;


    return vm;

    /////////////////////////////////////////////////////////////////////////////////

    function calcular()
    {
      vm.data.estructura.unidad = vm.unidades.unitSelected;

      return api.metaldeck.calcular(vm.data)
        .then(function (response) {
          vm.report = response.data;
          return vm.report
        })
    }

    //llamar json por defecto
    function getJsonStoraged()
    {
      return api.metaldeck.getJsonStoraged()
        .then(function (response) {
          vm.data = response.data;
          vm.superTransform.addDefaultLight(vm.data.estructura.luces[0].longitud);
          vm.superTransform.addApoyos(vm.data.estructura.apoyos.length);
          return vm.data;
        })
    }

    function chngLightSizeBySelecteds(long)
    {
      vm.superTransform.setLightSizeBySelecteds(long, function (i, l) {
        vm.data.estructura.luces[i].longitud = l;
      });
    }

    function aggLuz()
    {
      //solo hasta 5 lights(luz) se pueden agregar
      if (vm.superTransform.lights.list.length < 5) {
        vm.data.estructura.apoyos[vm.data.estructura.apoyos.length - 1].voladizo = false;
        vm.superTransform.addLight();
        vm.superTransform.addApoyos(1);
        vm.data.estructura.apoyos.push({nombre:"A" + vm.data.estructura.apoyos.length, voladizo: false});
      }

    }

    function __onAddLight(light)
    {
      vm.data.estructura.luces.push({longitud: light.getSize()})
    }

    function rmvLuz()
    {

      var totalLights = vm.superTransform.lights.list.length;

      if (totalLights > 1) {
        vm.superTransform.removeLight();
        vm.superTransform.removeApoyos();
      }
    }


    function __onRemoveLight(light)
    {
      vm.data.estructura.luces.pop();
      vm.data.estructura.apoyos.pop();
    }

    /**
     *
     * se encarga de agregar todas las cargas distribuidas sea de tipo muerta o viva.
     *
     * @param carga
     */
    function aggCargaDistribuida(carga)
    {
      if(carga.remove){
        removeAllSelectedCargasDistribuidas();
      }else {
        if (carga.cargaType == 'muerta') {
          vm.superTransform.lights.selecteds.forEach(function (transform) {
            __aggCargaDistribuidaMuerta(transform, carga);
          })
        }
        else if (carga.cargaType == 'viva') {
          vm.superTransform.lights.selecteds.forEach(function (transform) {
            __aggCargaDistribuidaViva(transform, carga);
          })
        }
      }



    }

    function __aggCargaDistribuidaViva(transform, carga)
    {
      var index = vm.superTransform.getIndexOfTransform(transform),
        cargaVivaD;

      cargaVivaD = __getCargaVivaDFromLuces(index);
      cargaVivaD.unidad = carga.unidades;

      transform.cargaDistMuerta.show = false;
      transform.cargaPuntViva.show = false;
      transform.cargaPuntMuerta.show = false;

      if (carga.cargaExistente) {
        var valorExistente = cargaVivaD.valor,
          cargaAcumulada = carga.valor + valorExistente;

        cargaVivaD.valor = carga.valor + valorExistente;

        transform.addCargaDistribuidaViva(cargaAcumulada);

      } else {

        cargaVivaD.valor = carga.valor;

        transform.addCargaDistribuidaViva(carga.valor);
      }

    }

    function __aggCargaDistribuidaMuerta(transform, carga)
    {
      var index = vm.superTransform.getIndexOfTransform(transform),
        cargaMuertaD;

      transform.cargaDistViva.show = false;
      transform.cargaPuntViva.show = false;
      transform.cargaPuntMuerta.show = false;

      cargaMuertaD = __getCargaMuertaDFromLuces(index);
      cargaMuertaD.unidad = carga.unidades;

      if (carga.cargaExistente) {
        var valorExistente = cargaMuertaD.valor,
          cargaAcumulada = carga.valor + valorExistente;

        cargaMuertaD.valor = cargaAcumulada;

        transform.addCargaDistribuidaMuerta(cargaAcumulada);

      } else {

        cargaMuertaD.valor = carga.valor;

        transform.addCargaDistribuidaMuerta(carga.valor);
      }

    }

    /**
     *
     * se encarga de agregar todas las cargas puntuales sea de tipo muerta o viva.
     *
     * @param carga
     */
    function aggCargaPuntual(carga)
    {

      if (carga.remove){
        removeAllSelectedCargasPuntuales();
      }else {
        if (carga.cargaType == 'muerta') {
          for (var i = 0; i < vm.superTransform.lights.list.length; i++) {
            __aggCargaPuntualMuerta(vm.superTransform.lights.selecteds[i], carga);
          }

        }
        else if (carga.cargaType == 'viva') {
          for (var j = 0; j < vm.superTransform.lights.list.length; j++) {
            __aggCargaPuntualViva(vm.superTransform.lights.selecteds[j], carga);
          }
        }
      }

    }

    function __aggCargaPuntualViva(transform, carga)
    {
      var index = vm.superTransform.getIndexOfTransform(transform),
        cargaVivaP;

      transform.cargaPuntMuerta.show = false;
      transform.cargaDistViva.show = false;
      transform.cargaDistMuerta.show = false;

      cargaVivaP = __getCargaVivaPFromLuces(index);
      cargaVivaP.unidad = carga.unidades;
      cargaVivaP.distanciaX = carga.distanciaX;
      cargaVivaP.anchoB = carga.anchoB;

      if (carga.cargaExistente) {
        var valorExistente = cargaVivaP.valor,
          cargaAcumulada = carga.valor + valorExistente;

        cargaVivaP.valor = cargaAcumulada;

        transform.addCargaPuntualViva(cargaAcumulada)

      } else {

        cargaVivaP.valor = carga.valor;

        transform.addCargaPuntualViva(carga.valor)
      }
    }

    function __aggCargaPuntualMuerta(transform, carga)
    {
      var index = vm.superTransform.getIndexOfTransform(transform),
        cargaMuertaP;

      transform.cargaPuntViva.show = false;
      transform.cargaDistViva.show = false;
      transform.cargaDistMuerta.show = false;


      cargaMuertaP = __getCargaMuertaPFromLuces(index);
      cargaMuertaP.unidad = carga.unidades;
      cargaMuertaP.distanciaX = carga.distanciaX;
      cargaMuertaP.anchoB = carga.anchoB;

      if (carga.cargaExistente) {
        var valorExistente = cargaMuertaP.valor,
          cargaAcumulada = carga.valor + valorExistente;

        cargaMuertaP.valor = cargaAcumulada;

        transform.addCargaPuntualMuerta(cargaAcumulada)
      } else {

        cargaMuertaP.valor = carga.valor;

        transform.addCargaPuntualMuerta(carga.valor)

      }
    }

    function showCargasByAllLuces(showMuertasD, showVivasD, showMuertasP, showVivasP)
    {

      vm.superTransform.lights.list.forEach(function (transform) {

        if (transform.hasCargaDistribuidaMuerta()) {
          transform.cargaDistMuerta.show = showMuertasD;
        }

        if (transform.hasCargaDistribuidaViva()) {
          transform.cargaDistViva.show = showVivasD;
        }

        if (transform.hasCargaPuntualMuerta()) {
          transform.cargaPuntMuerta.show = showMuertasP
        }

        if (transform.hasCargaPuntualViva()) {
          transform.cargaPuntViva.show = showVivasP;
        }

      })
    }

    function showCargasBySeletedLuces(showMuertasD, showVivasD, showMuertasP, showVivasP)
    {

      vm.superTransform.lights.selecteds.forEach(function (transform) {

        if (transform.hasCargaDistribuidaMuerta()) {
          transform.cargaDistMuerta.show = showMuertasD;
        }

        if (transform.hasCargaDistribuidaViva()) {
          transform.cargaDistViva.show = showVivasD;
        }

        if (transform.hasCargaPuntualMuerta()) {
          transform.cargaPuntMuerta.show = showMuertasP
        }

        if (transform.hasCargaPuntualViva()) {
          transform.cargaPuntViva.show = showVivasP;
        }

      })

    }

    function removeAllCargas()
    {
      vm.data.estructura.luces.forEach(function (luz) {
        if (luz.cargaVivaD) delete luz.cargaVivaD;
        if (luz.cargaVivaP) delete luz.cargaVivaP;
        if (luz.cargaMuertaP) delete luz.cargaMuertaP;
        if (luz.cargaMuertaD) delete luz.cargaMuertaD;
      });

      vm.superTransform.removeAllCargas();

    }

    function removeAllSelectedCargasDistribuidas()
    {
      vm.superTransform.lights.selecteds.forEach(function (light) {
        light.rmvCargaDistribuidaMuerta();
        light.rmvCargaDistribuidaViva();

        delete vm.data.estructura.luces[vm.superTransform.getIndexOfTransform(light)].cargaVivaD;
        delete vm.data.estructura.luces[vm.superTransform.getIndexOfTransform(light)].cargaMuertaD;
      })
    }

    function removeAllSelectedCargasPuntuales()
    {
      vm.superTransform.lights.selecteds.forEach(function (light) {
        light.rmvCargaPuntualMuerta();
        light.rmvCargaPuntualViva();

        delete vm.data.estructura.luces[vm.superTransform.getIndexOfTransform(light)].cargaVivaP;
        delete vm.data.estructura.luces[vm.superTransform.getIndexOfTransform(light)].cargaMuertaP;
      })
    }

    function setVoladizos(firstSupport, lastSupport)
    {
      var firstLightLong = vm.data.estructura.luces[0].longitud,
        lastLightLong = vm.data.estructura.luces[vm.data.estructura.luces.length - 1].longitud;

      vm.data.estructura.apoyos[0].voladizo = firstSupport;
      vm.data.estructura.apoyos[vm.data.estructura.apoyos.length - 1].voladizo = lastSupport;

      vm.superTransform.setVoladizos(firstSupport, lastSupport);

      /**
       * si alguno de los apoyos extremos es apto para voladizo la luz correspondiente a ese voladizo
       * debe tener un rango (0.1m - 1m) si está fuera de ese rango por defecto toma un valor de 1m dicha luz.
       */
      if(vm.data.estructura.apoyos[0].voladizo && (firstLightLong < 0.1 || firstLightLong > 1.0))
      {
        vm.data.estructura.luces[0].longitud = 1.0;
      }

      if(vm.data.estructura.apoyos[vm.data.estructura.apoyos.length - 1].voladizo && (lastLightLong < 0.1 || lastLightLong > 1.0))
      {
        vm.data.estructura.luces[vm.data.estructura.luces.length - 1].longitud = 1.0;
      }
    }

    /**
     *
     * @param index  ->  indice del item del que se quiere obtener el objeto cargaMuertaD
     * @returns {{}|*}  ->  objeto cargaMuertaD del indice index dado.  puede ser nuevo o una instancia.
     * @private
     */
    function __getCargaMuertaDFromLuces(index)
    {
      if (!vm.data.estructura.luces[index].hasOwnProperty("cargaMuertaD")) {
        vm.data.estructura.luces[index].cargaMuertaD = {};
      }

      return vm.data.estructura.luces[index].cargaMuertaD;
    }

    /**
     *
     * @param index  ->  indice del item del que se quiere obtener el objeto cargaVivaD
     * @returns {*}  ->  objeto cargaVivaD del indice index dado.  puede ser nuevo o una instancia.
     * @private
     */
    function __getCargaVivaDFromLuces(index)
    {
      if (!vm.data.estructura.luces[index].hasOwnProperty("cargaVivaD")) {
        vm.data.estructura.luces[index].cargaVivaD = {};
      }

      return vm.data.estructura.luces[index].cargaVivaD;
    }

    /**
     *
     * @param index  ->  indice del item del que se quiere obtener el objeto cargaVivaP
     * @returns {*}  ->  objeto cargaVivaP del indice index dado.  puede ser nuevo o una instancia.
     * @private
     */
    function __getCargaVivaPFromLuces(index)
    {
      if (!vm.data.estructura.luces[index].hasOwnProperty("cargaVivaP")) {
        vm.data.estructura.luces[index].cargaVivaP = {};
      }

      return vm.data.estructura.luces[index].cargaVivaP;
    }

    /**
     *
     * @param index  ->  ->  indice del item del que se quiere obtener el objeto cargaMuertaP
     * @returns {*}  ->  objeto cargaMuertaP del indice index dado.  puede ser nuevo o una instancia.
     * @private
     */
    function __getCargaMuertaPFromLuces(index)
    {
      if (!vm.data.estructura.luces[index].hasOwnProperty("cargaMuertaP")) {
        vm.data.estructura.luces[index].cargaMuertaP = {};
      }

      return vm.data.estructura.luces[index].cargaMuertaP;
    }

  }
})(window.angular);
