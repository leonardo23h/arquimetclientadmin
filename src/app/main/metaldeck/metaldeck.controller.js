/**
 * Created by aesleider on 30/03/2017.
 */
(function (angular) {
  'use strict';
  angular
    .module('app.metaldeck')
    .controller('MetaldeckController',[
      'activeItem',
      metaldeckController
    ]);

  function metaldeckController(activeItem) {

    var vm = this;

    vm.activeItem = activeItem;

    vm.activeItem.setActive("Metaldeck");
    /*
     * functions
     * */

    /*******************************************************************/

  }

})(window.angular);
