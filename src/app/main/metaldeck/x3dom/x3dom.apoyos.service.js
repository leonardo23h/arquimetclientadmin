(function (angular) {
  'use strict';

  angular
    .module('app.metaldeck')
    .service('apoyosMetaldeck', [
      'transformService',
      apoyosMetaldeck
    ]);

  function apoyosMetaldeck(transformService) {
    var vm = this;

    //injects

    //methods
    vm.newInstance = __newApoyo;

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    function __newApoyo(nameSpace)
    {
      var apoyo = newShape(nameSpace);

      //data
      apoyo.voladizo = false;

      //methods
      apoyo.setTranslation = __setTranslation;
      apoyo.setVoladizo = __setVoladizo;
      apoyo.setColor = __setColor;
      apoyo.onClick = __onClick;

      setTimeout(__initialize, 1000);

      return apoyo;

      ////////////////////////////////////////////////////////////
      function __setTranslation(position, actionCall)
      {
        apoyo.translation.z = position;

        if (typeof actionCall !== 'undefined')
          actionCall(apoyo.translation.x);
      }

      function __setVoladizo(value)
      {
        apoyo.voladizo = value;
      }

      function __handlerClick()
      {
        apoyo.onClick(apoyo);
      }

      function __setColor(color)
      {
        var id = apoyo.nameSpace + '__MA_Shape';

        /*
        * cambia el color del elemento
        * */
        document.getElementById(id).setAttribute('diffuseColor', color);
      }

      function __onClick(apoyo)
      {
        console.log('Event Click is Done!', apoyo.nameSpace)
      }

      function __initialize()
      {
        angular.element(document.getElementById(apoyo.nameSpace)).on('click', __handlerClick);
      }
    }

    function newShape(nameSpace)
    {
      var shape = transformService.newInstance(nameSpace);

      //@Override data
      shape.selected = false;
      shape.tipo = "separacion_metaldeck";

      return shape;
    }

  }

})(window.angular);
