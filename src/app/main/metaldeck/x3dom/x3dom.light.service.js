(function (angular) {
  'use strict';

  angular
    .module('app.metaldeck')
    .service('lightMetaldeck', [
      'transformService',
      'concretoService',
      'laminaService',
      lightMetaldeck
    ]);

  function lightMetaldeck(transformService, concretoService, laminaService)
  {
    var vm = this;

    //injects

    //methods
    vm.newInstance = newLight;

    //////////////////////////////////////////////////////////////////////////////////////////////////////
    function newLight(nameSpace, longitudVano)
    {
      var light = transformService.newInstance(nameSpace);

      //data
      light.selected = false;

      light.cargaDistViva = {};
      light.cargaDistViva.cargas = [];

      light.cargaDistMuerta = {};
      light.cargaDistMuerta.cargas = [];

      light.cargaPuntViva = {};
      light.cargaPuntViva.cargas = [];

      light.cargaPuntMuerta = {};
      light.cargaPuntMuerta.cargas = [];

      //Initialization Concreto Object
      light.concreto = concretoService.newInstance(nameSpace + "__Concreto", longitudVano);
      light.concreto.onClick = vano_onClick;

      //Initialization Lamina Object
      light.lamina = laminaService.newInstance(nameSpace + "__Lamina", longitudVano);
      light.lamina.onClick = vano_onClick;

      //methods
      light.addCargaDistribuidaViva = __addCargaDistribuidaViva;
      light.rmvCargaDistribuidaViva = __rmvCargaDistribuidaViva;
      light.addCargaDistribuidaMuerta = __addCargaDistribuidaMuerta;
      light.rmvCargaDistribuidaMuerta = __rmvCargaDistribuidaMuerta;
      light.addCargaPuntualViva = __addCargaPuntualViva;
      light.rmvCargaPuntualViva = __rmvCargaPuntualViva;
      light.addCargaPuntualMuerta = __addCargaPuntualMuerta;
      light.rmvCargaPuntualMuerta = __rmvCargaPuntualMuerta;
      light.hasCargaPuntualViva = hasCargaPuntualViva;
      light.hasCargaPuntualMuerta = hasCargaPuntualMuerta;
      light.hasCargaDistribuidaViva = hasCargaDistribuidaViva;
      light.hasCargaDistribuidaMuerta = hasCargaDistribuidaMuerta;
      light.setSize = __setSize;
      light.getSize = __getSize;

      //TODO mover los eventos al principio de las declaraciones.  tenerlas de ultimas puede generar excepciones pues no se han creado algunos objetos aun.
      //events
      light.onClick = __onClick;
      light.onChangeLong = __onChangeLong;


      return light;

      //////////////////////////////////////////////////////////////////////////////////////////////////

      /////////////// cargas ////////////////////////////////////////////
      function __addCargaDistribuidaViva(w)
      {

        if (Object.keys(light).length !== 0) {
          light.cargaDistViva = {};
          light.cargaDistViva.w = w;
          light.cargaDistViva.show = true;
          __repartirCargasDistViva();
        }

      }

      function __rmvCargaDistribuidaViva()
      {
        if (Object.keys(light).length !== 0) {
          light.cargaDistViva = {};
          light.cargaDistViva.cargas = [];
        }

      }

      function __addCargaDistribuidaMuerta(w)
      {

        if (Object.keys(light).length !== 0) {
          light.cargaDistMuerta = {};
          light.cargaDistMuerta.w = w;
          light.cargaDistMuerta.show = true;
          __repartirCargasDistMuerta()
        }

      }

      function __rmvCargaDistribuidaMuerta()
      {

        if (Object.keys(light).length !== 0) {
          light.cargaDistMuerta = {};
          light.cargaDistMuerta.cargas = [];
        }

      }

      function __addCargaPuntualViva(w)
      {

        if (Object.keys(light).length !== 0) {
          light.cargaPuntViva = {};
          light.cargaPuntViva.cargas = [];

          light.cargaPuntViva.w = w;
          light.cargaPuntViva.show = true;
          light.cargaPuntViva.cargas[0] = {x: 0, y: 0, z: 0};
        }

      }

      function __rmvCargaPuntualViva()
      {

        if (Object.keys(light).length !== 0) {
          light.cargaPuntViva = {};
          light.cargaPuntViva.cargas = [];
        }

      }

      function __addCargaPuntualMuerta(w)
      {

        if (Object.keys(light).length !== 0) {
          light.cargaPuntMuerta = {};
          light.cargaPuntMuerta.cargas = [];

          light.cargaPuntMuerta.w = w;
          light.cargaPuntMuerta.show = true;
          light.cargaPuntMuerta.cargas[0] = {x: 0, y: 0, z: 0};
        }

      }

      function __rmvCargaPuntualMuerta()
      {

        if (Object.keys(light).length !== 0) {
          light.cargaPuntMuerta = {};
          light.cargaPuntMuerta.cargas = [];
        }

      }

      function __repartirCargasDistViva()
      {
        var large = (light.concreto.scale.z * 4),
          width = (light.concreto.scale.x * 10),
          translationCargaLarge = -large / 2,
          translationCargaWidth = -width / 2;

        light.cargaDistViva.cargas = [];
        translationCargaWidth += 0.5;
        translationCargaLarge += 0.5;
        while (translationCargaWidth > -width / 2 && translationCargaWidth < width / 2) {
          while (translationCargaLarge > -large / 2 && translationCargaLarge < large / 2) {
            var carga = {
              translation: {x: translationCargaWidth, y: 0, z: translationCargaLarge}
            };

            light.cargaDistViva.cargas.push(carga);
            translationCargaLarge += 0.5;
          }
          translationCargaLarge = -large / 2;
          translationCargaLarge += 0.5;
          translationCargaWidth += 0.5;

        }

      }

      function __repartirCargasDistMuerta()
      {

        var large = (light.concreto.scale.z * 4),
          width = (light.concreto.scale.x * 10),
          translationCargaLarge = -large / 2,
          translationCargaWidth = -width / 2;

        light.cargaDistMuerta.cargas = [];
        translationCargaWidth += 0.5;
        translationCargaLarge += 0.5;
        while (translationCargaWidth > -width / 2 && translationCargaWidth < width / 2) {
          while (translationCargaLarge > -large / 2 && translationCargaLarge < large / 2) {
            var carga = {
              translation: {x: translationCargaWidth, y: 0, z: translationCargaLarge}
            };

            light.cargaDistMuerta.cargas.push(carga);
            translationCargaLarge += 0.5;
          }
          translationCargaLarge = -large / 2;
          translationCargaLarge += 0.5;
          translationCargaWidth += 0.5;

        }

      }

      function hasCargaPuntualViva()
      {
        var hasCarga = false;

        if (light.hasOwnProperty('cargaPuntViva') && light.cargaPuntViva.cargas.length > 0) {
          hasCarga = true;
        }

        return hasCarga;
      }

      function hasCargaPuntualMuerta()
      {
        var hasCarga = false;

        if (light.hasOwnProperty('cargaPuntMuerta') && light.cargaPuntMuerta.cargas.length > 0) {
          hasCarga = true;
        }

        return hasCarga;
      }

      function hasCargaDistribuidaViva()
      {
        var hasCarga = false;

        if (light.hasOwnProperty('cargaDistMuerta') && light.cargaDistViva.cargas.length > 0) {
          hasCarga = true;
        }

        return hasCarga;
      }

      function hasCargaDistribuidaMuerta()
      {
        var hasCarga = false;

        if (light.hasOwnProperty('cargaDistMuerta') && light.cargaDistMuerta.cargas.length > 0) {
          hasCarga = true;
        }

        return hasCarga;
      }


      /*
      * funcion para cambiar el tamaño de una luz
      * */
      function __setSize(z)
      {

        if (z <= 6 && z > 0) {
          light.concreto.scale.z = z;
          light.lamina.scale.z = z;
          if (hasCargaDistribuidaViva()) __repartirCargasDistViva();
          if (hasCargaDistribuidaMuerta()) __repartirCargasDistMuerta();
          light.onChangeLong(light)
        }

      }

      function __getSize()
      {

        if (light.concreto.scale.z === light.lamina.scale.z) {
          return light.concreto.scale.z;
        }

        return null
      }

      function vano_onClick(vano)
      {
        if (!vano.selected) {
          light.concreto.selected = true;
          light.concreto.setColor('0.5 0.5 0.5');

          light.lamina.selected = true;
          light.lamina.setColor('0.5 0.5 0.5');

          light.selected = true;
        }
        else {
          light.concreto.selected = false;
          light.concreto.setColor('0.800 0.800 0.800');

          light.lamina.selected = false;
          light.lamina.setColor('0.800 0.800 0.800');

          light.selected = false;
        }

        light.onClick(light);
      }

      function __onClick(light)
      {
        console.log('Event Click is Done!', light.nameSpace)
      }

      function __onChangeLong(light)
      {
        console.log('Event onChangeLong is Done!', light)
      }

    }

  }
})(window.angular);
