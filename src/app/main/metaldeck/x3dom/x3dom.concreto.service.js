(function (angular) {
  'use strict';

  angular
    .module('app.metaldeck')
    .service('concretoService', [
      'transformService',
      concretoService
    ]);

  function concretoService(transformService)
  {
    var vm = this;

    /**
     * counter es una funcion contador para controlar la cantidad de veces que el evento onclick se dispara
     */
    vm.counter = (function () {
      var count = 0;

      return function () {
        var yo = this;
        yo.counting = function () {
          count += 1;
        };
        yo.uncounting = function () {
          count = 0
        };
        yo.value = function () {
          return count;
        };

        return yo;
      }
    })();

    //injects

    //methods
    vm.newInstance = __newConcreto;

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * retorna un objeto Metaldeck (Concreto).
     * Cada caracteristica puede ser un subObjeto debido a que tiene otras caracteristicas dentro.
     */
    function __newConcreto(nameSpace, longitud)
    {
      var concreto = newShape(nameSpace, longitud);

      //data

      //methods
      concreto.setLongitud = setLongitud;
      concreto.setColor = __setColor;
      concreto.onClick = __onClick;

      setTimeout(__initialize, 1000);

      return concreto;

      ////////////////////////////////////////////////////////////
      function setLongitud(long, actionCall)
      {
        concreto.scale.z = long;

        if (typeof actionCall !== 'undefined')
          actionCall(concreto.scale.z);
      }

      function __handlerClick()
      {
        /*
         * para evitar que el evento se dispare por cada elemento metaldeck basico
         * del cual está compuesta la lamina, se crea una funcion contador y se ejecuta .count() que empiece a contar.
         * */
        vm.counter().counting();
        /*
         * se debe validar que solo se dispare 1 vez el evento.
         * */
        if (vm.counter().value() === 1) {
          concreto.onClick(concreto);
        }

        /*
         * cuando se haya disparado 8 veces, que corresponde al total de metaldeck basicos que hay en una lamina
         * se reinicia el contador.
         * */
        if (vm.counter().value() === 8) {
          vm.counter().uncounting();
        }
      }

      function __setColor(color)
      {
        var id = concreto.nameSpace + '__MA_Shape';

        /*
        * cambia el color del elemento
        * */
        document.getElementById(id).setAttribute('diffuseColor', color);
      }

      function __onClick(concreto)
      {
        console.log('Event Click is Done!', concreto.nameSpace)
      }

      function __initialize()
      {
        angular.element(document.getElementById(concreto.nameSpace)).on('click', __handlerClick);
      }
    }

    function newShape(nameSpace, longitudVano)
    {
      var shape = transformService.newInstance();

      //@Override data
      shape.nameSpace = nameSpace;
      shape.scale.x = 0.5;
      shape.scale.y = 0.5;
      shape.scale.z = longitudVano;
      shape.translation.x = 0;
      shape.translation.y = 0.670;
      shape.translation.z = 0;
      shape.selected = false;
      shape.tipo = "metaldeck_concreto";

      return shape;
    }

  }

})(window.angular);
