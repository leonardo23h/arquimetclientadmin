/**
 * Created by stackpointer on 24/07/17.
 */

/**
 * Created by aesleider on 13/03/2017.
 */
(function (angular) {
  'use strict';

  angular
    .module('app.metaldeck')
    .factory('metaldeckX3dom', [
      'x3dService',
      'disenoMetaldeck',
      metaldeckX3dom
    ]);

  function metaldeckX3dom(x3dService, disenoMetaldeck)
  {
    var vm = {};

    //data
    vm.x3d = x3dService.newInstance("x3domMetaldeckCentralSceneView");//construye una instancia del objeto x3d con el id especificado.

    vm.x3d.scene.superTransform = disenoMetaldeck;

    //methods


    return vm;
    /////////////////////////////////////////////// Functions  ////////////////////////////////////////////////////


  }

})(window.angular);
