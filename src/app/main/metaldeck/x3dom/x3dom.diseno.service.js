/**
 * Created by stackpointer on 24/07/17.
 */

/**
 * Created by aesleider on 13/03/2017.
 */
(function (angular) {
  'use strict';

  angular
    .module('app.metaldeck')
    .factory('disenoMetaldeck', [
      'transformService',
      'lightMetaldeck',
      'apoyosMetaldeck',
      disenoMetaldeck
    ]);

  function disenoMetaldeck(transformService, lightMetaldeck, apoyosMetaldeck)
  {
    var vm = transformService.newInstance();//construye una instancia del objeto x3d con el id especificado.

    //events
    vm.onInit = __onInit;
    vm.onClick = __onClick;
    vm.onLightClick = __onLightClick;
    vm.onSupportClick = __onSupportClick;
    vm.onAddLight = __onAddLight;
    vm.onRemoveLight = __onRemoveLight;

    //data
    _initialize();

    //methods
    vm.addDefaultLight = __addDefaultLight;
    vm.addApoyos = __addApoyos;
    vm.removeApoyos = __removeApoyos;
    vm.removeAllCargas = removeAllCargas;
    vm.addLight = newLight;
    vm.removeLight = removeLight;
    vm.setLightSize = __setLightSize;
    vm.setLightSizeBySelected = __setLightSizeBySelected;
    vm.setLightSizeBySelecteds = __setLightSizeBySelecteds;
    vm.getIndexOfTransform = __getIndexOfLight;
    vm.getIndexOfSupport = __getIndexOfSupport;
    vm.getIndexOfLightBySelected = __getIndexOfLightBySelected;
    vm.typeOf = __typeOf;
    vm.setVoladizos = __setVoladizos;


    return vm;

    /////////////////////////////////////////////// Functions  ////////////////////////////////////////////////////
    function _initialize()
    {
      //////////////////// objects instances ////////////////////

      //Lights
      vm.lights = {};
      vm.lights.list = [];
      vm.lights.selecteds = [];
      vm.lights.currently = {};

      //apoyos
      vm.apoyos = {};
      vm.apoyos.list = [];
      vm.apoyos.selecteds = [];
      vm.apoyos.currently = {};

      //////////////////// objects init ////////////////////

      //lights

      //execute onInit step.
      vm.onInit();

    }

    function __addDefaultLight(longitud)
    {
      vm.lights.list.push(lightMetaldeck.newInstance("Metaldeck", longitud));
      vm.lights.list[0].onClick = light_onClick;
      vm.lights.list[0].onChangeLong = light_onChangeLong;
    }

    function removeAllCargas()
    {
      vm.lights.list.forEach(function (light) {
        light.rmvCargaDistribuidaViva();
        light.rmvCargaDistribuidaMuerta();
        light.rmvCargaPuntualViva();
        light.rmvCargaPuntualMuerta();

      });
    }

    /**
     * funcion para la creación de nueva luz
     */
    function newLight()
    {

      restartLightsSelected();

      var transformCount = vm.lights.list.length,
        lastTransform = vm.lights.list[transformCount - 1],
        newTransform = lightMetaldeck.newInstance("Metaldeck_" + transformCount, lastTransform.concreto.scale.z);

      newTransform.onClick = light_onClick;
      newTransform.onChangeLong = light_onChangeLong;

      vm.lights.list.push(newTransform);

      newTransform.translation.z = getRetranslationLight(newTransform);

      vm.apoyos.list[vm.apoyos.list.length - 1].voladizo = false;

      vm.onAddLight(newTransform);

    }

    /**
     * funcion para eliminar luz
     */
    function removeLight()
    {
      if (vm.lights.list.length > 1) vm.onRemoveLight(vm.lights.list.pop());
      restartLightsSelected();
    }

    function __setLightSizeBySelecteds(l, callAction)
    {
      vm.lights.list.forEach(function (light, i) {
        __setLightSizeBySelected(light, l, callAction);
      })
    }

    function __setLightSize(light, l, callAction)
    {
      var indexOfList = __getIndexOfLight(light),
        existInSelected = indexOfList > -1;

      if (existInSelected) {
        light.setSize(l);
        callAction(indexOfList);
      }
    }

    function __setLightSizeBySelected(light, l, callAction)
    {
      var indexOfList = __getIndexOfLight(light),
        indexOfSelecteds = __getIndexOfLightBySelected(light),
        existInSelected = indexOfSelecteds > -1,
        isFirstLight = indexOfList === 0,
        isLastLight = indexOfList === vm.lights.list.length - 1,
        firstSupportIsVoladizo = vm.apoyos.list[0].voladizo,
        lastSupportIsVoladizo = vm.apoyos.list[vm.apoyos.list.length - 1].voladizo,
        lightSizeIsOverRange = light.getSize() < 0.1 || light.getSize() > 1.0;

      if (light.selected && existInSelected) {

        switch (vm.lights.list.length){
          case 1:

            light.setSize(l);
            callAction(indexOfList, l);
            break;

          default:
            if (isFirstLight && firstSupportIsVoladizo) {

              if(lightSizeIsOverRange){
                light.setSize(1.0);
                callAction(indexOfList, 1.0);
              }

            }
            else if ( isLastLight && lastSupportIsVoladizo) {

              if(lightSizeIsOverRange){
                light.setSize(1.0);
                callAction(indexOfList, 1.0);
              }

            }else {
              light.setSize(l);
              callAction(indexOfList, l);
            }
            break
        }

      }
    }

    //reestablece a los parametros iniciales del array de lights seleccionados.
    function restartLightsSelected()
    {
      if (vm.lights.selecteds.length > 0) {

        vm.lights.selecteds = [];

        vm.lights.list.forEach(function (light) {

          light.concreto.setColor('0.800 0.800 0.800');
          light.concreto.selected = false;

          light.lamina.setColor('0.800 0.800 0.800');
          light.lamina.selected = false;

        });

      }
    }

    /*
    * funcion que calcula una nueva posicion
    * puede usarse al agregar una nueva luz, o cuando un transform cambie su tamaño.
    * */
    function getRetranslationLight(fromObject)
    {
      var index = vm.lights.list.indexOf(fromObject),
        beforeTransform = vm.lights.list[index - 1],
        beforeTranslation = beforeTransform.translation,
        beforeMetaldeckScale = beforeTransform.concreto.scale,
        beforeSize = beforeMetaldeckScale.z * 4,

        currentTransform = vm.lights.list[index],
        currentMetaldeckScale = currentTransform.concreto.scale,
        currentSize = currentMetaldeckScale.z * 4;

      return beforeTranslation.z - beforeSize / 2 - currentSize / 2

    }

    /*
    * funcion que reordena la pila de lights luego que el tamaño de uno sea modificado
    * */
    function __reordenarStack(transform)
    {
      var indexStart = vm.lights.list.indexOf(transform);

      /**
       *
       */
      vm.lights.list.forEach(function (light, i) {
        if (i !== 0 && i >= indexStart) {
          light.translation.z = getRetranslationLight(light);
        }
      })
    }

    //funcion que se ejecuta luego de hacer click en
    function light_onClick(light)
    {
      var index = __getIndexOfLightBySelected(light);

      if (light.selected) {
        vm.lights.selecteds.push(light);
      }
      else {
        vm.lights.selecteds.splice(index, 1);
      }

      vm.onClick(light);
      vm.onLightClick(light);
    }

    //funcion que se ejecuta luego de cambiar el largo de una luz
    function light_onChangeLong(light)
    {
      __reordenarStack(light);
      __sortApoyos();
    }

    function __addApoyos(n)
    {
      var nApoyos = vm.apoyos.list.length, apoyo = {};

      for (var i = 1; i <= n; i++) {
        nApoyos++;

        apoyo = apoyosMetaldeck.newInstance("Apoyo_" + nApoyos);
        apoyo.onClick = support_onClick;

        vm.apoyos.list.push(apoyo);
      }

      __sortApoyos();
    }

    //funcion que se ejecuta luego de hacer click en apoyo
    function support_onClick(apoyo)
    {
      vm.onClick(apoyo);
      vm.onSupportClick(apoyo);
    }

    function __removeApoyos()
    {
      if (vm.lights.list.length > 0) vm.apoyos.list.pop();
    }

    //organiza los apoyos segun la longitud del vano
    function __sortApoyos()
    {
      vm.lights.list.forEach(function (light, i) {

        if (i === 0) {
          vm.apoyos.list[0].setTranslation(light.getSize() * 2);
          vm.apoyos.list[1].setTranslation(light.getSize() * -2);
        }
        else {
          vm.apoyos.list[i + 1].setTranslation(vm.apoyos.list[i].translation.z + light.getSize() * -4)
        }

      })
    }

    function __howApoyos()
    {
      var nApoyos = 0, //apoyos que deberian existir.
        leftApoyos = 0, //apoyos faltantes.
        EXTREMOS = 2, //Siempre deben existir apoyos extremos, excepto en voladizos pero solo no se muestran.
        intermedios = vm.lights.list.length - 1; //son los apoyos entre los extremos.

      if (vm.lights.list.length > 0) {
        nApoyos = EXTREMOS + intermedios;
        leftApoyos = nApoyos - vm.apoyos.list.length;
      }

      return leftApoyos;
    }

    function __getIndexOfLight(light)
    {
      return vm.lights.list.indexOf(light);
    }

    function __getIndexOfLightBySelected(light)
    {
      return vm.lights.selecteds.indexOf(light);
    }

    function __getIndexOfSupport(support)
    {
      return vm.apoyos.list.indexOf(support);
    }

    function __typeOf(shape)
    {
      var type = "";

      if (vm.lights.list.indexOf(shape) > -1) {
        type = "light"
      }
      if (vm.apoyos.list.indexOf(shape) > -1) {
        type = "apoyo"
      }

      return type;
    }

    function __setVoladizos(firstSupport, lastSupport)
    {
      var firstLightLong = vm.lights.list[0].getSize(),
        lastLightLong = vm.lights.list[vm.lights.list.length - 1].getSize();

      vm.apoyos.list[0].voladizo = firstSupport;
      vm.apoyos.list[vm.apoyos.list.length - 1].voladizo = lastSupport;

      /**
       * si alguno de los apoyos extremos es apto para voladizo la luz correspondiente a ese voladizo
       * debe tener un rango (0.1m - 1m) si está fuera de ese rango por defecto toma un valor de 1m dicha luz.
       */
      if (vm.apoyos.list[0].voladizo && (firstLightLong < 0.1 || firstLightLong > 1.0)) {
        vm.lights.list[0].setSize(1.0);
      }

      if (vm.apoyos.list[vm.apoyos.list.length - 1].voladizo && (lastLightLong < 0.1 || lastLightLong > 1.0)) {
        vm.lights.list[vm.lights.list.length - 1].setSize(1.0);
      }
    }

    ////////////////////////////// EVENTS //////////////////////////////
    function __onInit()
    {
      console.log('Initializing design!')
    }

    function __onClick(shape)
    {
      console.log("Click on " + __typeOf(shape) + " From Design")
    }

    function __onLightClick(light)
    {
      console.log("Click on " + __typeOf(light))
    }

    function __onSupportClick(support)
    {
      console.log("Click on " + __typeOf(support))
    }

    function __onAddLight(light)
    {
      console.log(light);
    }

    function __onRemoveLight(light)
    {
      console.log(light)
    }

  }

})(window.angular);
