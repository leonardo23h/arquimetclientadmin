(function (angular) {

  angular
    .module('app.metaldeck')
    .factory('svgApuntalarServices', [svgApuntalarServices]);

  function svgApuntalarServices() {
    var vm = {},
      WIDTH = 16  * 2,
      HEIGHT = 16  * 2;

    //data;

    //methods
    vm.W = W;
    vm.H = H;
    vm.getApuntalarHtml = getApuntalarHtml;


    return vm;
    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    function W()
    {
      return WIDTH;
    }

    function H() {
      return HEIGHT;
    }

    function getApuntalarHtml(id, x, y)
    {
      var gElement = angular.element('<g></g>'),
        pathElement = angular.element('<path stroke="#bf5f00" id="' + id + '" ' +
        'd="m'+ x +','+ y +'l'+ WIDTH/2 +',16l' + WIDTH/2 + ',-16l-'+ WIDTH +',0z"' +
        'stroke-width="3.0" ' +
        'fill="#ff7f00"/>');

      gElement.append(pathElement);

      return gElement;
    }

  }

})(window.angular);
