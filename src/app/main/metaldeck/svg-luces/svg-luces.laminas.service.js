(function (angular) {

  angular
    .module('app.metaldeck')
    .factory('svgLaminaServices', [svgLaminaServices]);

  function svgLaminaServices() {
    var vm = {},
      WIDTH = 92.8 * 2,
      HEIGHT = 16 * 2;

    //data;

    //methods
    vm.W = W;
    vm.H = H;
    vm.getLaminaHtml = getLaminaHtml;


    return vm;
    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    function W()
    {
      return WIDTH;
    }

    function H() {
      return HEIGHT;
    }

    function getLaminaHtml(id, x, y)
    {
      var gElement = angular.element('<g></g>'),
        rectElement = angular.element('<rect stroke="#7f7f7f" id="' + id + '" ' +
        'height="'+ HEIGHT +'" ' +
        'width="'+ WIDTH +'" ' +
        'y="'+ y +'" ' +
        'x="'+ x + '" ' +
        'stroke-width="1.0" ' +
        'fill="#c4c7ce"/>');

      gElement.append(rectElement);

      return gElement;
    }
  }

})(window.angular);
