(function (angular) {

  angular
    .module('app.metaldeck')
    .service('svgDesignBuilderServices', [
      'svgLaminaServices',
      'svgApoyoServices',
      'svgMomentoServices',
      'svgApuntalarServices',
      svgDesignBuilderServices
    ]);

  function svgDesignBuilderServices(svgLaminaServices, svgApoyoServices, svgMomentoServices, svgApuntalarServices)
  {
    var vm = this;

    //methods
    vm.buildSvg = buildSvg;
    vm.H = getH;
    vm.W = getW;

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * metodo para construir un elemento svg del diseno metaldeck
     *
     * @param lights
     * @param hasMomentoNegativo
     * @param apoyos
     * @return {*|Object}
     */
    function buildSvg(lights, hasMomentoNegativo, apoyos)
    {
      var svgElement = buildSvgContainerHtml(lights.length),
        gElement = angular.element('<g></g>');

      gElement.append(buildLights(lights.length));
      gElement.append(buildApuntalar(lights));
      if (hasMomentoNegativo) gElement.append(buildMomentosNegativos(lights.length));
      gElement.append(buildSupports(lights.length, apoyos));

      svgElement.append(gElement);

      return svgElement;
    }


    /**
     * metodo para construir el contenedor svg en cual se agregan los elementos del diseno metaldeck.
     *
     * @param nLuces --> basado en la cantidad de luces se calcula el ancho del svg.
     * @returns {*|Object} --> objeto Html Element que contiene el diseno metaldeck en svg.
     */
    function buildSvgContainerHtml(nLuces)
    {
      return angular.element('<svg width="' + getW(nLuces) + '" ' +
        'height="' + getH() + '" ' +
        'xmlns="http://www.w3.org/2000/svg"></svg>')
    }

    /**
     * metodo para construir el conjunto de etiquetas g de luces que se agregan al diseno metaldeck
     *
     * @param n --> cantidad de etiquetas g para luces a construir.
     * @returns {*|Object} --> etiqueta g que contiene el conjunto de luces para el diseno metaldeck svg.
     */
    function buildLights(n)
    {
      var gElement = angular.element('<g></g>'),
        gElementLight = {},
        elementLight = {},
        Xlamina = 0;

      for (var i = 0; i < n; i++) {

        gElementLight = angular.element('<g></g>');
        Xlamina = (i * svgLaminaServices.W()) + svgApoyoServices.W() / 2;
        elementLight = svgLaminaServices.getLaminaHtml("svg_lamina_" + i + 1, Xlamina, 0);

        gElement.append(gElementLight.append(elementLight))

      }

      return gElement;
    }

    /**
     * metodo para construir el conjunto de etiquetas g de apuntalar que se agregan al diseno metaldeck
     *
     * @param lights --> conjunto de luces para etiquetas g para apuntalar a construir.
     * @returns {*|Object} --> etiqueta g que contiene el conjunto de apuntalar para el diseno metaldeck svg.
     */
    function buildApuntalar(lights)
    {
      var gElement = angular.element('<g></g>'),
        gElementApuntalar = {},
        elementApuntalar = {},
        SizelaminasBefore = 0, // sumatoria del ancho de todas las laminas creadas antes de la actual.
        Xapuntalar = 0;

      lights.forEach(function (light, i) {

        if (light.apuntalar){
          gElementApuntalar = angular.element('<g></g>');
          SizelaminasBefore = i * svgLaminaServices.W(); // sumatoria del ancho de todas las laminas creadas antes de la actual.
          Xapuntalar = SizelaminasBefore + svgLaminaServices.W() / 2 + svgApoyoServices.W() / 4;
          elementApuntalar = svgApuntalarServices.getApuntalarHtml("svg_apuntalar_" + i + 1, Xapuntalar, svgLaminaServices.H());

          gElement.append(gElementApuntalar.append(elementApuntalar))
        }

      });

      return gElement;
    }

    /**
     * metodo para construir el conjunto de etiquetas g de momentos negativos que se agregan al diseno metaldeck
     *
     * @param n --> cantidad de etiquetas g para momentos negativos a construir.
     * @returns {*|Object} --> etiqueta g que contiene el conjunto de momentos negativos para el diseno metaldeck svg.
     */
    function buildMomentosNegativos(n)
    {
      var gElement = angular.element('<g></g>'),
        gElementMomentosNegativos = {},
        elementMomentosNegativos = {},
        Xapoyo = 0;

      for (var i = 1; i < n; i++) {

        gElementMomentosNegativos = angular.element('<g></g>');
        Xapoyo = (i * svgLaminaServices.W());
        elementMomentosNegativos = svgMomentoServices.getMomentoHtml("svg_momento_negativo_" + i + 1, Xapoyo, svgLaminaServices.H()/1.8);

        gElement.append(gElementMomentosNegativos.append(elementMomentosNegativos))

      }

      return gElement;
    }

    function buildSupports(nLights, apoyos)
    {

      var gElement = angular.element('<g></g>'),
        gElementSupport = {},
        elementSupport = {},
        nApoyos = nLights + 1,
        Xapoyo = 0;

      apoyos.forEach(function (apoyo, i) {

        if ( !apoyo.voladizo) {
          gElementSupport = angular.element('<g></g>');
          Xapoyo = (i * svgLaminaServices.W());
          elementSupport = svgApoyoServices.getSupportHtml("svg_apoyo_" + i + 1, Xapoyo, svgLaminaServices.H());

          gElement.append(gElementSupport.append(elementSupport))
        }

      });

      return gElement;

    }


    function getW(nLuces)
    {
      return (nLuces * svgLaminaServices.W()) + svgApoyoServices.W();
    }

    function getH()
    {
      return svgLaminaServices.H() + svgApoyoServices.H();
    }

  }

})(window.angular);
