(function (angular) {

  angular
    .module('app.metaldeck')
    .factory('svgApoyoServices', [svgApoyoServices]);

  function svgApoyoServices() {
    var vm = {},
      WIDTH = 16 * 2,
      HEIGHT = 16 * 2;

    //data;

    //methods
    vm.W = W;
    vm.H = H;
    vm.getHowManySupports = getHowManySupports;
    vm.getSupportHtml = getSupportHtml;


    return vm;
    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    function W()
    {
      return WIDTH;
    }

    function H() {
      return HEIGHT;
    }

    function getSupportHtml(id, x, y)
    {
      var gElement = angular.element('<g></g>'),
        rectElement = angular.element('<rect stroke="#000" id="' + id + '" ' +
        'height="'+ HEIGHT +'" ' +
        'width="'+ WIDTH +'" ' +
        'y="'+ y +'" ' +
        'x="'+ x + '" ' +
        'stroke-width="1.0" ' +
        'fill="#0f5fff"/>');

      gElement.append(rectElement);

      return gElement;
    }

    function getHowManySupports(nLuces) {
      return nLuces + 1;
    }
  }

})(window.angular);
