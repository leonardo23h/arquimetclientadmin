(function (angular) {

  angular
    .module('app.metaldeck')
    .factory('svgMomentoServices', [svgMomentoServices]);

  function svgMomentoServices() {
    var vm = {},
      WIDTH = 16 * 2;

    //data;

    //methods
    vm.W = W;
    vm.getMomentoHtml = getMomentoHtml;


    return vm;
    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    function W()
    {
      return WIDTH;
    }

    function getMomentoHtml(id, x, y)
    {
      var init =  x,
        end = x + WIDTH,
        gElement = angular.element('<g></g>'),
        lineElement = angular.element('<line stroke-linecap="null" ' +
        'stroke-linejoin="null" ' +
        'id="' + id + '" ' +
        'y2="' + y + '" ' +
        'x2="' + end + '" ' +
        'y1="' + y + '" ' +
        'x1="' + init + '" ' +
        'stroke-width="3.5" ' +
        'stroke="#4c4c4c" ' +
        'fill="none"/>');

      gElement.append(lineElement);

      return gElement;
    }
  }

})(window.angular);
