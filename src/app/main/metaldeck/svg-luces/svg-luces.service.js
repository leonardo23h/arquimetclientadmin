(function (angular) {
  'use strict';

  angular
    .module('app.metaldeck')
    .service('svgLucesService', [
      'svgDesignBuilderServices',
      svgLucesService
    ]);

  function svgLucesService(svgDesignBuilderServices) {
    var vm = this;

    vm.getSvg = getSvg;
    vm.H = H;
    vm.W = getW;

    /**
     *
     * @param lights -> conjunto de luces para el diseno.
     * @param momentoNegativo -> indica si debe dibujarse una figura Momento Negativo entre luces
     * @param apoyos --> conjunto de apoyos para el diseno.
     */
    function getSvg(lights, momentoNegativo, apoyos)
    {
      var svg = svgDesignBuilderServices.buildSvg(lights, momentoNegativo, apoyos);

      return svg[0];
    }

    function H() {
      return svgDesignBuilderServices.H();
    }

    function getW(nLuces)
    {
      return svgDesignBuilderServices.W(nLuces);
    }



  }

})(window.angular);
